# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20190129072236) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "activeadmin_settings_pictures", :force => true do |t|
    t.string   "data"
    t.string   "data_file_size"
    t.string   "data_content_type"
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "activeadmin_settings_settings", :force => true do |t|
    t.string   "name"
    t.string   "string"
    t.string   "file"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "activeadmin_settings_settings", ["name"], :name => "index_activeadmin_settings_on_name"

  create_table "activities", :force => true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "activities", ["owner_id", "owner_type"], :name => "index_activities_on_owner_id_and_owner_type"
  add_index "activities", ["recipient_id", "recipient_type"], :name => "index_activities_on_recipient_id_and_recipient_type"
  add_index "activities", ["trackable_id", "trackable_type"], :name => "index_activities_on_trackable_id_and_trackable_type"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "banners", :force => true do |t|
    t.string   "name"
    t.string   "link"
    t.string   "banner_file_name"
    t.string   "banner_content_type"
    t.integer  "banner_file_size"
    t.datetime "banner_updated_at"
    t.text     "banner_meta"
    t.integer  "clicks",              :default => 0,    :null => false
    t.integer  "max_clicks",          :default => 0,    :null => false
    t.string   "place"
    t.integer  "views",               :default => 0,    :null => false
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.boolean  "published",           :default => true, :null => false
  end

  create_table "calendars", :force => true do |t|
    t.string   "title",         :null => false
    t.datetime "start_at",      :null => false
    t.datetime "finish_at",     :null => false
    t.string   "place",         :null => false
    t.string   "organizations", :null => false
    t.string   "disciplines",   :null => false
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "ckeditor_assets", :force => true do |t|
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_ckeditor_assetable_type"

  create_table "comments", :force => true do |t|
    t.string   "title",            :limit => 50, :default => ""
    t.text     "comment"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.integer  "user_id"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
  end

  add_index "comments", ["commentable_id"], :name => "index_comments_on_commentable_id"
  add_index "comments", ["commentable_type"], :name => "index_comments_on_commentable_type"
  add_index "comments", ["user_id"], :name => "index_comments_on_user_id"

  create_table "competition_articles", :force => true do |t|
    t.string   "title",                            :null => false
    t.text     "body",                             :null => false
    t.string   "link"
    t.boolean  "show_comments",  :default => true, :null => false
    t.integer  "competition_id",                   :null => false
    t.string   "slug",                             :null => false
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  add_index "competition_articles", ["competition_id"], :name => "index_competition_articles_on_competition_id"
  add_index "competition_articles", ["slug"], :name => "index_competition_articles_on_slug", :unique => true

  create_table "competitions", :force => true do |t|
    t.string   "title",                        :null => false
    t.text     "preview",                      :null => false
    t.string   "slug"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.string   "thumbnail_image_file_name"
    t.string   "thumbnail_image_content_type"
    t.integer  "thumbnail_image_file_size"
    t.datetime "thumbnail_image_updated_at"
    t.text     "thumbnail_image_meta"
    t.text     "thumbnail_video"
    t.datetime "start_at"
    t.string   "video_link"
    t.integer  "year",                         :null => false
  end

  add_index "competitions", ["slug"], :name => "index_competitions_on_slug", :unique => true
  add_index "competitions", ["year"], :name => "index_competitions_on_year"

  create_table "discussions", :force => true do |t|
    t.integer  "messages_count",   :default => 0
    t.integer  "discussable_id"
    t.string   "discussable_type"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "discussions", ["discussable_id", "discussable_type"], :name => "discussions_discussable_idx"

  create_table "documents_posts", :id => false, :force => true do |t|
    t.integer "post_id"
    t.integer "document_id"
  end

  add_index "documents_posts", ["post_id", "document_id"], :name => "index_documents_posts_on_post_id_and_document_id", :unique => true

  create_table "forums", :force => true do |t|
    t.string   "title"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.string   "slug"
    t.integer  "topics_count",    :default => 0
    t.integer  "topics_per_page"
    t.integer  "posts_per_page"
  end

  add_index "forums", ["slug"], :name => "index_categories_on_slug", :unique => true

  create_table "groups", :force => true do |t|
    t.string "name"
    t.string "description"
  end

  add_index "groups", ["name"], :name => "index_groups_on_name", :unique => true

  create_table "images_posts", :id => false, :force => true do |t|
    t.integer "post_id"
    t.integer "image_id"
  end

  add_index "images_posts", ["post_id", "image_id"], :name => "index_images_posts_on_post_id_and_image_id", :unique => true

  create_table "memberships", :force => true do |t|
    t.integer "group_id"
    t.integer "member_id"
  end

  add_index "memberships", ["group_id"], :name => "index_memberships_on_group_id"
  add_index "memberships", ["member_id"], :name => "index_memberships_on_member_id"

  create_table "messages", :force => true do |t|
    t.integer  "user_id"
    t.integer  "discussion_id"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "messages", ["discussion_id"], :name => "index_messages_on_discussion_id"
  add_index "messages", ["user_id"], :name => "index_messages_on_user_id"

  create_table "movies_posts", :id => false, :force => true do |t|
    t.integer "post_id"
    t.integer "movie_id"
  end

  add_index "movies_posts", ["post_id", "movie_id"], :name => "index_movies_posts_on_post_id_and_movie_id", :unique => true

  create_table "news", :force => true do |t|
    t.string   "title",                                :null => false
    t.string   "slug"
    t.string   "anonce",                               :null => false
    t.text     "body"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.boolean  "published",          :default => true, :null => false
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.text     "photo_meta"
    t.string   "external_link"
    t.integer  "year",                                 :null => false
    t.integer  "month",                                :null => false
  end

  add_index "news", ["month", "year"], :name => "index_news_on_month_and_year"
  add_index "news", ["slug"], :name => "index_news_on_slug", :unique => true

  create_table "pages", :force => true do |t|
    t.string   "title"
    t.text     "body"
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.string   "slug"
    t.boolean  "draft",         :default => false, :null => false
    t.string   "link"
    t.boolean  "in_menu",       :default => true,  :null => false
    t.boolean  "show_comments", :default => false, :null => false
    t.text     "joined_path"
  end

  add_index "pages", ["joined_path"], :name => "index_pages_on_joined_path", :unique => true
  add_index "pages", ["lft", "rgt"], :name => "index_pages_on_lft_rgt"
  add_index "pages", ["link"], :name => "index_pages_on_link"
  add_index "pages", ["parent_id"], :name => "index_pages_on_parent_id"
  add_index "pages", ["slug"], :name => "pages_slug", :unique => true

  create_table "photo_albums", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                                               :null => false
    t.datetime "updated_at",                                               :null => false
    t.string   "slug"
    t.boolean  "published",                        :default => false,      :null => false
    t.integer  "coverable_id"
    t.string   "coverable_type"
    t.string   "cover_file_name"
    t.string   "cover_content_type"
    t.integer  "cover_file_size"
    t.datetime "cover_updated_at"
    t.integer  "year",                                                     :null => false
    t.string   "sort",               :limit => 15, :default => "position", :null => false
    t.string   "order",              :limit => 4,  :default => "asc",      :null => false
  end

  add_index "photo_albums", ["coverable_type", "coverable_id"], :name => "index_photo_albums_on_coverable_type_and_coverable_id"
  add_index "photo_albums", ["slug"], :name => "index_photo_albums_on_slug", :unique => true

  create_table "photos", :force => true do |t|
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.text     "description"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.integer  "photoable_id"
    t.string   "photoable_type"
    t.text     "photo_meta"
    t.integer  "position",           :default => 1,     :null => false
    t.boolean  "in_top",             :default => false, :null => false
    t.boolean  "top_at_once",        :default => false, :null => false
    t.datetime "showed_at"
    t.datetime "expired_at"
  end

  add_index "photos", ["photoable_id", "photoable_type"], :name => "photos_photoable_idx"

  create_table "post_attachments", :force => true do |t|
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.text     "file_meta"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "posts", :force => true do |t|
    t.text     "body"
    t.integer  "user_id"
    t.integer  "topic_id"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.string   "state",      :default => "approved"
    t.integer  "position",   :default => 1,          :null => false
  end

  add_index "posts", ["topic_id", "position"], :name => "topic_id_position", :unique => true
  add_index "posts", ["user_id"], :name => "index_posts_on_user_id"

  create_table "profiles", :force => true do |t|
    t.integer  "user_id"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.date     "birthday"
    t.text     "bio"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.boolean  "show_email",          :default => false, :null => false
    t.string   "gender"
    t.string   "forum_sign"
    t.boolean  "show_online",         :default => true,  :null => false
    t.text     "avatar_meta"
    t.boolean  "publish_comments",    :default => false, :null => false
    t.string   "time_zone"
  end

  add_index "profiles", ["user_id"], :name => "index_profiles_on_user_id"

  create_table "seo_meta", :force => true do |t|
    t.integer  "seo_meta_id"
    t.string   "seo_meta_type"
    t.string   "browser_title"
    t.text     "meta_description"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "meta_keywords"
  end

  add_index "seo_meta", ["id"], :name => "index_seo_meta_on_id"
  add_index "seo_meta", ["seo_meta_id", "seo_meta_type"], :name => "id_type_index_on_seo_meta"

  create_table "sliders", :force => true do |t|
    t.string   "name"
    t.string   "slide_file_name"
    t.string   "slide_content_type"
    t.integer  "slide_file_size"
    t.datetime "slide_updated_at"
    t.text     "slide_meta"
    t.integer  "position",           :default => 0, :null => false
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  create_table "speakers", :force => true do |t|
    t.integer  "user_id"
    t.integer  "discussion_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "speakers", ["discussion_id"], :name => "index_speakers_on_discussion_id"
  add_index "speakers", ["user_id"], :name => "index_speakers_on_user_id"

  create_table "subscription_forums", :force => true do |t|
    t.integer  "user_id",    :null => false
    t.integer  "forum_id",   :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "subscription_forums", ["user_id", "forum_id"], :name => "index_subscription_forums_on_user_id_and_forum_id", :unique => true

  create_table "subscriptions", :force => true do |t|
    t.integer  "user_id",    :null => false
    t.integer  "topic_id",   :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "subscriptions", ["user_id", "topic_id"], :name => "index_subscriptions_on_user_id_and_topic_id", :unique => true

# Could not dump table "topics" because of following StandardError
#   Unknown type 'topic_states' for column 'state'

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.datetime "last_request_at"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer  "failed_attempts",        :default => 0
    t.datetime "locked_at"
    t.string   "omniauth_token"
    t.string   "omniauth_secret"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "videos", :force => true do |t|
    t.string   "link"
    t.string   "video_id"
    t.string   "title"
    t.string   "provider"
    t.text     "description"
    t.integer  "width"
    t.integer  "height"
    t.text     "embed_code"
    t.string   "embed_url"
    t.integer  "videoable_id"
    t.string   "videoable_type"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "thumbnail_file_name"
    t.string   "thumbnail_content_type"
    t.integer  "thumbnail_file_size"
    t.datetime "thumbnail_updated_at"
    t.text     "thumbnail_meta"
    t.boolean  "published",              :default => false, :null => false
    t.time     "duration"
  end

  add_index "videos", ["video_id"], :name => "index_videos_on_video_id"
  add_index "videos", ["videoable_id", "videoable_type"], :name => "videos_videoable_idx"

  create_table "views", :force => true do |t|
    t.integer  "user_id"
    t.integer  "viewable_id"
    t.string   "viewable_type"
    t.integer  "count",         :default => 0
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  add_index "views", ["user_id"], :name => "index_views_on_user_id"
  add_index "views", ["viewable_id", "viewable_type"], :name => "views_viewable_idx"

end
