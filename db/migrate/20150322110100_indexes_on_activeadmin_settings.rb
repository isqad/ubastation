class IndexesOnActiveadminSettings < ActiveRecord::Migration
  def self.up
    execute <<-SQL.strip_heredoc
      CREATE INDEX index_activeadmin_settings_on_name ON
      activeadmin_settings_settings ("name");

      DROP INDEX index_topics_on_slug;
    SQL

    if table_exists? :bugs
      drop_table :bugs
    end

    execute <<-SQL.strip_heredoc
      ALTER TABLE topics ADD CONSTRAINT topics_slug UNIQUE (forum_id, slug)
      DEFERRABLE INITIALLY DEFERRED;
    SQL
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
