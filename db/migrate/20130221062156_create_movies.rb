class CreateMovies < ActiveRecord::Migration
  def self.up
    create_table :movies do |t|
      t.string :title
      t.text :description
      t.string :link
      t.string :provider
      t.time :duration
      t.text :embed_code
      t.string :embed_url
      t.integer :width
      t.integer :height
      t.string :video_id
      t.attachment :thumbnail
      t.references :user
      t.timestamps
    end

    add_index :movies, :user_id

    create_table :movies_posts, :id => false do |t|
      t.references :post, :movie
    end

    add_index :movies_posts, [ :post_id, :movie_id ], :unique => true
  end

  def self.down
    drop_table :movies
    drop_table :movies_posts
  end
end
