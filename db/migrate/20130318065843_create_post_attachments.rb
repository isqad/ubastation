class CreatePostAttachments < ActiveRecord::Migration
  def change
    create_table :post_attachments do |t|
      t.attachment :file
      t.text :file_meta
      t.timestamps
    end
  end
end
