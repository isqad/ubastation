class AddCacheCounterToTopics < ActiveRecord::Migration
  def self.up
    add_column :topics, :posts_count, :integer, :default => 0

    Topic.reset_column_information
    Topic.find_each do |t|
      Topic.update_counters t.id, :posts_count => t.posts.length
    end
  end

  def self.down
    remove_column :topics, :posts_count
  end
end
