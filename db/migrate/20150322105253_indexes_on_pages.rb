class IndexesOnPages < ActiveRecord::Migration
  def self.up
    execute <<-SQL.strip_heredoc
      DROP INDEX index_pages_on_lft;
      DROP INDEX index_pages_on_rgt;
      DROP INDEX index_pages_on_slug;
    SQL

    execute <<-SQL.strip_heredoc
      CREATE INDEX index_pages_on_lft_rgt ON pages (lft, rgt);
      CREATE INDEX index_pages_on_link ON pages (link);
      ALTER TABLE pages ADD CONSTRAINT pages_slug UNIQUE (slug)
      DEFERRABLE INITIALLY DEFERRED;
    SQL
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
