class ConvertPostsBbcodeToHtmlSafe < ActiveRecord::Migration
  include ActionView::Helpers::TextHelper

  def up
    Post.all.each do |post|
      Post.update_all({
        :body => post.body.bbcode_to_html_with_formatting.smileize.html_safe
      }, {:id => post.id})
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
