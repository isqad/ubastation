class CreateImages < ActiveRecord::Migration
  def self.up
    create_table :images do |t|
      t.attachment :file
      t.text :description
      t.references :user
      t.timestamps
    end

    add_index :images, :user_id

    create_table :images_posts, :id => false do |t|
      t.references :post, :image
    end

    add_index :images_posts, [ :post_id, :image_id ], :unique => true
  end

  def self.down
    drop_table :images
    drop_table :images_posts
  end
end
