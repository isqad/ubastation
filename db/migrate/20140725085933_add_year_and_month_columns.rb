# encoding: utf-8
class AddYearAndMonthColumns < ActiveRecord::Migration
  def self.up
    add_column :news, :year, :integer, :limit => 4
    add_column :news, :month, :integer, :limit => 2

    execute <<-SQL
      BEGIN;
      UPDATE news SET year = EXTRACT(YEAR FROM created_at AT TIME ZONE '+6'), month = EXTRACT(MONTH FROM created_at AT TIME ZONE '+6');
      COMMIT;
    SQL

    change_column :news, :year, :integer, :null => false
    change_column :news, :month, :integer, :null => false

    add_index :news, [:month, :year]

    add_column :competitions, :year, :integer, :limit => 4

    execute <<-SQL
      BEGIN;
      UPDATE competitions SET year = EXTRACT(YEAR FROM start_at AT TIME ZONE '+6');
      COMMIT;
    SQL

    change_column :competitions, :year, :integer, :null => false

    add_index :competitions, :year
  end

  def self.down
    remove_column :news, :year
    remove_column :news, :month
  end
end
