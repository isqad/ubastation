class AddMembershipIdIndex < ActiveRecord::Migration
  def self.up
    add_index :memberships, :member_id
  end

  def self.down
    remove_index :memberships, :column => :member_id
  end
end
