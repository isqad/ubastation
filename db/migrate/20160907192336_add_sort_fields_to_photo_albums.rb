class AddSortFieldsToPhotoAlbums < ActiveRecord::Migration
  def up
    execute <<-SQL
      END;
      CREATE TYPE photo_albums_sort_type AS ENUM ('position', 'photo_file_name', 'created_at');
      CREATE TYPE photo_albums_order_type AS ENUM ('asc', 'desc');
    SQL

    add_column :photo_albums, :sort, :string, limit: 15, null: false, default: 'position'
    add_column :photo_albums, :order, :string, limit: 4, null: false, default: 'asc'
  end

  def down
    remove_column :photo_albums, :sort
    remove_column :photo_albums, :order

    execute <<-SQL
      END;
      DROP TYPE photo_albums_sort_type;
      DROP TYPE photo_albums_order_type;
    SQL
  end
end
