class CreateCalendars < ActiveRecord::Migration
  def change
    create_table :calendars do |t|

      t.string :title, :null => false
      t.datetime :start_at, :null => false
      t.datetime :finish_at, :null => false
      t.string :place, :null => false
      t.string :organizations, :null => false
      t.string :disciplines, :null => false

      t.timestamps
    end
  end
end
