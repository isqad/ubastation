class AddReferencesToPhotosAlbum < ActiveRecord::Migration
  def change
    change_table :photos do |t|
      t.references :photo_albums
    end

    add_index :photos, :photo_albums_id
  end
end
