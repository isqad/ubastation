class AddShowCommentsToPages < ActiveRecord::Migration
  def self.up
    add_column :pages, :show_comments, :boolean, default: false, null: false
  end

  def self.down
    remove_column :pages, :show_comments
  end
end
