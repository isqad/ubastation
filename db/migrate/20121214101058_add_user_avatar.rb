class AddUserAvatar < ActiveRecord::Migration
  def change
    change_table(:users) do |t|
      t.attachment :avatar
      t.string :name
      t.date :birthday
      t.text :bio
    end
  end
end
