class RemoveNumberColumnFromPost < ActiveRecord::Migration
  def self.up
    remove_column :posts, :number
  end

  def self.down
    add_column :posts, :number, :default => 1, :null => false
  end
end
