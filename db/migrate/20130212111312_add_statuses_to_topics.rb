class AddStatusesToTopics < ActiveRecord::Migration
  def change
    change_table :topics do |t|
      t.boolean :closed, :default => false, :null => false
      t.boolean :pinned, :default => false, :null => false
    end
  end
end
