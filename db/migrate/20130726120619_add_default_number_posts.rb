class AddDefaultNumberPosts < ActiveRecord::Migration
  def self.up
    execute %{UPDATE forums SET posts_per_page = 15;}
  end

  def self.down
  end
end
