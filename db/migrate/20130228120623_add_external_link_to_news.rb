class AddExternalLinkToNews < ActiveRecord::Migration
  def change
    add_column :news, :external_link, :string
  end
end
