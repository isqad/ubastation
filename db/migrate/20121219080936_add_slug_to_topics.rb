class AddSlugToTopics < ActiveRecord::Migration
  def change
    change_table :topics do |t|
      t.string :slug
    end

    add_index :topics, :slug, :unique => true
  end
end
