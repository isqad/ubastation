class RenameColumnCompetitions < ActiveRecord::Migration
  def self.up
    rename_column :competitions, :start, :start_competition
    rename_column :competitions, :end, :end_competition
  end

  def self.down
    rename_column :competitions, :start_competition, :start
    rename_column :competitions, :end_competition, :end
  end
end
