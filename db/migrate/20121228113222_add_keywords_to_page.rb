class AddKeywordsToPage < ActiveRecord::Migration
  def change
    change_table :seo_meta do |t|
      t.string :meta_keywords
    end
  end
end
