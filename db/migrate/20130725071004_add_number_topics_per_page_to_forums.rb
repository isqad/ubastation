class AddNumberTopicsPerPageToForums < ActiveRecord::Migration
  def self.up
    add_column :forums, :topics_per_page, :integer

    execute %q{UPDATE forums SET topics_per_page = 11;}
  end

  def self.down
    remove_column :forums, :topics_per_page
  end
end
