class AddCloserToTopics < ActiveRecord::Migration
  def self.up
    add_column :topics, :closer_id, :integer
    add_index :topics, :closer_id
  end

  def self.down
    remove_column :topics, :closer_id
  end
end
