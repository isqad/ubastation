class AddSlugToForum < ActiveRecord::Migration
  def change
    change_table :forums do |t|
      t.string :slug
    end

    add_index :forums, :slug, :unique => true
  end
end
