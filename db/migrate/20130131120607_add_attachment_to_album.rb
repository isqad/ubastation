class AddAttachmentToAlbum < ActiveRecord::Migration
  def self.up
    add_attachment :photo_albums, :cover
  end

  def self.down
    remove_attachment :photo_albums, :cover
  end
end
