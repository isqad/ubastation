# encoding: utf-8
class AddJoinedPathToPages < ActiveRecord::Migration
  def self.up
    add_column :pages, :joined_path, :text
    add_index :pages, :joined_path, :unique => true

    Page.ordered.each do |page|
      joined_path = ['', page.path_nodes].join('/'.freeze)
      Page.update_all({:joined_path => joined_path}, {:id => page.id})
    end
  end

  def self.down
    remove_column :pages, :joined_path
  end
end
