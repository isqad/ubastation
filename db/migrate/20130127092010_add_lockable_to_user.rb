class AddLockableToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :failed_attempts, :integer, :default => 0 # Only if lock strategy is :failed_attempts
    # add_column :users, :unlock_token, :string # Only if unlock strategy is :email or :both
    add_column :users, :locked_at, :datetime
  end

  def self.down
    remove_column :users, :failed_attempts, :locked_at
  end
end
