class CreateMemberships < ActiveRecord::Migration
  def change
    create_table :memberships do |t|
      t.integer :group_id
      t.integer :member_id
    end

    add_index :memberships, :group_id
  end
end
