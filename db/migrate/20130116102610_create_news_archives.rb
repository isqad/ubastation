class CreateNewsArchives < ActiveRecord::Migration
  def change
    create_table :news_archives do |t|
      t.string :name, :null => false
      t.integer :parent_id
      t.integer :lft
      t.integer :rgt
      t.integer :depth

      t.timestamps
    end

    add_index :news_archives, :parent_id
    add_index :news_archives, :lft
    add_index :news_archives, :rgt

    change_table :news do |t|
      t.references :news_archive
    end

    add_index :news, :news_archive_id
  end
end
