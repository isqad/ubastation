class AddPublishedToBanners < ActiveRecord::Migration
  def self.up
    add_column :banners, :published, :boolean, :default => true, :null => false
  end

  def self.down
    remove_column :banners, :published
  end
end
