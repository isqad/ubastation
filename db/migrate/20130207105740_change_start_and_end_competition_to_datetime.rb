class ChangeStartAndEndCompetitionToDatetime < ActiveRecord::Migration
  def self.up
    change_column :competitions, :start_competition, :datetime
    change_column :competitions, :end_competition, :datetime
  end

  def self.down
    change_column :competitions, :start_competition, :date
    change_column :competitions, :end_competition, :date
  end
end
