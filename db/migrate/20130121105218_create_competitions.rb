class CreateCompetitions < ActiveRecord::Migration
  def change
    create_table :competitions do |t|
      t.string :name, :null => false
      t.attachment :cover
      t.string :preview, :null => false
      t.text :body
      t.date :start, :null => false
      t.date :end, :null => false
      t.string :place
      t.string :organizations
      t.string :disciplines
      t.string :slug

      t.timestamps
    end

    add_index :competitions, [:start, :end]
    add_index :competitions, :slug, :unique => true
  end
end
