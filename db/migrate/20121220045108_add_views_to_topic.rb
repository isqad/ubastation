class AddViewsToTopic < ActiveRecord::Migration
  def change
    change_table :topics do |t|
      t.integer :views, :default => 0
    end
  end
end
