# coding: utf-8
class AddYearFieldToPhotoAlbums < ActiveRecord::Migration
  def self.up
    add_column :photo_albums, :year, :integer

    execute <<-SQL
      UPDATE photo_albums SET "year" = EXTRACT(YEAR FROM photo_albums.created_at);
    SQL

    execute <<-SQL
      ALTER TABLE photo_albums ALTER COLUMN "year" SET NOT NULL;
    SQL
  end

  def self.down
    remove_column :photo_albums, :year
  end
end
