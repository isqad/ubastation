class AddMetaToPhotoableEntities < ActiveRecord::Migration
  def self.up
    add_column :profiles, :avatar_meta, :text
    add_column :competitions, :cover_meta, :text
    add_column :news, :photo_meta, :text
    add_column :photos, :photo_meta, :text
  end

  def self.down
    remove_column :profiles, :avatar_meta
    remove_column :competitions, :cover_meta
    remove_column :news, :photo_meta
    remove_column :photos, :photo_meta
  end
end
