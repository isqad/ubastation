class CreateTopPhotos < ActiveRecord::Migration
  def self.up
    rename_column :photos, :top_once, :top_at_once
    rename_column :photos, :top, :in_top

    create_table :top_photos do |t|
      t.integer :photo_id
      t.boolean :show_in_top, :default => false, :null => true
      t.datetime :expired_at
      t.timestamps
    end
    add_index :top_photos, :photo_id
  end


  def self.down
    rename_column :photos, :top_at_once, :top_once
    rename_column :photos, :in_top, :top
    drop_table :top_photos
  end
end
