class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|

      t.string :title, :null => false
      t.string :slug
      t.string :anonce, :null => false
      t.text :body
      t.attachment :photo
      t.boolean :published, :default => true, :null => false


      t.timestamps
    end

    add_index :news, :created_at
    add_index :news, :slug, :unique => true
  end
end
