class ChangePhotoAlbumCover < ActiveRecord::Migration

  def change
    change_table :photo_albums do |t|
      t.boolean :published, :default => false, :null => false
      t.integer :coverable_id
      t.string :coverable_type
    end

    Paperclip::Schema::COLUMNS.keys.each do |column|
      remove_column :photo_albums, "cover_#{column}"
    end

    add_index :photo_albums, :coverable_id
    add_index :photo_albums, :coverable_type
    add_index :photo_albums, :created_at
  end
end
