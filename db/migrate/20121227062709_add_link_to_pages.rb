class AddLinkToPages < ActiveRecord::Migration
  def change
    change_table :pages do |t|
      t.string :link
    end
  end
end
