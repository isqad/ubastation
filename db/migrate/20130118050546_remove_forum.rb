class RemoveForum < ActiveRecord::Migration
  def change
    drop_table :forums
    remove_column :categories, :forum_id
    remove_column :categories, :description
    remove_column :categories, :num_posts
    rename_column :topics, :category_id, :forum_id
    rename_table :categories, :forums
  end
end
