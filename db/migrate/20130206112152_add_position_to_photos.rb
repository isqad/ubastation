class AddPositionToPhotos < ActiveRecord::Migration
  def change
    change_table :photos do |t|
      t.integer :position, :default => 1, :null => false
    end
  end
end
