class CreateViews < ActiveRecord::Migration
  def change
    create_table :views do |t|
      t.references :user
      t.references :viewable, :polymorphic => true
      t.integer :count, :default => 0

      t.timestamps
    end

    add_index :views, :user_id
    add_index :views, :viewable_id
    add_index :views, :updated_at

    remove_column :topics, :views
  end
end
