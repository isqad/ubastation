class RemoveTableArchives < ActiveRecord::Migration
  def change
    remove_column :photo_albums, :album_archive_id
    drop_table :album_archives
    remove_column :news, :news_archive_id
    drop_table :news_archives
  end
end
