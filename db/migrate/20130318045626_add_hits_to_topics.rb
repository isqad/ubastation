class AddHitsToTopics < ActiveRecord::Migration
  def change
    change_table :topics do |t|
      t.integer :hits, :null => false, :default => 0
    end
  end
end
