class CreateDocuments < ActiveRecord::Migration
  def self.up
    create_table :documents do |t|
      t.attachment :file
      t.text :description
      t.references :user
      t.timestamps
    end

    add_index :documents, :user_id

    # association with forum posts
    create_table :documents_posts, :id => false do |t|
      t.references :post, :document
    end

    add_index :documents_posts, [ :post_id, :document_id ], :unique => true
  end

  def self.down
    drop_table :documents
    drop_table :documents_posts
  end
end
