class AddPublishCommentsToProfiles < ActiveRecord::Migration
  def change
    change_table :profiles do |t|
      t.boolean :publish_comments, :null => false, :default => false
    end
  end
end
