class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.references :user
      t.string :name
      t.attachment :avatar
      t.date :birthday
      t.text :bio
      t.timestamps
    end

    add_index :profiles, :user_id
  end
end
