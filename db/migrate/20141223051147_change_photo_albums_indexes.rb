# coding: utf-8
class ChangePhotoAlbumsIndexes < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      END;
    SQL

    execute <<-SQL
      DROP INDEX IF EXISTS index_photo_albums_on_coverable_id;
    SQL

    execute <<-SQL
      END;
    SQL

    execute <<-SQL
      DROP INDEX IF EXISTS index_photo_albums_on_coverable_type;
    SQL

    execute <<-SQL
      END;
    SQL

    execute <<-SQL
      CREATE INDEX index_photo_albums_on_coverable_type_and_coverable_id
      ON photo_albums (coverable_type, coverable_id);
    SQL
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
