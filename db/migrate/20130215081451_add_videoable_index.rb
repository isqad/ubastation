class AddVideoableIndex < ActiveRecord::Migration
  def self.up
    remove_index :videos, :column => :videoable_id
    add_index :videos, [:videoable_id, :videoable_type], :name => 'videos_videoable_idx'
  end

  def self.down
    remove_index :videos, :name => 'videos_videoable_idx'
    add_index :videos, :videoable_id
  end
end
