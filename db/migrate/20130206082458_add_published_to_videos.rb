class AddPublishedToVideos < ActiveRecord::Migration
  def change
    change_table :videos do |t|
      t.boolean :published, :default => false, :null => false
    end
  end
end
