class AddCountMessagesToCategories < ActiveRecord::Migration
  def change
    change_table :categories do |t|
      t.integer :num_posts, :default => 0, :null => false
    end
  end
end
