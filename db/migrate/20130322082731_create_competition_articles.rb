class CreateCompetitionArticles < ActiveRecord::Migration
  def change
    create_table :competition_articles do |t|
      t.string :title, :null => false
      t.text :body, :null => false
      t.string :link
      t.boolean :show_comments, :null => false, :default => true
      t.integer :competition_id, :null => false
      t.string :slug, :null => false
      t.timestamps
    end
    add_index :competition_articles, :competition_id
    add_index :competition_articles, :slug, :unique => true
  end
end
