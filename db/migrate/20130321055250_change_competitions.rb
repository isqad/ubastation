class ChangeCompetitions < ActiveRecord::Migration
  def self.up
    change_column :competitions, :preview, :text, :null => false
    add_attachment :competitions, :thumbnail_image
    add_column :competitions, :thumbnail_image_meta, :text
    add_column :competitions, :thumbnail_video, :text
    add_column :competitions, :start_at, :datetime
    add_column :competitions, :video_link, :string
  end

  def self.down
    change_column :competitions, :preview, :string, :null => true
    remove_attachment :competitions, :thumbnail_image
    remove_column :competitions, :thumbnail_image_meta
    remove_column :competitions, :thumbnail_video
    remove_column :competitions, :start_at
    remove_column :competitions, :video_link
  end
end
