class CreateSliders < ActiveRecord::Migration
  def change
    create_table :sliders do |t|
      t.string :name
      t.attachment :slide
      t.text :slide_meta
      t.integer :position, :default => 0, :null => false
      t.timestamps
    end
  end
end
