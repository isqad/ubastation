class CreateAlbumArchives < ActiveRecord::Migration
  #def migrate(direction)
  #  super
    # Create a root
  #  AlbumArchive.create!(:name => 'root') if direction == :up
  #end

  def change
    create_table :album_archives do |t|
      t.string :name
      t.integer :parent_id
      t.integer :lft
      t.integer :rgt
      t.integer :depth
    end
    add_index :album_archives, :parent_id
    add_index :album_archives, :lft
    add_index :album_archives, :rgt

    change_table :photo_albums do |t|
      t.references :album_archive
    end

    add_index :photo_albums, :album_archive_id
  end
end
