class AddSlugToPhotoAlbums < ActiveRecord::Migration
  def change
    change_table :photo_albums do |t|
      t.string :slug
    end

    add_index :photo_albums, :slug, :unique => true
  end
end