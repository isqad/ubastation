class AddDraftFieldToPages < ActiveRecord::Migration
  def change
    change_table :pages do |t|
      t.boolean :draft, :default => false, :null => false
    end
  end
end
