class AddGenderOnlineForumSignToUser < ActiveRecord::Migration
  def change
    change_table :profiles do |t|
      t.string :gender
      t.string :forum_sign
      t.boolean :show_online, :default => true, :null => false
    end
  end
end
