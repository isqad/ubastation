class AddThumbnailToVideo < ActiveRecord::Migration

  def self.up

    remove_column :videos, :thumbnail_medium
    remove_column :videos, :thumbnail_small

    add_attachment :videos, :thumbnail
    add_column :videos, :thumbnail_meta, :text

    # Download thumbnail
    Video.find_each do |v|

      begin
        attach = URI.parse(v.thumbnail_large)
      rescue
        attach = nil
      end

      v.update_attribute(:thumbnail, attach) if attach

    end

    remove_column :videos, :thumbnail_large
  end

  def self.down
    remove_attachment :videos, :thumbnail
    remove_column :videos, :thumbnail_meta

    add_column :videos, :thumbnail_large, :string
    add_column :videos, :thumbnail_medium, :string
    add_column :videos, :thumbnail_small, :string
  end

end
