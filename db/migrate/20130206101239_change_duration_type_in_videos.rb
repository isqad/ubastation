class ChangeDurationTypeInVideos < ActiveRecord::Migration

  def self.up
    add_column :videos, :duration_time, :time

    Video.find_each do |v|
      v.update_attribute(:duration_time, Time.at(v.duration.to_i).utc.strftime("%H:%M:%S"))
    end

    remove_column :videos, :duration
    rename_column :videos, :duration_time, :duration
  end

  def self.down
    change_column :videos, :duration, :string
  end
end
