class DropTopPhotos < ActiveRecord::Migration
  def self.up
    drop_table :top_photos
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
