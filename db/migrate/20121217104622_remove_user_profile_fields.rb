class RemoveUserProfileFields < ActiveRecord::Migration
  def change
    remove_column :users, :avatar_file_name
    remove_column :users, :avatar_content_type
    remove_column :users, :avatar_file_size
    remove_column :users, :avatar_updated_at
    remove_column :users, :name
    remove_column :users, :birthday
    remove_column :users, :bio
  end
end
