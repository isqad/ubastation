class AddPositionToPosts < ActiveRecord::Migration
  def self.up
    add_column :posts, :position, :integer, null: false, default: 1

    execute <<-SQL
      UPDATE posts p SET position = t.real_position
      FROM (
        SELECT id, row_number() OVER (PARTITION BY topic_id ORDER BY created_at, id) AS real_position
        FROM posts) t
      WHERE p.id = t.id;
    SQL

    execute <<-SQL
      ALTER TABLE posts ADD CONSTRAINT topic_id_position UNIQUE (topic_id, "position");
    SQL

    execute <<-SQL
      DROP INDEX index_posts_on_topic_id;
      DROP INDEX index_posts_on_state;
    SQL
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
