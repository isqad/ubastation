class CreatePhotoAlbums < ActiveRecord::Migration
  def change
    create_table :photo_albums do |t|
      t.attachment :cover
      t.string :name
      t.text :description
      t.timestamps
    end
  end
end
