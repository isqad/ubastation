class AddLastRequestAtToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.datetime :last_request_at
    end
  end
end
