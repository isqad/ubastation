class AddViewableIndex < ActiveRecord::Migration
  def self.up
    add_index :videos, :video_id
    remove_index :views, :column => :viewable_id
    add_index :views, [:viewable_id, :viewable_type], :name => 'views_viewable_idx'
  end

  def self.down
    remove_index :views, :name => 'views_viewable_idx'
    add_index :views, :viewable_id
    remove_index :videos, :column => :video_id
  end
end
