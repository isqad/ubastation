class AddNumberPostsToForums < ActiveRecord::Migration
  def change
    add_column :forums, :posts_per_page, :integer
  end
end
