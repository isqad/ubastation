class ChangeCompetitionToCalendar < ActiveRecord::Migration
  def self.up
    #copy from competitions to calendars
    Competition.find_each do |c|
      Calendar.create(:title => c.name,
                      :start_at => c.start_competition,
                      :finish_at => c.end_competition,
                      :place => c.place,
                      :organizations => c.organizations,
                      :disciplines => c.disciplines)
    end

    rename_column :competitions, :name, :title
    remove_column :competitions, :body
    remove_column :competitions, :start_competition
    remove_column :competitions, :end_competition
    remove_column :competitions, :place
    remove_column :competitions, :organizations
    remove_column :competitions, :disciplines
    Paperclip::Schema::COLUMNS.keys.each do |column|
      remove_column :competitions, "cover_#{column}"
    end
    remove_column :competitions, :cover_meta
  end
end
