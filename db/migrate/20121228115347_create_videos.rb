class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :link
      t.string :video_id
      t.string :title
      t.string :provider
      t.text :description
      t.integer :width
      t.integer :height
      t.text :embed_code
      t.string :embed_url
      t.string :thumbnail_small
      t.string :thumbnail_medium
      t.string :thumbnail_large
      t.string :duration
      t.references :videoable, :polymorphic => true
      t.timestamps
    end

    add_index :videos, :videoable_id
    add_index :videos, :created_at
  end
end
