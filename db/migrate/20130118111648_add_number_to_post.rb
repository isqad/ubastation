class AddNumberToPost < ActiveRecord::Migration
  def change
    change_table :posts do |t|
      t.integer :number, :default => 1, :null => false
    end
  end
end
