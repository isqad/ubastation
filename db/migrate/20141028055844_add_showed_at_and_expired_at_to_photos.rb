# coding: utf-8
class AddShowedAtAndExpiredAtToPhotos < ActiveRecord::Migration
  def self.up
    add_column :photos, :showed_at, :datetime
    add_column :photos, :expired_at, :datetime

    execute <<-SQL
      UPDATE photos SET showed_at = NOW(), expired_at = NOW() + INTERVAL '4 hours';
    SQL
  end

  def self.down
    remove_column :photos, :showed_at
    remove_column :photos, :expired_at
  end
end
