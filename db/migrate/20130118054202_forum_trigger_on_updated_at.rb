class ForumTriggerOnUpdatedAt < ActiveRecord::Migration
  def self.up
    # Create trigger
    execute <<-_SQL
      DROP FUNCTION IF EXISTS forum_topic_updated_at () CASCADE;

      CREATE OR REPLACE FUNCTION forum_topic_updated_at () RETURNS TRIGGER AS $$
      BEGIN
      IF TG_OP = 'INSERT' THEN
      UPDATE topics SET updated_at = NEW.created_at WHERE id = NEW.id;
      END IF;
      RETURN NEW;
      END;
      $$ LANGUAGE plpgsql;

      CREATE TRIGGER forum_updated_at AFTER INSERT ON posts
      FOR EACH ROW EXECUTE PROCEDURE forum_topic_updated_at ();
    _SQL
  end

  def self.down
    execute "DROP FUNCTION IF EXISTS forum_topic_updated_at () CASCADE;"
  end
end
