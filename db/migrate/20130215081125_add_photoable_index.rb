class AddPhotoableIndex < ActiveRecord::Migration
  def self.up
    remove_index :photos, :column => :photoable_id

    add_index :photos, [:photoable_id, :photoable_type], :name => 'photos_photoable_idx'
  end

  def self.down
    remove_index :photos, :name => 'photos_photoable_idx'
    add_index :photos, :photoable_id
  end
end
