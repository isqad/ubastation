class AddStateToTopic < ActiveRecord::Migration
  def up
    execute <<-SQL.strip_heredoc
      CREATE TYPE topic_states AS ENUM ('accepted', 'pending', 'rejected')
    SQL

    execute <<-SQL.strip_heredoc
      ALTER TABLE topics ADD COLUMN state topic_states DEFAULT 'pending'::topic_states
    SQL

    if Rails.env.production?
      say 'Please run `bundle exec rake forum:topics:fill_state`'
    else
      execute 'COMMIT'

      Rake::Task['forum:topics:fill_state'].invoke
    end
  end

  def down
    execute <<-SQL
      ALTER TABLE topics DROP COLUMN state
    SQL
  end
end
