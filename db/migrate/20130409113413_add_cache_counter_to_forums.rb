class AddCacheCounterToForums < ActiveRecord::Migration
  def self.up
    add_column :forums, :topics_count, :integer, :default => 0

    Forum.reset_column_information
    Forum.find_each do |t|
      Forum.update_counters t.id, :topics_count => t.topics.length
    end
  end

  def self.down
    remove_column :forums, :topics_count
  end
end
