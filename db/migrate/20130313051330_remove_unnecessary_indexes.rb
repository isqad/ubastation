class RemoveUnnecessaryIndexes < ActiveRecord::Migration
  def self.up
    remove_index :views, :updated_at
    remove_index :videos, :created_at
    remove_index :photo_albums, :created_at
    remove_index :news, :created_at
    remove_index :discussions, :updated_at

  end

  def self.down
    add_index :views, :updated_at
    add_index :videos, :created_at
    add_index :photo_albums, :created_at
    add_index :news, :created_at
    add_index :discussions, :updated_at
  end
end
