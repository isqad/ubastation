class CreateSubscriptionForums < ActiveRecord::Migration
  def change
    create_table :subscription_forums do |t|
      t.integer :user_id, :null => false
      t.integer :forum_id, :null => false

      t.timestamps
    end

    add_index :subscription_forums, [ :user_id, :forum_id ], :unique => true
  end
end
