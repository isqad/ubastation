class AddOmniauthSecretToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :omniauth_secret
    end
  end
end
