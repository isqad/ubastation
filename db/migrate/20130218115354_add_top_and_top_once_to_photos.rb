class AddTopAndTopOnceToPhotos < ActiveRecord::Migration
  def self.up
    add_column :photos, :top, :boolean, :default => false, :null => false
    add_column :photos, :top_once, :boolean, :default => false, :null => false
  end

  def self.down
    remove_column :photos, :top
    remove_column :photos, :top_once
  end
end
