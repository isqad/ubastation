class AddMenuToPages < ActiveRecord::Migration
  def change
    change_table :pages do |t|
      t.boolean :in_menu, :default => true, :null => false
    end
  end
end
