class AddStateToPosts < ActiveRecord::Migration
  def self.up
    add_column :posts, :state, :string, :default => 'approved'
    add_index :posts, :state
  end

  def self.down
    remove_column :posts, :state
  end
end
