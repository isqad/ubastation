class AddSlugToPage < ActiveRecord::Migration
  def change
    change_table :pages do |t|
      t.string :slug
    end

    add_index :pages, :slug, :unique => true
  end
end
