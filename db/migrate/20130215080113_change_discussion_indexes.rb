class ChangeDiscussionIndexes < ActiveRecord::Migration
  def self.up
    remove_index :discussions, :column => :discussable_id
    add_index :discussions, [ :discussable_id, :discussable_type ], :name => 'discussions_discussable_idx'
  end

  def self.down
    remove_index :discussions, :name => 'discussions_discussable_idx'
    add_index :discussions, :discussable_id
  end
end
