class RemoveNameFromProfileToUser < ActiveRecord::Migration
  def change
    remove_column :profiles, :name
    add_column :users, :name, :string
  end
end
