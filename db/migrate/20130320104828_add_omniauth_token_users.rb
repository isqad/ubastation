class AddOmniauthTokenUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :omniauth_token, :string
  end

  def self.down
    remove_column :users, :omniauth_token
  end
end
