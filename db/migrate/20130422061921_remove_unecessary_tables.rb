class RemoveUnecessaryTables < ActiveRecord::Migration
  def self.up
    drop_table :documents
    drop_table :movies
    drop_table :images
  end

  def self.down
  end
end
