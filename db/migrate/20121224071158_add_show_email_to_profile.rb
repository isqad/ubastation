class AddShowEmailToProfile < ActiveRecord::Migration
  def change
    change_table :profiles do |t|
      t.boolean :show_email, :default => false, :null => false
    end

  end
end
