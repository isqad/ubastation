class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|

      t.string :name
      t.string :link
      t.attachment :banner
      t.text :banner_meta
      t.integer :clicks, :default => 0, :null => false
      t.integer :max_clicks, :default => 0, :null => false
      t.string :place
      t.integer :views, :default => 0, :null => false


      t.timestamps
    end
  end
end
