# coding: utf-8

module Extensions
  module Ckeditor
    module ApplicationController
      extend ActiveSupport::Concern

      included do
        def ckeditor_pictures_scope(options = {})
          user_params = signed_in? ? {:assetable_id => current_user.id} : {}
          ckeditor_filebrowser_scope(options.merge(user_params))
        end
      end
    end
  end
end
