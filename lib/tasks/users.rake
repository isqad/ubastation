# encoding: utf-8
namespace :users do
  desc 'Инициализация пользовательских полей'
  task :initialize => :environment do
    User.all.each do |user|
      last_request = user.last_request_at.present? ? user.last_request_at.utc.to_i : nil
      Redis.current.hset('users_online', user.id, last_request)
    end
  end

  desc 'Сброс накопленной статистики по пользователям в БД'
  task :dump => :environment do
    stats = Redis.current.hgetall('users_online')
    stats.each do |user_id, last_request|
      user_id = user_id.to_i
      user = User.find_by_id(user_id)
      next if user.blank?
      last_request_at = last_request.present? ? Time.at(last_request.to_i).utc : nil
      User.update_all({:last_request_at => last_request_at}, {:id => user_id})
    end
  end
end
