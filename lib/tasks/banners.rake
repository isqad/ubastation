# encoding: utf-8
namespace :banners do
  desc 'Инициализация полей баннеров'
  task :initialize => :environment do
    Banner.all.each do |banner|
      # TODO: исправить
      Redis.current.hset('banners_views', banner.id, banner.views)
      Redis.current.hset('banners_clicks', banner.id, banner.clicks)
      place_ids = Redis.current.hget('banners', banner.place).to_s.split(',').map(&:to_i)
      Redis.current.hset('banners', banner.place, (place_ids << banner.id).join(',')) unless place_ids.include? banner.id
    end
  end
end
