namespace :forum do
  namespace :topics do
    task fill_state: :environment do
      logger = Logger.new(STDOUT)
      logger.formatter = Logger::Formatter.new

      batch_size = Integer(ENV.fetch('BATCH_SIZE', 5_000))

      min_id, max_id = ::Topic.connection.select_one(<<-SQL).values.map(&:to_i)
        SELECT MIN(id), MAX(id)
        FROM topics
        WHERE state IS NULL
      SQL

      logger.info "Min: #{min_id} Max: #{max_id}."

      while min_id < max_id
        next_id = min_id + batch_size

        ::Topic.connection.execute(<<-SQL)
          UPDATE topics SET state = '#{Topic::STATE_ACCEPTED}'::topic_states WHERE state IS NULL
        SQL

        logger.info "Processed #{min_id} to #{next_id - 1}."

        min_id = next_id
      end

      logger.info 'Finish'

      ::Topic.connection.execute(<<-SQL)
        ALTER TABLE topics ALTER COLUMN state SET NOT NULL
      SQL
    end
  end
end
