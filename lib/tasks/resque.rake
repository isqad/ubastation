require 'erb'
require 'yaml'
require 'resque/tasks'
require 'resque/scheduler/tasks'

task 'resque:setup' => :environment do
  ENV['QUEUE'] ||= '*'
  Resque.before_fork = proc { ActiveRecord::Base.establish_connection }
end

namespace :resque do
  task :conf => :environment do
    template = ERB.new(File.read(Rails.root.join('config', 'god.erb')))
    File.write(config_file, template.result(binding))
  end

  task :start => :environment do
    if god_running?
      puts `#{god} start resque`
    else
      puts `#{god} -c #{config_file} -P #{pid_file} -l #{log_file}`
    end
  end

  task :stop => :environment do
    if god_running?
      puts `#{god} stop resque`
    end
  end

  task :restart => :environment do
    Rake::Task['resque:stop'].invoke
    Rake::Task['resque:start'].invoke
  end

  task :setup_schedule => :environment do
    require 'resque-scheduler'

    Resque.schedule = YAML.load_file(Rails.root.join('config', 'resque_schedule.yml'))
  end

  private

  def god
    `which god`.strip
  end

  def god_running?
    File.exists?(pid_file) && Process.kill(0, File.read(pid_file).to_i)
  rescue Errno::ESRCH
    false
  rescue Errno::EPERM
    true
  end

  def pid_file
    Rails.root.join('tmp/pids/resque-god.pid').to_s
  end

  def log_file
    Rails.root.join('log/resque.log').to_s
  end

  def config_file
    Rails.root.join('config/resque.god').to_s
  end
end
