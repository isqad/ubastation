require 'resque_web'

Uba::Application.routes.draw do
  default_url_options host: Rails.env.production? ? 'ubastation.ru' : 'localhost:3000'

  mount Ckeditor::Engine => '/ckeditor'

  ActiveAdmin.routes(self)

  mount_activeadmin_settings

  devise_for :admin_users, ActiveAdmin::Devise.config

  devise_for :users, :controllers => { :registrations => 'registrations',
                                       :sessions => 'sessions',
                                       :passwords => 'passwords',
                                       :omniauth_callbacks => 'omniauth_callbacks' },
                    :path => 'user',
                    :path_names => { :sign_in => 'login', :sign_out => 'logout', :sign_up => 'register' }

  # Current User profile
  resource :profile, only: :show

  # User page
  resources :users, only: :show

  # Forums
  resources :forums, only: [:index, :show] do
    member do
      post 'subscribe'
      delete 'unsubscribe'
    end

    resources :topics, except: :index do
      member do
        post 'subscribe'
        delete 'unsubscribe'
        put 'close'
        put 'open'
        put 'pin'
        put 'unpin'
      end

      resources :posts, except: [:index, :show] do
        member do
          post 'answer'
          post 'cite'
          put 'complaint'
          put 'spam'
          put 'approve'
        end
      end
    end
  end

  # Media
  namespace :media do
    constraints(:year => /[0-9]{4}/) do

      resources :albums, only: [:index, :show], controller: 'photo_albums', path: '(:year)' do
        resources :photos, only: [:index, :show] do
          resources :comments, only: [:index, :new, :create, :destroy]
          # Top photo (on main page)
          member do
            get 'top'
          end
        end

        resources :videos, only: [:index, :show] do
          resources :comments, only: [:index, :new, :create, :destroy]
        end
      end
    end
  end

  # News
  namespace :news do
    constraints(:year => /[0-9]{4}/, :month => /0?[0-9]{1,2}/) do
      match 'archive/:year(/:month)' => 'news#archive', via: :get, as: :archives

      resources :news, only: :show, path: ':year/:month', as: :tiding do
        resources :photos, only: :show do
          resources :comments, only: [:index, :new, :create, :destroy]
          # Top photo from news (on main page)
          member do
            get 'top'
          end
        end

        resources :videos, only: :show do
          resources :comments, only: [:index, :new, :create, :destroy]
        end

        resources :comments, only: [:index, :new, :create, :destroy]
      end
    end

    root to: 'news#index', via: :get, as: :index
  end

  # Competitions calendar
  get 'calendar(/:year)' => 'calendars#index', as: :calendar, constraints: {year: /[0-9]{4}/}
  # Competitions
  resources :competitions, path: 'competitions(/:year)', only: [:index, :show], constraints: { year: /[0-9]{4}/ } do
    resources :articles, only: :show, controller: 'competition_articles' do
      resources :comments, controller: 'competition_comments', only: [:index, :new, :create, :destroy]
    end
  end

  resource :roadmap, :only => :show

  get 'search' => 'search#show', as: :search

  resources :discussions, only: [:index, :show, :new, :create, :destroy] do
    resources :messages, :only => [:create, :index]
    resources :speakers, :module => :inboxes, :only => [:create, :destroy]
    member do
      post 'leave'
    end
  end

  # Banners
  resources :banners, :only => :show

  resources :pages, only: [] do
    resources :comments, only: [:new, :create, :destroy, :index]
  end

  mount ResqueWeb::Engine => '/resque_web'
  mount Goatmail::App, at: '/letter_opener' if Rails.env.development?

  # Pages
  get '/*path' => 'pages#show', as: :page, constraints: {path: /^[^.]+$|.*\.(?!(js|css|jpg|jpeg|gif|zip|rar|doc|docx|ppt|pptx|png|gz|xls|xlsx)$)([^.]+$)$/}

  # Home page
  root to: 'pages#home'
end
