# Unicorn::WorkerKiller setting
# https://github.com/kzk/unicorn-worker-killer
WORKER_MEMORY_LIMIT_MIN = 630 * 1024 * 1024 # 630 * 1024**2
WORKER_MEMORY_LIMIT_MAX = 680 * 1024 * 1024 # 680 * 1024**2
# Unicorn::OobGC settings
# http://unicorn.bogomips.org/Unicorn/OobGC.html
#
# number of requests which after processing, the garbage-collected
# OOBGC enabled when WORKER_OOBGC > 0
WORKER_OOBGC = 10
