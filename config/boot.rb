require 'rubygems'

# Set up gems listed in the Gemfile.
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)

require 'bundler/setup' if File.exists?(ENV['BUNDLE_GEMFILE'])

require "dotenv"
rails_env = ENV.fetch('RAILS_ENV', 'development')
env_files = %W(.env .env.#{rails_env} .env.local).map { |env_file| File.expand_path("../../#{env_file}", __FILE__) }
Dotenv.overload(*env_files)
