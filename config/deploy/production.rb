set :stage, :production
set :rails_env, 'production'
server 'chromium.locum.ru', user: 'hosting_uba', roles: %w{app db web}
