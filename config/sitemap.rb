# coding: utf-8
Sitemap::Generator.instance.load :host => 'ubastation.ru' do
  path :root, :priority => 1
  resources :page,
            :objects => proc { Page.fast_menu.without_main },
            :skip_index => true,
            :priority => 1,
            :change_frequency => 'weekly'
end
