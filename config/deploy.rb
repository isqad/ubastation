lock '3.7.2'

set :application, 'uba-site'

set :repo_url, 'git@gitlab.com:isqad/ubastation.git'

set :deploy_to, "/home/hosting_uba/projects/uba-site"
set :ssh_options, {forward_agent: true, user: 'hosting_uba'}
set :rvm_type, :system
set :rvm_ruby_version, '2.1'
set :scm, :git
set :branch, ENV['REVISION'] || ENV['BRANCH_NAME'] || 'master'
set :log_level, :debug
set :conditionally_migrate, true
set :keep_assets, false
set :bundle_without, 'development test'
set :bundle_jobs, 7
set :rails_env, :production

set :rails_assets_groups, :assets

SSHKit.config.command_map[:rake] = 'bundle exec rake'
SSHKit.config.command_map[:rails] = 'bundle exec rails'

set :format, :airbrussh
module Airbrussh::Colors
  def blue(string)
    "\e[0;36;49m#{string}\e[0m"
  end
end

set :linked_files, %w(config/database.yml
                      config/application.yml
                      config/redis.yml
                      config/resque.yml
                      config/resque_schedule.yml
                      config/redis_cache.yml
                      .env)

set :linked_dirs, %w(log
                     tmp/pids
                     tmp/cache
                     tmp/sockets
                     public/ckeditor_assets
                     public/system
                     public/assets
                     db/sphinx/production)

namespace :deploy do
  desc 'Setup backbone app'
  task :setup_backbone do
    on roles(:app) do
      execute <<-COMMAND
        rm -rf /home/hosting_uba/projects/uba-site/shared/public/assets/app/ /home/hosting_uba/projects/uba-site/shared/assets/vendor/ &&\
        cp -R /home/hosting_uba/projects/uba-site/current/app/assets/javascripts/app/ /home/hosting_uba/projects/uba-site/shared/public/assets/ &&\
        cp -R /home/hosting_uba/projects/uba-site/current/app/assets/javascripts/vendor/ /home/hosting_uba/projects/uba-site/shared/public/assets/
      COMMAND
    end
  end
  after :publishing, :setup_backbone

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute 'test -e /var/run/unicorn/hosting_uba/uba-site.uba.pid && kill -USR2 $(cat /var/run/unicorn/hosting_uba/uba-site.uba.pid)'
    end
  end
  after :publishing, :restart

  desc 'Resque configure'
  task :resque_conf do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'resque:conf'
        end
      end
    end
  end
  after :publishing, :resque_conf

  desc 'Resque restart'
  task :resque_restart do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'resque:restart'
        end
      end
    end
  end
  after :publishing, :resque_restart
end
