# encoding: utf-8
deploy_to = '~/projects/uba-site'
rails_root = "#{deploy_to}/current"
pid_file = "#{deploy_to}/shared/pids/unicorn.pid"
socket_file = "#{deploy_to}/shared/sockets/unicorn.sock"
log_file = "#{deploy_to}/log/unicorn.log"
error_log = "#{deploy_to}/log/unicorn.stderr.log"
old_pid = pid_file + '.olbbin'

timeout 30
worker_processes 2
listen socket_file, :backlog => 1024
pid pid_file
stderr_path error_log
stdout_path log_file

preload_app true

GC.copy_on_write_friendly = true if GC.respond_to?(:copy_on_write_friendly=)

before_exec do |_|
  ENV['BUNDLE_GEMFILE'] = "#{rails_root}/Gemfile"
  ENV['RUBY_HEAP_MIN_SLOTS'] = '2500000'
  ENV['RUBY_HEAP_SLOTS_INCREMENT'] = '1000000'
  ENV['RUBY_HEAP_SLOTS_GROWTH_FACTOR'] = '1'
  ENV['RUBY_GC_MALLOC_LIMIT'] = '50000000'
end

before_fork do |server, _|
  # Перед тем, как создать первый рабочий процесс, мастер отсоединяется от базы.
  if defined?(ActiveRecord::Base)
    ActiveRecord::Base.connection.disconnect!
  end

  # Ниже идет магия, связанная с 0 downtime deploy.
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill('QUIT', File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      # someone else did our job for us
    end
  end
end

after_fork do |server, worker|
  # После того как рабочий процесс создан, он устанавливает соединение с базой.
  if defined?(ActiveRecord::Base)
    ActiveRecord::Base.establish_connection
  end
end


