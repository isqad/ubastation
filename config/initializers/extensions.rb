if Rails.env.production?
  ActiveadminSettings::Setting.send(:include, Extensions::ActiveRecord::CachedQueries)
  ActiveadminSettings::Setting.cached_queries_expires_in = 1.day
  ActiveadminSettings::Setting.cached_queries_local_expires_in = 1.day
  ActiveadminSettings::Setting.cached_queries_with_tags = true
end
