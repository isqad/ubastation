# encoding: utf-8
require 'yaml'

redis_config = YAML.load(File.read(Rails.root.join('config', 'redis.yml')))

Redis.current = Redis.new(redis_config[Rails.env])
