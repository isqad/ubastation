require 'resque-scheduler'
require 'resque/scheduler/server'

rails_root = ENV['RAILS_ROOT'] || File.dirname(__FILE__) + '/../..'
rails_env = ENV['RAILS_ENV'] || 'development'

Resque.redis = Redis.current
Resque.schedule = YAML.load_file(rails_root + '/config/resque_schedule.yml')
