crumb :root do
  link I18n.t(:home_page), root_path
end

crumb :pages do |page|
  link page.title, page.nested_path
  if page.parent
    parent :pages, page.parent
  end
end

crumb :news do
  link I18n.t('activerecord.models.news.other'), news_index_path
end

crumb :archive_news do |attrs|
  link(attrs[:year], news_archives_path(attrs[:year])) if attrs[:year]

  if attrs[:month]
    link I18n.t('date.month_names')[attrs[:month].to_i], news_archives_path(attrs[:year], attrs[:month])
  end

  link(attrs[:news].title, attrs[:news].decorate.url) if attrs[:news]

  parent :news
end

crumb :photo_album do |page, album|
  link album.name, media_album_path(album.year, album)
  parent :pages, page
end

crumb :forums do
  link I18n.t('activerecord.models.forum.one'), forums_path
end

crumb :forum do |forum|
  link forum.title, forum_path(forum)
  parent :forums
end

crumb :topic do |forum, topic|
  link topic.title, forum_topic_path(forum, topic)
  parent :forum, forum
end

crumb :edit_post do |forum, topic, post|
  link I18n.t('posts.edit_post_title'), edit_forum_topic_post_path(forum, topic, post)
  parent :topic, forum, topic
end

crumb :sign_in do
  link I18n.t('users.sign_in'), new_user_session_path
end

crumb :sign_up do
  link I18n.t('users.sign_up'), new_user_registration_path
end

crumb :forgot_password do
  link I18n.t('users.forgot_your_password'), new_user_password_path
end

crumb :user do |user|
  link user.name, user_path(user)
end

crumb :search do |path|
  link I18n.t('search'), path
end
