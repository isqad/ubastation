Ubastation.ru
================

[![Build Status](https://travis-ci.org/ubastation/ubastation.svg?branch=master)](https://travis-ci.org/ubastation/ubastation) [![Dependency Status](https://gemnasium.com/ubastation/ubastation.svg)](https://gemnasium.com/ubastation/ubastation)

CMS engine, news, forum, some social elements.

https://ubastation.ru

## Installation

For development please install latest versions of [docker](https://docs.docker.com/engine/installation/) and [docker-compose](https://docs.docker.com/compose/install/).

To run project follow next steps:

```sh
dip provision
dip rails s
```

### Run tests (work in progress)

```sh
RAILS_ENV=test dip provision
RAILS_ENV=test dip rspec
```

### Rails console

```sh
dip rails c
```

### Deployment

```sh
BUNDLE_GEMFILE=gemfiles/deploy dip compose run --rm uba-deploy bundle exec cap production deploy
```
