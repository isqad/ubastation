# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment',  __FILE__)
require ::File.expand_path('../config/unicorn_workers',  __FILE__)

if defined?(Unicorn)
   require 'unicorn/worker_killer'
   use Unicorn::WorkerKiller::Oom,
      WORKER_MEMORY_LIMIT_MIN,
      WORKER_MEMORY_LIMIT_MAX,
      300
  if defined?(WORKER_OOBGC) && WORKER_OOBGC.to_i > 0
    require 'unicorn/oob_gc'
    GC.disable
    use Unicorn::OobGC, WORKER_OOBGC.to_i
  end
end

run Uba::Application
