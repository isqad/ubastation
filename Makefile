RAILS_ENV = test
BUNDLE = RAILS_ENV=${RAILS_ENV} bundle
BUNDLE_OPTIONS = -j 3 --without=production
RAKE = ${BUNDLE} exec rake
RSPEC = ${BUNDLE} exec rspec

all: test

test: configure bundler migrations
	${RSPEC} spec/ 2>&1

configure:
	cp config/database.yml.example config/database.yml
	cp config/application.yml.example config/application.yml
	cp config/resque_schedule.yml.example config/resque_schedule.yml
	cp config/memcached.yml.example config/memcached.yml

bundler:
	if ! gem list bundler -i > /dev/null; then \
	  gem install bundler; \
	fi
	${BUNDLE} install ${BUNDLE_OPTIONS}

migrations:
	${RAKE} db:migrate
