source 'https://rubygems.org'

gem 'dotenv', '2.2.1'
gem 'rails', '3.2.22.4'
gem 'rack-cache', '~> 1.6.1'

gem 'pg', '~> 0.18.4'

gem 'findit'

# Auth
gem 'devise', '~> 2.2.5'
gem 'omniauth', '= 1.1.1'
gem 'omniauth-facebook', '~> 1.5.1'
gem 'omniauth-twitter', '= 0.0.14'
gem 'omniauth-vkontakte', '= 1.2.0'

# Authorization
gem 'cancan', '~> 1.6.10'

# Pagination
gem 'will_paginate', '~> 3.0.5'

gem 'acts_as_list', '~> 0.6.0'

# Breadcrumbs
gem 'gretel', '~> 3.0.8'

# HAML
gem 'haml-rails', '~> 0.4'

# Files attachments
gem 'paperclip', '4.3.6'
gem 'paperclip-meta', '~> 1.2.0'
gem 'mini_magick', '~> 3.6.0'
gem 'carrierwave', '= 0.8.0'

# Admin generator
gem 'activeadmin', '= 0.5.1'
gem 'formtastic', '~> 2.2.1'
gem 'bson_ext', '~> 1.12.3'
gem 'activeadmin-settings', '= 0.4.2'

# Russian translaterizations
gem 'russian', '~> 0.6.0'

# Slugs
gem 'friendly_id', '= 4.0.9'

# wysiwig
gem 'ckeditor', '~> 4.1.2'

# SEO
gem 'seo_meta', '~> 1.3.0'

# RSS parser
gem 'simple-rss', github: 'isqad88/simple-rss'

# HTML parser
gem 'nokogiri', '1.6.8'

# Nested set SQL
gem 'nested_set', '= 1.7.1'
gem 'the_sortable_tree', '= 1.9.4'

# Youtube, Vimeo parser
gem 'video_info', github: 'thibaudgg/video_info'

# Forms
gem 'simple_form', '~> 2.1.1'

# Twitter API
gem 'twitter', '~> 5.11.0'
# Facebook API
gem 'fb_graph', '~> 2.7.12'

# Comments
gem 'acts_as_commentable', '~> 3.0.1'

# Timeline users activity
gem 'public_activity', '~> 1.4.1'

# Autolink
gem 'rails_autolink', '~> 1.1.5'

# Search
gem 'mysql2', '0.4.4'
gem 'thinking-sphinx', '~> 3.1.1'

# Backgorund jobs
gem 'resque', '~> 1.25.2'
gem 'resque-web', '= 0.0.5'
gem 'god', '~> 0.13.4'
gem 'resque_mailer', '~> 2.2.6'
gem 'resque-scheduler', '~> 4.0.0'

# Messages
gem 'inboxes', github: 'isqad88/inboxes'

# Sidebars
gem 'cells', '~> 3.11.1'

# jquery timeago
gem 'rails-timeago', '~> 2.0'

# state machine
gem 'workflow', '~> 0.8.7'

# Application config
gem 'settingslogic', '~> 2.0.9'

gem 'addressable', '~> 2.3.6'
gem 'truncate_html'

gem 'draper', '~> 1.3.1'
gem 'redis', '>= 3.2.0'
gem 'hiredis', '~> 0.6.0'
gem 'readthis'
gem 'tzinfo', '~> 0.3.42'
gem 'sitemap', github: 'viseztrance/rails-sitemap'
gem 'newrelic_rpm', '~> 3.14'
gem 'scout_apm'

gem 'autoprefixer-rails', '~> 5.1.11'
gem 'jbuilder'
gem 'oj'

gem 'test-unit', '~> 3.0'
gem 'turbo-sprockets-rails3'

gem 'recaptcha', require: 'recaptcha/rails'

gem 'lru_redux'

group :production do
  gem 'unicorn', '~> 4.8.3'
  gem 'unicorn-worker-killer', '~> 0.4.3'
end

group :development do
  gem 'letter_opener', '~> 1.1.0'
  gem 'better_errors', '~> 0.7.2'
  gem 'rails_best_practices', '~> 1.15.2'
  gem 'rack-mini-profiler'
  gem 'annotate', '~> 2.6.10'
  gem 'quiet_assets'
  gem 'goatmail'
end

group :test, :development do
  gem 'rspec-rails', '~> 3.3.2'
end

group :test do
  gem 'shoulda-matchers', '~> 2.8.0', require: false
  gem 'factory_girl_rails', '~> 4.0'
  gem 'mock_redis', '~> 0.14.0'
  gem 'simplecov'
  gem 'timecop', '~> 0.7.4'
end

gem 'sass-rails',   '~> 3.2.3'
gem 'therubyracer', '= 0.11.3', :platforms => :ruby
gem 'coffee-script-source', '= 1.4.0'
gem 'coffee-rails', '~> 3.2.2'
gem 'uglifier', '~> 2.7.2'
gem 'jquery-rails', '= 2.1.3'
gem 'bootstrap-sass', '= 2.3.0.0'
