#!/bin/bash -e

openssl genrsa -des3 -passout pass:xxxx -out config/server.pass.key 2048
openssl rsa -passin pass:xxxx -in config/server.pass.key -out config/server.key
rm config/server.pass.key
openssl req -new -key config/server.key -out config/server.csr
openssl x509 -req -sha256 -days 365 -in config/server.csr -signkey config/server.key -out config/server.crt
