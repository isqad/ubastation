FactoryGirl.define do
  factory :post do
    sequence(:body) { |n| "Test body of the post #{n}" }
    association :topic
    association :user
  end
end
