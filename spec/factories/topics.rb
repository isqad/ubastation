FactoryGirl.define do
  factory :topic do
    sequence(:title) { |n| "Test topic#{n}" }
    association :forum
    association :user

    after(:build) do |topic|
      topic.posts = [FactoryGirl.build(:post, topic: topic)]
    end
  end
end
