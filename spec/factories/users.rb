FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "andrew#{n}@yandex.ru" }
    sequence(:password) { |n| "testpassword#{n}" }
    sequence(:name) { |n| "Andrew-#{n}" }
    confirmed_at Time.current

    after(:build) do |user|
      user.profile = FactoryGirl.build(:profile, :user => user)
    end
  end

  factory :admin_user do
    sequence(:email) { |n| "admin#{n}@yandex.ru" }
    sequence(:password) { |n| "testpassword#{n}" }
  end
end
