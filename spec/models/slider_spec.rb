# == Schema Information
#
# Table name: sliders
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  slide_file_name    :string(255)
#  slide_content_type :string(255)
#  slide_file_size    :integer
#  slide_updated_at   :datetime
#  slide_meta         :text
#  position           :integer          default(0), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

require 'spec_helper'

RSpec.describe Slider, type: :model do
  it { expect(described_class.new(name: "test", slide: nil)).not_to be_valid }
end
