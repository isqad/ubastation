# encoding: utf-8
# == Schema Information
#
# Table name: forums
#
#  id              :integer          not null, primary key
#  title           :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  slug            :string(255)
#  topics_count    :integer          default(0)
#  topics_per_page :integer
#  posts_per_page  :integer
#

require 'spec_helper'

describe Forum do

  let(:forum) { build :forum }

  context 'when create' do
    subject { forum }

    it { should be_valid }

    it 'requires a title' do
      forum.title = ''
      forum.should_not be_valid
    end

    it 'rejects titles are too long' do
      forum.title = 'a'*121
      forum.should_not be_valid
    end

    it 'rejects titles are too small' do
      forum.title = 'a'*2
      forum.should_not be_valid
    end

    context 'when non unique' do
      let(:title) { 'Test forum' }

      before do
        forum.title = title
        forum.save!
      end

      subject { build :forum, {:title => title} }

      it { should_not be_valid }
    end
  end

  context 'when associated with topic' do
    it { should respond_to :topics }

    context 'when limited for home page' do
      let(:create_topic_list) { create_list :topic, 12, :forum => forum }

      before do
        forum.save!
        create_topic_list
      end

      pending { forum.topics(true).main(Forum::TOPICS_COUNT_FOR_HOME_PAGE).count.should eq Forum::TOPICS_COUNT_FOR_HOME_PAGE }
    end
  end

  context 'when user subscribes to forum' do
    let(:user) { create :user }

    before do
      forum.save!
      forum.feed_users << user
    end

    subject { forum.feed_users(true) }

    it { should include user }

    it 'rejects add same user' do
      expect {
        forum.feed_users << user
      }.to_not change(subject, :count)
    end

    describe '#subscribe?' do
      subject { forum }

      it { should be_subscribe user }
    end
  end
end
