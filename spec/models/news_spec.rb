# encoding: utf-8
# == Schema Information
#
# Table name: news
#
#  id                 :integer          not null, primary key
#  title              :string(255)      not null
#  slug               :string(255)
#  anonce             :string(255)      not null
#  body               :text
#  photo_file_name    :string(255)
#  photo_content_type :string(255)
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#  published          :boolean          default(TRUE), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  photo_meta         :text
#  external_link      :string(255)
#  year               :integer          not null
#  month              :integer          not null
#

require 'spec_helper'

RSpec.describe News, type: :model do
  let(:news) { build :news }

  describe 'when create' do

    subject { news }

    it { should be_valid }

    it 'requires a photo' do
      expect {
        news.photo = nil
      }.to change(news, :valid?).to(false)
    end

    it 'require a title' do
      expect {
        news.title = ''
      }.to change(news, :valid?).to(false)
    end

    it 'require anonce' do
      expect {
        news.anonce = ''
      }.to change(news, :valid?).to(false)
    end

    it 'does not require body' do
      expect {
        news.body = ''
      }.to_not change(news, :valid?)
    end

    it 'unique' do
      expect {
        create :news, :title => news.title
      }.to change(news, :valid?).to(false)
    end
  end

  describe '#internal?' do
    before do
      news.external_link = "https://www.#{HOST}/pages/12"
    end

    it do
      expect(news.internal?).to be_truthy
    end
  end
end
