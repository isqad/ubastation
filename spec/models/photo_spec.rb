# encoding: utf-8
# == Schema Information
#
# Table name: photos
#
#  id                 :integer          not null, primary key
#  photo_file_name    :string(255)
#  photo_content_type :string(255)
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#  description        :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  photoable_id       :integer
#  photoable_type     :string(255)
#  photo_meta         :text
#  position           :integer          default(1), not null
#  in_top             :boolean          default(FALSE), not null
#  top_at_once        :boolean          default(FALSE), not null
#  showed_at          :datetime
#  expired_at         :datetime
#

require 'spec_helper'

describe Photo do
  describe '.today_photo' do
    context 'when only in_top photos exists' do
      let(:photos_in_top) { create_list :photo, 3, in_top: true }
      let(:start_time) { Time.local(2014, 12, 1, 10, 0, 0).in_time_zone }

      before do
        photos_in_top
        Timecop.travel(start_time)
      end

      after do
        Timecop.return
      end

      it { expect(described_class.today_photo).to eq photos_in_top.first }
      it { expect(described_class.today_photo.expired_at.utc.to_s).to eq described_class.top_photo_expiration.utc.to_s }
      it { expect(described_class.today_photo.showed_at.utc.to_s).to eq Time.now.utc.to_s }
    end

    context 'when change photo' do
      let(:photos_in_top) { create_list :photo, 3, in_top: true }

      context 'when before 18 by local time' do
        let(:start_time) { Time.local(2014, 12, 1, 10, 0, 0).in_time_zone }

        before do
          photos_in_top
          Timecop.travel(start_time)
        end

        it 'changes in_top photo' do
          first = described_class.today_photo

          Timecop.travel(start_time.end_of_day + 4.hours + 5.minutes)

          second = described_class.today_photo

          expect(first).to eq photos_in_top.first
          expect(second).to eq photos_in_top.second
        end

        after do
          Timecop.return
        end
      end

      context 'when after 18 by local time' do
        let(:start_time) { Time.local(2014, 12, 1, 19, 0, 0).in_time_zone }

        before do
          photos_in_top
          Timecop.travel(start_time)
        end

        it 'changes in_top photo' do
          first = described_class.today_photo

          Timecop.travel(start_time.end_of_day + 4.hours + 5.minutes)

          second = described_class.today_photo

          # expect(first).to eq photos_in_top.first
          # expect(second).to eq first

          Timecop.travel(start_time.end_of_day + 1.days + 4.hours + 5.minutes)

          third = described_class.today_photo

          # expect(third).to eq photos_in_top.second
        end

        after do
          Timecop.return
        end
      end
    end

    context 'when top_at_once exists' do
      let!(:photo_top_at_once) { create :photo, top_at_once: true }

      before do
        Timecop.freeze
      end

      after do
        Timecop.return
      end

      it { expect(described_class.today_photo).to eq photo_top_at_once }
      it { expect(described_class.today_photo.showed_at.utc.to_s).to eq Time.now.utc.to_s }
    end
  end
end
