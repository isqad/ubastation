# encoding: utf-8
# == Schema Information
#
# Table name: competition_articles
#
#  id             :integer          not null, primary key
#  title          :string(255)      not null
#  body           :text             not null
#  link           :string(255)
#  show_comments  :boolean          default(TRUE), not null
#  competition_id :integer          not null
#  slug           :string(255)      not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'spec_helper'

describe CompetitionArticle do
  pending "add some examples to (or delete) #{__FILE__}"
end
