# encoding: utf-8
# == Schema Information
#
# Table name: banners
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  link                :string(255)
#  banner_file_name    :string(255)
#  banner_content_type :string(255)
#  banner_file_size    :integer
#  banner_updated_at   :datetime
#  banner_meta         :text
#  clicks              :integer          default(0), not null
#  max_clicks          :integer          default(0), not null
#  place               :string(255)
#  views               :integer          default(0), not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  published           :boolean          default(TRUE), not null
#

require 'spec_helper'

describe Banner, type: :model do
  subject { build :banner }

  it { should be_valid }
end
