# == Schema Information
#
# Table name: profiles
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  avatar_file_name    :string(255)
#  avatar_content_type :string(255)
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#  birthday            :date
#  bio                 :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  show_email          :boolean          default(FALSE), not null
#  gender              :string(255)
#  forum_sign          :string(255)
#  show_online         :boolean          default(TRUE), not null
#  avatar_meta         :text
#  publish_comments    :boolean          default(FALSE), not null
#

require 'spec_helper'

describe Profile do
  pending "add some examples to (or delete) #{__FILE__}"
end
