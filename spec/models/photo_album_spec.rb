# encoding: utf-8
# == Schema Information
#
# Table name: photo_albums
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  description        :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  slug               :string(255)
#  published          :boolean          default(FALSE), not null
#  coverable_id       :integer
#  coverable_type     :string(255)
#  cover_file_name    :string(255)
#  cover_content_type :string(255)
#  cover_file_size    :integer
#  cover_updated_at   :datetime
#

require 'spec_helper'

describe PhotoAlbum do
  it { should validate_presence_of :name }
  it { should validate_length_of(:name).is_at_least(3) }
  it { should validate_length_of(:name).is_at_most(50) }
  it { should validate_uniqueness_of(:name) }
  it { should validate_length_of(:description).is_at_most(4000) }

  describe '.years' do
    let!(:photo_album) { create :photo_album, :created_at => '2014-02-01' }
    let!(:photo_album_old) { create :photo_album, :created_at => '2013-02-01' }

    it { expect(described_class.years).to eq [2014, 2013] }
  end

  describe '.by_year' do
    let!(:photo_album) { create :photo_album, :created_at => '2014-02-01' }
    let!(:photo_album_old) { create :photo_album, :created_at => '2013-02-01' }
    let!(:photo_album_2) { create :photo_album, :created_at => '2014-07-01' }

    it { expect(described_class.by_year(2014)).to eq [photo_album_2, photo_album] }
  end
end
