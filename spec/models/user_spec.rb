# encoding: utf-8
# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  provider               :string(255)
#  uid                    :string(255)
#  name                   :string(255)
#  last_request_at        :datetime
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  failed_attempts        :integer          default(0)
#  locked_at              :datetime
#  omniauth_token         :string(255)
#  omniauth_secret        :string(255)
#

require 'spec_helper'

describe User do

  let!(:test_user) { FactoryGirl.create(:user) }

  it "should be persisted" do
    test_user.should be_persisted
  end

  it "always has a profile" do
    User.new.profile.should_not be_nil
  end

  it "should create a new user" do
    user = User.new(:name => "tester",
                     :email => "test@example.com",
                     :password => Devise.friendly_token[0,20])
    user.should be_valid
  end

  describe '.find_for_oauth' do
    it 'returns user' do
      user = User.find_for_oauth(OmniAuth.config.mock_auth[:facebook])

      user.should be_valid
    end
  end
end
