# == Schema Information
#
# Table name: subscriptions
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  topic_id   :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe Subscription do

  let(:user) { FactoryGirl.create(:user) }
  let(:topic) { FactoryGirl.create(:topic, :user => user) }

  it "should create instance given valid parameters" do
    Subscription.new(:user => user, :topic => topic).should be_valid  
  end

  it "should require a user" do
    Subscription.new(:user => nil, :topic => topic).should_not be_valid  
  end

  it "should require a topic" do
    Subscription.new(:user => user, :topic => nil).should_not be_valid  
  end
end
