# encoding: utf-8
# == Schema Information
#
# Table name: topics
#
#  id          :integer          not null, primary key
#  title       :string(255)
#  user_id     :integer
#  forum_id    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  slug        :string(255)
#  closed      :boolean          default(FALSE), not null
#  pinned      :boolean          default(FALSE), not null
#  closer_id   :integer
#  hits        :integer          default(0), not null
#  posts_count :integer          default(0)
#

require 'spec_helper'

describe Topic do

  let!(:user) { FactoryGirl.create(:user) }
  let!(:forum) { FactoryGirl.create(:forum) }

  let!(:topic) { FactoryGirl.build(:topic, :forum => forum, :user => user) }

  describe 'when creating' do

    it 'create given valid parameters' do
      topic.save!
    end

    it 'has post' do
      topic.save!
      topic.posts.size.should eql(1)
    end

    it 'require a user' do
      topic.user = nil
      topic.should_not be_valid
    end

    it 'has updated_at equal his first post' do
      topic.posts.first.created_at.should eql(topic.updated_at)
    end
  end

  describe 'subscribtion' do
    before(:each) do
      topic.save!
      forum.feed_users << user
    end

    it 'has a user' do
      topic.feed_users(true).should include(user)
    end

    it 'unique list of subscribe users' do
      forum.feed_users << user
      forum.feed_users(true).size.should eql(1)
    end

    it '#subscribe?' do
      topic.feed_users << user
      topic.subscribe?(user).should eql(true)
    end
  end

  describe 'manage subscribtion' do
    before(:each) do
      topic.save!
    end

    it '#add_subscriber' do
      updated = topic.updated_at

      topic.add_subscriber(user)

      topic.feed_users(true).should include(user)
      topic.updated_at.should eql(updated)
    end

    it '#remove_subscriber' do
      topic.feed_users << user

      updated = topic.updated_at

      topic.remove_subscriber(user)

      topic.feed_users(true).should_not include(user)
      topic.updated_at.should eql(updated)
    end
  end

  context 'when registering view' do
    before do
      topic.save!
    end

    context 'when not threshold' do
      before do
        Timecop.freeze((topic.created_at + Topic::HIT_VIEW_THRESHOLD - 1.minutes).in_time_zone)
        topic.register_view
      end

      it { expect(topic.reload.hits).to be_zero }

      after do
        Timecop.return
      end
    end

    context 'when threshold' do
      before do
        Timecop.freeze((topic.created_at + Topic::HIT_VIEW_THRESHOLD + 1.minutes).in_time_zone)
        topic.register_view
      end

      it { expect(topic.reload.hits).to_not be_zero }

      after do
        Timecop.return
      end
    end
  end

  it 'open and close' do
    topic.save!

    topic.close_by!(user)

    topic.reload.should be_closed
    topic.closer.should eql(user)

    topic.open_by!(user)

    topic.reload.should_not be_closed
    topic.reload.closer.should be_nil
  end

  it 'pin and unpin' do
    topic.save!

    topic.pin!

    topic.reload.should be_pinned

    topic.unpin!

    topic.reload.should_not be_pinned
  end
end
