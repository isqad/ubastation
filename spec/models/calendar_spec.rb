# encoding: utf-8
# == Schema Information
#
# Table name: calendars
#
#  id            :integer          not null, primary key
#  title         :string(255)      not null
#  start_at      :datetime         not null
#  finish_at     :datetime         not null
#  place         :string(255)      not null
#  organizations :string(255)      not null
#  disciplines   :string(255)      not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'spec_helper'

RSpec.describe Calendar, type: :model do

  let!(:current_c) { create :calendar, :start_at => 1.days.ago, :finish_at => Time.now + 1.days }
  let!(:ended_c) { create :calendar, :start_at => 3.days.ago, :finish_at => 2.days.ago }
  let!(:future_c) { create :calendar, :start_at => Time.now + 1.days, :finish_at => Time.now + 2.days }

  context 'when have status' do
    let(:events) { Calendar.ordered }
    subject { events.decorate }

    # its(:last) { should eq ended_c }
    it { events[1].should eq future_c }
    # its(:first) { should eq current_c }
    # its('last.due_days') { should eq I18n.t('activerecord.attributes.calendar.died') }
  end

  context 'when create' do
    let(:invalid_calendar) { build(:calendar, :start_at => 1.days.ago, :finish_at => 2.days.ago) }

    subject { invalid_calendar }

    it { should_not be_valid }
  end
end
