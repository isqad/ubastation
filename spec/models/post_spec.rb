# == Schema Information
#
# Table name: posts
#
#  id         :integer          not null, primary key
#  body       :text
#  user_id    :integer
#  topic_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  state      :string(255)      default("approved")
#

require 'spec_helper'

describe Post, type: :model do
  let(:topic) { create :topic }
  let(:post) { create :post, topic: topic }

  describe '#title' do
    it { expect(post.title).to eq post.body }
  end

  describe '#page_for_pagination' do
    context 'when more than 1 page' do
      let!(:posts) { create_list :post, 3, topic: topic }

      subject { posts.last }

      it { expect(subject.page_for_pagination(2)).to eq 2 }
    end

    context 'when 1 page' do
      subject { topic.posts.first }

      it { expect(subject.page_for_pagination(10)).to eq 1 }
    end
  end

  describe '#edited?' do
    let!(:post) { create :post, topic: topic }

    before do
      Timecop.travel(described_class::CHANGED_THRESHOLD.from_now)
    end

    after { Timecop.return }

    it { expect { post.update_attributes!(body: 'Hello') }.to change(post, :edited?).to true }
  end

  describe '#first?' do
    it { expect(topic.posts.ordered.first).to be_first }
  end

  describe '#url_path' do
    it { expect(post.url_path).to match /#post-#{post.id}$/ }
  end
end
