# encoding: utf-8
# == Schema Information
#
# Table name: videos
#
#  id                     :integer          not null, primary key
#  link                   :string(255)
#  video_id               :string(255)
#  title                  :string(255)
#  provider               :string(255)
#  description            :text
#  width                  :integer
#  height                 :integer
#  embed_code             :text
#  embed_url              :string(255)
#  videoable_id           :integer
#  videoable_type         :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  thumbnail_file_name    :string(255)
#  thumbnail_content_type :string(255)
#  thumbnail_file_size    :integer
#  thumbnail_updated_at   :datetime
#  thumbnail_meta         :text
#  published              :boolean          default(FALSE), not null
#  duration               :time
#

require 'spec_helper'

describe Video do
  let!(:attr_youtube) { { :link => 'https://www.youtube.com/watch?v=Pyw9WkZ_yzU' } }
  let!(:attr_vimeo) { { :link => 'http://vimeo.com/30581015' } }
  let!(:attr_dummy) { { :link => 'google.com' } }

  it "should has a uploaded thumbnail from youtube" do
    video = Video.create(attr_youtube)
    expect(video.thumbnail).to be_exists
  end

  it "should has a uploaded thumbnail from vimeo" do
    video = Video.create(attr_vimeo)
    expect(video.thumbnail).to be_exists
  end

  it "should require a link" do
    expect(Video.new(attr_dummy.merge(:link => ''))).not_to be_valid
  end

  it "should require valid URL" do
    expect(Video.new(attr_dummy.merge(:link => 'dummy_string'))).not_to be_valid
    expect(Video.new(attr_dummy.merge(:link => 'vimeo.com/'))).not_to be_valid
    expect(Video.new(attr_dummy.merge(:link => 'youtube.com/watch?v='))).not_to be_valid
    expect(Video.new(attr_dummy.merge(:link => 'youtube.com/watch?v=qCc_LxpGF_U'))).to be_valid
    expect(Video.new(attr_dummy.merge(:link => 'vimeo.com/30581015'))).to be_valid

    video = Video.create(attr_dummy.merge(:link => 'vimeo.com/'))
    expect(video.errors[:link][0]).to match(/имеет неверное значение/)
  end

  it "should create a valid youtube instance" do
    expect(Video.new(attr_youtube)).to be_valid
  end

  it "should create a valid vimeo instance" do
    expect(Video.new(attr_vimeo)).to be_valid
  end

  it "should not be valid if url video is not match uri string" do
    expect(Video.new(attr_dummy)).not_to be_valid
  end

  it "should get a valid vimeo video info" do
    video = Video.create(attr_vimeo)

    expect(video.title).not_to be_empty
    expect(video.link).to eq attr_vimeo[:link]
  end
end
