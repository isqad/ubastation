# encoding: utf-8
# == Schema Information
#
# Table name: competitions
#
#  id                           :integer          not null, primary key
#  title                        :string(255)      not null
#  preview                      :text             not null
#  slug                         :string(255)
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  thumbnail_image_file_name    :string(255)
#  thumbnail_image_content_type :string(255)
#  thumbnail_image_file_size    :integer
#  thumbnail_image_updated_at   :datetime
#  thumbnail_image_meta         :text
#  thumbnail_video              :text
#  start_at                     :datetime
#  video_link                   :string(255)
#  year                         :integer          not null
#

require 'spec_helper'

include ActionDispatch::TestProcess

describe Competition do

  let(:competition) { build(:competition) }

  subject { competition }

  it { should be_valid }

  it 'should required video or photo as cover' do
    competition.video_link = 'http://www.youtube.com/watch?v=qse_LxpGF_U'
    competition.should_not be_valid
  end

  it 'should not required thumbnail_image' do
    competition.thumbnail_image = nil
    competition.should be_valid
  end
end
