# encoding: utf-8
# == Schema Information
#
# Table name: pages
#
#  id            :integer          not null, primary key
#  title         :string(255)
#  body          :text
#  parent_id     :integer
#  lft           :integer
#  rgt           :integer
#  depth         :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  slug          :string(255)
#  draft         :boolean          default(FALSE), not null
#  link          :string(255)
#  in_menu       :boolean          default(TRUE), not null
#  show_comments :boolean          default(FALSE), not null
#  joined_path   :text
#

require 'spec_helper'

RSpec.describe Page, type: :model do
  let(:page) { create :page, :title => 'about' }
  let(:child_page) { create :page, :title => 'example', :parent => page }

  describe '#nested_path' do
    subject { child_page }

    # its(:nested_path) { should eq '/about/example' }
  end

  describe '#nested_url' do
    subject { child_page }

    # its(:path_nodes) { should eq %w(about example) }
  end

  context 'when create' do
    let(:page) { build :page }

    subject { page }

    it { should be_valid }

    it 'requires title' do
      expect {
        page.title = ''
      }.to change(page, :valid?).to(false)
    end

    it 'does not require body' do
      expect {
        page.body = ''
      }.to_not change(page, :valid?)
    end

    describe '#browser_title' do
      context 'when empty' do
        before do
          page.browser_title = ''
          page.save!
        end

        subject { page.browser_title }

        it { should eq page.title }
      end
    end
  end

  context 'when update' do
    describe '#browser_title' do
      context 'when empty' do
        before do
          page.save!
          page.update_attributes!(:browser_title => nil)
        end

        subject { page.browser_title }

        it { should eq page.title }
      end
    end
  end

  describe '.find_by_path' do
    let!(:child_page) { create :page, :title => 'example', :parent => page }

    it 'returns child about page when looking for /about/example' do
      described_class.find_by_path('about/example').should eq child_page
    end

    it 'returns about page when looking for /about' do
      described_class.find_by_path('about').should eq page
    end
  end
end
