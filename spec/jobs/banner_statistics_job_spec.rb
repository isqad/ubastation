require 'spec_helper'

RSpec.describe BannerStatisticsJob do
  describe '.perform' do
    let(:banner) { create :banner }

    before do
      Redis.current.hset(Banner::CLICKS_KEY, banner.id, 11)
      Redis.current.hset(Banner::VIEWS_KEY, banner.id, 13)

      described_class.perform
    end

    it do
      expect(banner.reload.clicks).to eq 11
      expect(banner.reload.views).to eq 13
    end
  end
end
