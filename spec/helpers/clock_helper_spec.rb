require 'spec_helper'

RSpec.describe ClockHelper, type: :helper do
  it 'returns today' do
    helper.human_time_in_words(Time.now).should eql("#{I18n.t('time.today')} #{I18n.l(Time.now, :format => :short)}")
  end

  it 'returns yesterday' do
    helper.human_time_in_words(1.days.ago).should eql("#{I18n.t('time.yesterday')} #{I18n.l(1.days.ago, :format => :short)}")
  end

  it 'returns normal datetime' do
    helper.human_time_in_words(2.days.ago).should eql(I18n.l(2.days.ago, :format => :medium))
  end
end
