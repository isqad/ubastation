include ActionDispatch::TestProcess

FactoryGirl.define do

  factory :forum_moderator, :class => 'User', :parent => :user do
    tos_agreement true

    after(:build) do |user|
      user.profile = build(:profile, :user => user)
      user.roles << create(:group, :name => 'forum_moderator')
    end
  end

  factory :competition do
    sequence(:title) { |n| "Test competition ##{n}" }
    thumbnail_image { fixture_file_upload 'spec/support/test_photo.jpg', 'image/jpeg' }
    preview 'Test preview competition'
    start_at Time.now
  end

  factory :calendar do
    sequence(:title) { |n| "Test competition ##{n}" }
    start_at { Time.now + 2.days }
    finish_at { Time.now + 4.days }
    place 'Ekaterinburg'
    organizations 'Roga & Copyta'
    disciplines 'Sport'
  end

  factory :news do
    sequence(:title) { |n| "Test news ##{n}" }
    photo { fixture_file_upload 'spec/support/test_photo.jpg', 'image/jpeg' }
    body ("test body"*7)
    anonce "test anonce"
    meta_description "test description"
    external_link ''
  end

  factory :profile do
    bio "I am a cat lover and I love to run"
    birthday 21.years.ago
    gender "Мужской"
    avatar { fixture_file_upload 'spec/support/test_photo.jpg', 'image/jpeg' }
    user
  end

  factory :banner do
    sequence(:name) { |n| "Banner#{n}" }
    link "http://google.com/"
    place "Левый в шапке"
    max_clicks 0
    banner { fixture_file_upload 'spec/support/test_photo.jpg', 'image/jpeg' }
  end

  factory :photo do
    sequence(:description) { |n| "Test photo ##{n}"}
    photo { fixture_file_upload 'spec/support/test_photo.jpg', 'image/jpeg' }
    photoable nil
    in_top false
    top_at_once false
  end

  factory :group do
    name 'forum_moderator'
  end

  factory :forum do
    sequence(:title) { |n| "Test forum#{n}" }
    topics_per_page 10
    posts_per_page 10
  end

  factory :page do
    title "about"
  end

  factory :photo_album do
    sequence(:name) { |n| "photo_album_#{n}" }
    description 'Photo album'
    published true
  end

end
