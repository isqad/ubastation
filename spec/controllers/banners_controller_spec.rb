require 'spec_helper'

RSpec.describe BannersController, type: :controller do
  let!(:banner) { create :banner }

  describe '#show' do
    it do
      get :show, id: banner.id

      is_expected.to redirect_to banner.link
      expect(Redis.current.hget(Banner::CLICKS_KEY, banner.id).to_i).to eq 1
    end
  end
end
