require 'spec_helper'

include Warden::Test::Helpers

RSpec.describe TopicsController, type: :controller do
  let(:author) { create :user }
  let(:user) { create :user }
  let(:forum_moderator) { create :forum_moderator }

  let(:forum) { create :forum }
  let(:topic) { create :topic, title: 'Test title', forum: forum, user: author, state: Topic::STATE_ACCEPTED }

  describe '#create' do
    let(:topic_attrs) { attributes_for :topic }
    before do
      create :admin_user
      sign_in author
      ActionMailer::Base.deliveries.clear
    end

    after { ActionMailer::Base.deliveries.clear }

    it 'creates and notices about topic' do
      post :create, forum_id: forum, topic: topic_attrs

      expect(response).to redirect_to forum_url(forum)
      expect(flash[:notice]).to eq I18n.t('topics.created_and_moderated')
      expect(ActionMailer::Base.deliveries.size).to eq(1)
    end
  end

  describe '#show' do
    context 'when pending' do
      let(:topic) { create :topic, title: 'Test title', forum: forum, user: author }
      before { get :show, forum_id: forum, id: topic }

      it { expect(response.status).to eq 404 }
    end

    context 'when accepted' do
      before { get :show, forum_id: forum, id: topic }

      it { expect(response.status).to eq 200 }
    end

    context 'when rejected' do
      let(:topic) { create :topic, title: 'Test title', forum: forum, user: author, state: Topic::STATE_REJECTED }
      before { get :show, forum_id: forum, id: topic }

      it { expect(response.status).to eq 404 }
    end
  end

  describe '#close' do
    context 'when guest signed in' do
      before { put :close, forum_id: forum, id: topic }

      it { expect(response).to redirect_to new_user_session_url }
    end

    context 'when user is author' do
      before do
        sign_in author
        put :close, forum_id: forum, id: topic
      end

      it { expect(response).to redirect_to forum_topic_url(forum, topic) }
      it { expect(topic.reload).to be_closed }
      it { expect(topic.reload.closer).to eq author }
    end

    context 'when user is not author' do
      before do
        sign_in user
        put :close, forum_id: forum, id: topic.id
      end

      it { expect(response).to have_http_status :forbidden }
    end

    context 'when user is moderator' do
      before do
        sign_in forum_moderator
        put :close, forum_id: forum, id: topic
      end

      it { expect(response).to redirect_to forum_topic_url(forum, topic) }
      it { expect(topic.reload).to be_closed }
      it { expect(topic.reload.closer).to eq forum_moderator }
    end
  end

  describe '#open' do
    shared_examples_for 'open topic' do
      it { expect(response).to redirect_to forum_topic_url(forum, topic) }
      it { expect(topic.reload).not_to be_closed }
      it { expect(topic.reload.closer).to be_nil }
    end

    context 'when guest' do
      before { put :open, forum_id: forum, id: topic }

      it { expect(response).to redirect_to new_user_session_url }
    end

    context 'when user is author' do
      before do
        sign_in author
        put :open, forum_id: forum, id: topic
      end

      it_should_behave_like 'open topic'
    end

    context 'when user is moderator' do
      before do
        sign_in forum_moderator
        put :open, forum_id: forum, id: topic
      end

      it_should_behave_like 'open topic'
    end
  end

  describe '#edit' do
    shared_examples_for 'edit topic page' do
      it { expect(response).to have_http_status :success }
      it { expect(response).to render_template :edit }
      it { expect(assigns(:topic)).to eq topic }
      it { expect(assigns(:forum)).to eq forum }
    end

    context 'when guest signed in' do
      before { get :edit, forum_id: forum, id: topic }

      it { expect(response).to redirect_to new_user_session_url }
    end

    context 'when not author' do
      before do
        sign_in create(:user)
        get :edit, forum_id: forum, id: topic
      end

      it { expect(response).to have_http_status :forbidden }
    end

    context 'when author' do
      before do
        sign_in author
        get :edit, forum_id: forum, id: topic
      end

      it_should_behave_like 'edit topic page'
    end

    context 'when moderator' do
      before do
        sign_in forum_moderator
        get :edit, forum_id: forum, id: topic
      end

      it_should_behave_like 'edit topic page'
    end
  end

  describe '#update' do
    shared_examples_for 'update topic' do
      it { expect(response).to redirect_to forum_topic_url(forum, topic.reload) }
      it { expect(topic.reload.title).to eq 'Cool title!' }
    end

    let(:topic_attrs) { attributes_for :topic, title: 'Cool title!' }

    context 'when guest signed in' do
      before { put :update, forum_id: forum, id: topic, topic: topic_attrs }

      it { expect(response).to redirect_to new_user_session_url }
    end

    context 'when not author' do
      before do
        sign_in create(:user)
        put :update, forum_id: forum, id: topic, topic: topic_attrs
      end

      it { expect(response).to have_http_status :forbidden }
    end

    context 'when author' do
      before do
        sign_in author
        put :update, forum_id: forum, id: topic, topic: topic_attrs
      end

      it_should_behave_like 'update topic'
    end

    context 'when moderator' do
      before do
        sign_in forum_moderator
        put :update, forum_id: forum, id: topic, topic: topic_attrs
      end

      it_should_behave_like 'update topic'
    end

    context 'when invalid attributes' do
      let(:topic_attrs) { attributes_for :topic, title: '' }

      before do
        sign_in forum_moderator
        put :update, forum_id: forum, id: topic, topic: topic_attrs
      end

      it { expect(response).to render_template :edit }
      it { expect(topic.reload.title).not_to eq 'Cool title!' }
    end
  end
end
