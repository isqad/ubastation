require 'spec_helper'

RSpec.describe RegistrationsController, type: :controller do
  describe '#create' do
    let(:user_attributes) do
      {
        user: {
          name: 'Alex',
          email: 'foo@bar.com',
          password: '12345678',
          password_confirmation: '12345678',
          profile_attributes: {
            time_zone: 'Hawaii'
          }
        }
      }
    end

    before do
      @request.env['devise.mapping'] = Devise.mappings[:user]
    end

    it 'registers new user' do
      post :create, user_attributes

      expect(User.count).to eq 1
      expect(User.first.name).to eq user_attributes[:user][:name]
      expect(User.first.profile.time_zone).to eq user_attributes[:user][:profile_attributes][:time_zone]
    end
  end
end
