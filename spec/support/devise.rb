# encoding: utf-8
OmniAuth.config.test_mode = true

OmniAuth.config.mock_auth[:facebook] = {
  :uid => '1234567',
  :provider => 'facebook',
  :credentials => {
    :token => 'token',
    :secret => 'secret'
  },
  :info => {
    :name => 'Porosenok Petr',
    :nickname => 'porosenok_petr',
    :email => 'porosenok@example.com'
  }
}

OmniAuth.config.add_mock(:facebook, OmniAuth.config.mock_auth[:facebook])
