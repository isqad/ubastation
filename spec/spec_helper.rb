require 'rubygems'

require 'simplecov'
SimpleCov.start 'rails'

ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rspec/rails'
require 'factory_girl_rails'
require 'shoulda/matchers'
require 'timecop'
require 'mock_redis'

redis = MockRedis.new
Redis.current = redis
Resque.redis = redis

Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

RSpec.configure do |config|
  config.include Devise::TestHelpers, :type => :controller
  config.extend ControllerMacros, :type => :controller
  config.include FactoryGirl::Syntax::Methods

  config.before(:all) do
    Redis.current.flushdb
    Resque.redis.flushdb
  end

  config.use_transactional_fixtures = true

  config.infer_base_class_for_anonymous_controllers = false
  config.order = 'random'
end
