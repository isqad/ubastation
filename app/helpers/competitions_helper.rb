module CompetitionsHelper

  def competitions_index
    current_year = params[:year].nil? ? Time.current.year : params[:year].to_i

    content_tag :ul do
      ul_body = ''

      Competition.years.each do |y|
        ul_body << content_tag(:li, :class => y == current_year ? 'active' : 'inactive') do
          link_to(y == current_year ? ' - ' : ' + ', '#', class: 'toggle', title: y == current_year ? I18n.t(:collapse) : I18n.t(:expand)) + ' ' +
          link_to(y, competitions_path(y)) +
          draw_competitions(y)
        end
      end

      ul_body.html_safe
    end
  end

  private
  def draw_competitions(year)
    content_tag :ul do
      ul_body = ''

      Competition.by_year(year).each do |c|
        class_active = ((params[:competition_id].nil? && @competition == c) || (params[:competition_id].present? && @competition == c)) ? 'active' : 'inactive'

        ul_body << content_tag(:li, class: class_active) do
         link = ''
         if c.competition_articles.size > 0
          link += link_to(class_active == 'active' ? ' - ' : ' + ', '#', class: 'toggle', title: class_active == 'active' ? I18n.t(:collapse) : I18n.t(:expand)) + ' '
          link += link_to(c.title, competition_path(year, c))
         else
          link += link_to(c.title, '#')
         end

         "#{link} #{draw_articles(year, c)}".html_safe
        end
      end

      ul_body.html_safe
    end
  end

  def draw_articles(year, c)
    if c.competition_articles.size > 0
      content_tag :ul do
        ul_body = ''

        c.competition_articles.each do |a|
          class_active = params[:competition_id].present? && @article == a ? 'active' : 'inactive'

          ul_body << content_tag(:li, class: class_active) do
           link_to(a.title, a.link.to_s == ''.freeze ? competition_article_path(year, c, a) : a.link)
          end
        end

        ul_body.html_safe
      end
    end
  end
end
