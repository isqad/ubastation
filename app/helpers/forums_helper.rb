# encoding: utf-8

module ForumsHelper

  SMILES = [ /\:-?\)/i, # smile
             /\:-?D/, # laugh
             /\;-?\)/i, # joke
             /\:angel\:/i, # angel
             /\:crazy\:/i,
             /\:cry\:/i,
             /\:dance\:/i,
             /\:glasses\:/i,
             /\:hoho\:/i,
             /\:rofl\:/i,
             /\:look\:/i,
             /\:love\:/i,
             /\:music\:/i,
             /\:-?\(/i,
             /\:scratch\:/i,
             /\:-?[o0]/i,
             /\:sick\:/i,
             /\:smoke\:/i,
             /\:stuk\:/i,
             /\:v\:/i,
             /\:zlo\:/i,
             /\:zst\:/i
  ]

  def to_icon(key)
    "<img src=\"/assets/smiles/sm#{SMILES.index(key) + 1}.gif\" alt=\"#{key}\" class=\"sm\" />"
  end
  module_function :to_icon

  # Returns Integer
  def max_latest_posts
    return @max_latest_posts if defined? @max_latest_posts
    count = ActiveadminSettings::Helpers.settings_value('Количество последних постов виджета Форум')
    @max_latest_posts = count.present? && (count = count.to_i) > 0 ? count : 6
  end

  def start_page(per_page=15)
    params[:page].to_i > 0 ? per_page*(params[:page].to_i - 1) : 0
  end

  def posts_paginate(topic, forum)
    posts = topic.posts
    per_page = forum.posts_per_page
    url = forum_topic_path(forum, topic)
    pages = (posts.size.to_f / per_page).ceil

    if pages > 1
      result = ''

      if pages <= 10
        1.upto(pages) do |i|
          result << link_to(i.to_s, "#{url}?page=#{i}")
          result << ', ' if i < pages
        end
      else
        1.upto(5){ |i| result << link_to(i.to_s, "#{url}?page=#{i}") << ', ' }
        result << '... , '
        result << link_to(pages.to_s, "#{url}?page=#{pages}")
      end

      result << " #{I18n.t('topics.pages_short')}"

      result.html_safe
    end
  end
end

class String
  def smileize
    ForumsHelper::SMILES.each do |smile|
      if self =~ smile
        self.gsub!(smile, ForumsHelper.to_icon(smile))
      end
    end
    self
  end
end
