# encoding: utf-8
module ApplicationHelper
  include ActiveSupport::Memoizable

  def presenter(name, action, *args)
    presenter = "#{name.to_s.camelize}Presenter".constantize.new(self)
    yield(presenter) if block_given?
    presenter.send(action, *args)
  end

  def error_messages(resource)
    return "" if resource.errors.empty?

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    sentence = I18n.t('errors.messages.not_saved',
                      :count => resource.errors.count,
                      :resource => resource.class.model_name.human.downcase)

    html = <<-HTML
    <div id="error_explanation">
      <h2>#{sentence}</h2>
      <ul>#{messages}</ul>
    </div>
    HTML

    html.html_safe
  end

  def title
    @site_title ||= @title.present? ? "#{@title} | #{I18n.t(:site_title)}" : I18n.t(:site_title)
  end

  def image_url(source)
    URI.join(root_url, image_path(source))
  end

  def page_link
    @page_link
  end

  def meta_description
    return @meta_description if defined? @meta_description
    return @page_description unless @page_description.to_s == ''.freeze
    @meta_description = settings_value('Seo описание сайта')
  end

  def private_message_link(user)
    return @private_message_link if defined? @private_message_link

    discussion = Discussion.find_between_users(current_user, user)

    @private_message_link = discussion.present? ? discussion_url(discussion) : new_discussion_url(:user_id => user.id)
  end

  def phone_number(number, options = {})
    return unless number.present?

    begin
      Float(number)
    rescue ArgumentError, TypeError
      raise InvalidNumberError, number
    end if options[:raise]

    number       = number.to_s.strip
    options      = options.symbolize_keys
    area_code    = options[:area_code]
    delimiter    = options[:delimiter] || "<span>-</span>"
    extension    = options[:extension]
    country_code = options[:country_code]

    if area_code
      number.gsub!(/(\d{1,3})(\d{3})(\d{4}$)/,"(\\1) \\2#{delimiter}\\3")
    else
      number.gsub!(/(\d{0,3})(\d{3})(\d{4})$/,"\\1#{delimiter}\\2#{delimiter}\\3")
      number.slice!(0, 1) if number.starts_with?(delimiter) && !delimiter.blank?
    end

    str = []
    str << "+#{country_code}#{delimiter}" unless country_code.blank?
    str << "<span>#{number}</span>"
    str << " x #{extension}" unless extension.blank?
    str.join.html_safe
  end
  #memoize :phone_number
end
