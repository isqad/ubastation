module PhotoHelper
  def top_photo_link(photo)
    photoable = photo.photoable

    if photoable.is_a?(PhotoAlbum)
      top_media_album_photo_path(photoable.year, photoable, photo, format: :html)
    elsif photoable.is_a?(News)
      top_news_tiding_photo_path(photoable.year, photoable.month, photoable, photo, format: :html)
    end
  end

  def album_link(photoable)
    if photoable.kind_of?(PhotoAlbum)
      media_album_path(photoable.year, photoable)
    elsif photoable.kind_of?(News)
      news_tiding_path(photoable.year, photoable.month, photoable)
    end
  end
end
