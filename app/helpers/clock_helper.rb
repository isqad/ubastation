module ClockHelper
  def human_time_in_words(time, opts = { :default_format => :medium })
    if time < Time.current.beginning_of_day && time >= 1.days.ago.beginning_of_day
      "#{I18n.t('time.yesterday')} #{I18n.l(time, :format => :short)}"
    elsif time >= Time.current.beginning_of_day && time < (Time.current + 1.days).beginning_of_day
      "#{I18n.t('time.today')} #{I18n.l(time, :format => :short)}"
    else
      I18n.l(time, :format => opts[:default_format])
    end
  end
end
