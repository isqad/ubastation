json.array! videos do |video|
  json.(video, :id, :title, :description, :created_at, :duration, :embed_code)

  json.thumb do
    json.url video.thumbnail.url(:thumb)
    json.width video.thumbnail.width(:thumb)
    json.height video.thumbnail.height(:thumb)
  end

  json.album_name video.videoable.respond_to?(:title) ? video.videoable.title.to_s : video.videoable.name.to_s
end
