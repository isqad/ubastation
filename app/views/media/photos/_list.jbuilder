json.array! photos do |photo|
  json.(photo, :id, :created_at, :position)

  json.description auto_link(photo.description, html: {target: '_blank'}, sanitize: false).html_safe

  json.sizes do
    json.thumb photo.photo.url(:thumb)
    json.show photo.photo.url(:show)
    json.original photo.photo.url(:original)
  end

  json.dimensions do
    json.thumb do
      json.width photo.photo.width(:thumb)
      json.height photo.photo.height(:thumb)
    end
    json.show do
      json.width photo.photo.width(:show)
      json.height photo.photo.height(:show)
    end
    json.original do
      json.width photo.photo.width(:original)
      json.height photo.photo.height(:original)
    end
  end

  json.album_name photo.respond_to?(:photoable_title) ? photo.photoable_title.to_s : photo.photoable_name.to_s

  json.album_link album_link(@photo_album)
end
