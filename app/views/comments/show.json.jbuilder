json.comment do
  json.(@comment, :id, :created_at)
  json.body auto_link(@comment.comment, html: {target: '_blank'}, sanitize: false).html_safe
  json.author do
    json.(@comment.user, :id, :name)
    json.avatar @comment.user.avatar.url(:thumb)
    json.avatar_width @comment.user.avatar.width(:thumb)
    json.avatar_height @comment.user.avatar.height(:thumb)
  end
end
