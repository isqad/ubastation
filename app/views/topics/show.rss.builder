xml.instruct! :xml, :version => "1.0"
xml.rss :version => "2.0" do
  xml.channel do
    xml.title title
    xml.description meta_description
    xml.link forum_topic_url(@forum, @topic)

    for post in @posts
      xml.item do
        xml.title @topic.title
        xml.description post.body.smileize.html_safe
        xml.pubDate post.created_at.to_s(:rfc822)
        xml.link forum_topic_url(@forum, @topic)
        xml.guid forum_topic_url(@forum, @topic)
      end
    end
  end
end