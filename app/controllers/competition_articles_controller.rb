class CompetitionArticlesController < PageableController
  layout 'two'

  def show
    @competition = Competition.find(params[:competition_id])
    @article = @competition.competition_articles.find(params[:id])

    @title = "#{@article.title} | #{I18n.t('activerecord.models.competition.other')}"
  end
end
