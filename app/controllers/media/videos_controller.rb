class Media::VideosController < PageableController

  layout 'two'.freeze

  before_filter :set_conditions

  def index
    @videos = @photo_album.videos

    respond_to do |format|
      format.html { redirect_to media_album_url(params[:year], @photo_album) }
      format.json { render partial: 'media/videos/list', videos: @videos }
    end
  end

  def show
    @photo = @photo_album.photos.find(params[:id])

    set_seo_for @photo_album

    respond_to do |format|
      format.html { render 'media/photo_albums/show' }
    end
  end

  private

  def set_conditions
    @photo_album = PhotoAlbum.find(params[:album_id])

    @conditions = { :select => 'photo_albums.*, COUNT(videos.id) AS photos_count, COUNT(photos.id) AS videos_count',
                    :joins => "LEFT OUTER JOIN videos ON videos.videoable_id = photo_albums.id AND videos.videoable_type = 'PhotoAlbum'
                               LEFT OUTER JOIN photos ON photos.photoable_id = photo_albums.id AND photos.photoable_type = 'PhotoAlbum'",
                    :conditions => ['photo_albums.published = ?', true],
                    :group => 'photo_albums.id',
                    :having => 'COUNT(videos.id) > 0 OR COUNT(photos.id) > 0'
    }
  end
end
