class Media::PhotoAlbumsController < PageableController

  layout 'two'.freeze

  def index
    @photo_albums = ::PhotoAlbumsFinder.new(year: params[:year], current_page: params[:page])
  end

  def show
    @photo_album = PhotoAlbum.find(params[:id])

    set_seo_for @photo_album
  end
end
