class Media::CommentsController < ApplicationController

  layout false

  before_filter :authenticate_user!, except: [ :new, :index ]
  before_filter :find_entity

  respond_to :json, :html

  def index
    @comments = @entity.comments
  end

  def new
    @comment = @entity.comments.build
  end

  def create
    @comment = @entity.comments.build(params[:comment].merge(user: current_user))

    ActiveRecord::Base.transaction do
      @comment.save
      @comment.create_activity key: 'comment.create', owner: current_user
    end

    @comment.share(media_album_url(@album.year, @album))

    respond_to do |format|
      format.json { render :show }
    end
  end

  def destroy
    @comment = @entity.comments.find(params[:id])
    @comment.destroy

    respond_to do |format|
      format.json { render nothing: true }
    end
  end

  private

  def find_entity
    @album = PhotoAlbum.find(params[:album_id])

    if params.has_key?(:photo_id)
      @entity = @album.photos.find(params[:photo_id])
    elsif params.has_key?(:video_id)
      @entity = @album.videos.find(params[:video_id])
    end
  end
end
