class Media::PhotosController < PageableController
  include PhotoHelper

  def index
    @photo_album = PhotoAlbum.find(params[:album_id])

    @photos = @photo_album.photos.order("#{@photo_album.sort} #{@photo_album.order}, id desc")

    respond_to do |format|
      format.html { redirect_to media_album_url(params[:year], @photo_album) }
      format.json { render partial: 'media/photos/list', photos: @photos }
    end
  end

  def show
    @photo_album = PhotoAlbum.find(params[:album_id])
    @photo = @photo_album.photos.find(params[:id])

    respond_to do |format|
      format.html do
        set_seo_for @photo_album

        render 'media/photo_albums/show', layout: 'two'.freeze
      end
    end
  end

  def top
    respond_to do |format|
      format.html do
        find_homepage
        render 'pages/home'
      end
      format.json do
        @photo_album = PhotoAlbum.find(params[:album_id])
        @photo = @photo_album.photos.find(params[:id])
        render :show
      end
    end
  end

  private

  def find_homepage
    @page = Page.where(link: '/'.freeze).first
    @page_title = @title = I18n.t(:home_page)
    @page_link = @page ? @page.nested_path : '/'.freeze
  end
end
