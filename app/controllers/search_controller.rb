class SearchController < PageableController
  layout 'one'

  def show
    @query = params[:query]
    @total = 0
    @results = Array.new

    if @query.present?
      @results = ThinkingSphinx.search @query
      @total = @results.size
    end
  end
end
