class RegistrationsController < Devise::RegistrationsController
  layout 'one'

  def after_update_path_for(resource)
    profile_path
  end

  def create
    if session[:omniauth].nil?
      if !Rails.env.production? || verify_recaptcha
        super
        session[:omniauth] = nil unless @user.new_record? # OmniAuth
      else
        build_resource
        clean_up_passwords(resource)
        flash[:recaptcha_error] = "There was an error with the recaptcha code below. Please re-enter the code."
        # use render :new for 2.x version of devise
        render :new
      end
    else
      super
      session[:omniauth] = nil unless @user.new_record?
    end
  end

  protected

  def after_inactive_sign_up_path_for(resource)
    session[:previous_url] || root_url
  end

  def after_sign_up_path_for(resource)
    session[:previous_url] || root_url
  end
end
