class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :update_user_online_status, :if => proc { user_signed_in? }
  before_filter :set_default_locale
  after_filter :store_location

  rescue_from CanCan::AccessDenied, with: :forbidden
  rescue_from ActiveRecord::RecordNotFound, with: :not_found

  protected

  def update_user_online_status
    current_user.update_online_status
  end

  def set_default_locale
    I18n.locale = 'ru'
  end

  def not_found
    respond_to do |format|
      format.html  { render 'shared/errors/404', status: 404, layout: 'application' }
      format.json { render json: {message: 'not found', status: 404} }
      format.all { render nothing: true, status: 404 }
    end
  end

  def forbidden
    respond_to do |format|
      format.html  { render 'shared/errors/403', status: 403, layout: 'application' }
      format.json { render json: {message: 'forbidden', status: 403} }
      format.all { render nothing: true, status: 403 }
    end
  end

  def store_location
    # store last url as long as it isn't a /users path
    session[:previous_url] = request.fullpath unless request.fullpath =~ %r{/(user)|(admin)}
  end

  def after_sign_in_path_for(resource)
    if resource.is_a?(AdminUser)
      admin_root_path
    else
      session[:previous_url] || root_path
    end
  end

  def after_sign_out_path_for(resource)
    if resource.is_a?(AdminUser)
      admin_root_path
    else
      session[:previous_url] || root_path
    end
  end
end
