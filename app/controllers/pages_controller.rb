class PagesController < PageableController
  before_filter :find_page
  before_filter :not_found, unless: proc { @page.present? }
  before_filter :set_magick_meta

  def show
    render layout: 'two'
  end

  def home
  end

  protected

  def find_page
    @page = action_name == 'home' ? Page.ordered.where(:link => '/').first : Page.find_by_path(params[:path])
  end

  def set_magick_meta
    @page_title = @title = @page.seo_title

    @page_description = @page.meta_description
    @page_link = @page.nested_path
  end
end
