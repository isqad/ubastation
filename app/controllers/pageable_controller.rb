# encoding: utf-8
class PageableController < ApplicationController
  before_filter :set_pageable

  protected

  def set_pageable
    path = request.fullpath.split('/').reject(&:blank?) if request.fullpath != '/'

    @page = Page.find_by_link("/#{path.shift}") if path

    unless @page.nil?
      @page_title = @title = @page.seo_title
      @page_content = @page.body.html_safe
      @page_description = @page.meta_description
      @page_link = @page.nested_path
    end
  end

  def set_seo_for(object)
    if object.respond_to?(:title)
      @title = object.title.to_s
    elsif object.respond_to?(:name)
      @title = object.name.to_s
    end

    @page_description = object.meta_description.to_s if object.respond_to?(:meta_description)
  end
end
