class News::VideosController < PageableController
  layout 'two'

  before_filter :find_news

  def index
    @videos = @news.videos.ordered

    respond_to do |format|
      format.html { redirect_to @news.decorate.url }
      format.json { render partial: 'media/videos/list', photos: @videos }
    end
  end

  def show
    @video = @news.videos.find(params[:id])

    set_seo_for @news

    respond_to do |format|
      format.html { render('news/news/show') }
    end
  end

  private

  def find_news
    @news = News.find(params[:tiding_id])
  end
end
