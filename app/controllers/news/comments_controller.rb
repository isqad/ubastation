class News::CommentsController < ApplicationController
  layout false

  before_filter :authenticate_user!, except: [ :new, :index ]
  before_filter :find_entity

  def index
    @comments = @entity.comments.ordered

    respond_to do |format|
      format.html { redirect_to @news.decorate.url }
      format.json { render 'comments/index' }
    end
  end

  def new
    @comment = @entity.comments.build

    respond_to do |format|
      format.html { request.xhr? ? render(layout: false) : redirect_to(@news.decorate.url) }
    end
  end

  def create
    @comment = @entity.comments.build(params[:comment].merge(user: current_user))

    Comment.transaction do
      status = @comment.save
      # activity
      @comment.create_activity key: 'comment.create', owner: current_user
    end

    respond_to do |format|
      if status
        @comment.share(@news.decorate.url)

        format.html { redirect_to @news.decorate.url }
        format.json { render 'comments/show' }
      else
        format.html { render :new }
        format.json { render nothing: true, status: 422 }
      end
    end

  end

  def destroy
    @comment = @entity.comments.find(params[:id])
    @comment.destroy

    respond_to do |format|
      format.html { redirect_to @news.decorate.url }
      format.json { render nothing: true, status: 204 }
    end
  end

  private
  def find_entity
    @news = News.find(params[:tiding_id])

    if params.has_key?(:photo_id)
      @entity = @news.photos.find(params[:photo_id])
    elsif params.has_key?(:video_id)
      @entity = @news.videos.find(params[:video_id])
    elsif params.has_key?(:tiding_id)
      @entity = @news
    end
  end
end
