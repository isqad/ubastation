class News::NewsController < PageableController

  layout 'two'

  before_filter :authenticate_user!, only: :comment

  def index
    @news = News.live.where(:year => Time.current.year).paginate(:page => params[:page])

    if @news.size.zero?
      redirect_to news_archives_url(Time.current.year - 1)
      return
    end

    respond_to do |format|
      format.html
      format.rss
    end
  end

  def show
    @news = News.find(params[:id])

    set_seo_for @news
  end

  def archive
    @news = News.live.where(:year => params[:year])
    @news = @news.where(:month => params[:month]) if params[:month].present?
    @news = @news.paginate(:page => params[:page])

    render :index
  end

  def comment
    @news = News.find(params[:id])
    @comment = @news.comments.create(params[:comment].merge(user: current_user))

    respond_to do |format|
      format.html { redirect_to :back }
      format.js
    end
  end
end
