class News::PhotosController < PageableController
  include PhotoHelper

  layout 'two'

  before_filter :find_news

  def index
    @photos = @news.photos.ordered

    respond_to do |format|
      format.html { redirect_to @news.decorate.url }
      format.json { render partial: 'media/photos/list', photos: @photos }
    end
  end

  def show
    @photo = @news.photos.find(params[:id])

    set_seo_for @news

    respond_to do |format|
      format.html { render('news/news/show') }
    end
  end

  def top
    respond_to do |format|
      format.html { redirect_to root_url }
      format.json do
        @photo = @news.photos.find(params[:id])
        render 'media/photos/show'
      end
    end
  end

  private

  def find_news
    @news = News.find(params[:tiding_id])
  end
end
