class TopicsController < PageableController
  layout 'one'

  before_filter :authenticate_user!, except: :show

  before_filter :find_forum
  before_filter :find_topic, except: [:new, :create]

  before_filter :set_seo, :register_hit_view, only: :show

  load_and_authorize_resource

  def show
    @posts = @topic.posts.includes(:user).ordered.paginate(page: params[:page], per_page: @forum.posts_per_page)
    @post = @topic.posts.build

    respond_to do |format|
      format.html
      format.rss
    end
  end

  def new
    @topic = @forum.topics.build
    @post = @topic.posts.build
  end

  def edit
    @topic = @forum.topics.find(params[:id])
  end

  def create
    @topic = @forum.topics.build(params[:topic].merge(:user => current_user))

    if @topic.save
      ForumMailer.new_topic_admin_notice(@topic.id).deliver

      flash[:notice] = I18n.t('topics.created_and_moderated')
      redirect_to forum_url(@forum)
    else
      flash[:alert] = I18n.t('topics.not_created')
      render :new
    end
  end

  def update
    @topic = @forum.topics.find(params[:id])

    if @topic.update_attributes(params[:topic])
      redirect_to forum_topic_url(@forum, @topic), notice: I18n.t('topics.updated')
    else
      flash[:alert] = I18n.t('topics.not_updated')
      render :edit
    end
  end

  def destroy
    @topic = @forum.topics.find(params[:id])

    @topic.destroy

    flash[:notice] = I18n.t('topics.deleted')
    redirect_to forum_url(@forum)
  end

  def subscribe
    @topic.add_subscriber(current_user)

    redirect_to forum_topic_url(@forum, @topic)
  end

  def unsubscribe
    @topic.remove_subscriber(current_user)

    redirect_to forum_topic_url(@forum, @topic)
  end

  def close
    @topic.close_by!(current_user)
    redirect_to forum_topic_url(@forum, @topic)
  end

  def open
    @topic.open_by!(current_user)
    redirect_to forum_topic_url(@forum, @topic)
  end

  def pin
    @topic.pin!
    redirect_to forum_topic_url(@forum, @topic)
  end

  def unpin
    @topic.unpin!
    redirect_to forum_topic_url(@forum, @topic)
  end

  protected

  def find_forum
    @forum = Forum.find(params[:forum_id])
  end

  def find_topic
    @topic = @forum.topics.published.find(params[:id])
  end

  def register_hit_view
    @topic.register_view
  end

  def set_seo
    set_seo_for @topic
  end
end
