class ForumsController < PageableController
  layout 'one'

  before_filter :find_forum, :only => [:show, :subscribe, :unsubscribe]

  before_filter :authenticate_user!, :only => [:subscribe, :unsubscribe]

  load_and_authorize_resource

  def index
    @forums = Forum.ordered
  end

  def show
    @topics = @forum.topics.
      published.
      preload(:user, :posts).
      ordered.
      paginate(page: params[:page], per_page: @forum.topics_per_page)

    set_seo_for @forum
  end

  def subscribe
    @forum.feed_users << current_user

    redirect_to forum_url(@forum)
  end

  def unsubscribe
    @forum.subscriptions.where(user_id: current_user.id).delete_all

    redirect_to forum_url(@forum)
  end

  private

  def find_forum
    @forum = Forum.find(params[:id])
  end
end
