class DiscussionsController < Inboxes::DiscussionsController
  layout 'one'

  before_filter :authenticate_user!

  def index
    super
  end

  def show
    super
  end

  def new
    super

    @user = User.find(params[:user_id])
  end

  def destroy
    @discussion.destroy

    flash[:notice] = I18n.t("discussions.removed")
    begin
      redirect_to :back
    rescue ActionController::RedirectBackError
      redirect_to discussions_path
    end
  end

  def create
    @discussion.add_recipient_token current_user.id

    @discussion.messages.each do |m|
      m.discussion = @discussion
      m.user = current_user
    end

    if @discussion.save
      redirect_to discussion_path(@discussion), :notice => I18n.t("discussions.started")
    else
      render 'new'
    end
  end

  private

  def load_and_check_discussion_recipient
    # initializing model for new and create actions
    @discussion = Discussion.new(params[:discussion].presence || {})

    # checking if discussion with this user already exists
    if @discussion.recipient_ids && @discussion.recipient_ids.size == 1
      user = User.find(@discussion.recipient_ids.first)
      discussion = Discussion.find_between_users(current_user, user)
      if discussion
        # it exists, let's add message and redirect current user
        @discussion.messages.each do |message|
          Message.create(:discussion => discussion, :user => current_user, :body => message.body) if message.body
        end
        # redirecting to that existing object
        redirect_to discussion_url(discussion), :notice => t("discussions.already_exists", :user => user.name)
      end
    end
  end
end
