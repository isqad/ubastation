class RoadmapsController < ApplicationController
  def show
    if request.xhr?
      render layout: false
    else
      find_homepage

      render 'pages/home'
    end
  end

  private

  def find_homepage
    @page = Page.ordered.where(link: '/'.freeze).first
    @page_title = @title = I18n.t(:home_page)
    @page_link = @page ? @page.nested_path : '/'.freeze
  end
end
