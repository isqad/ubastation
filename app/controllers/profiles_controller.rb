class ProfilesController < PageableController
  layout 'one'

  before_filter :authenticate_user!

  def show
    @user = current_user

    set_seo_for @user
  end
end
