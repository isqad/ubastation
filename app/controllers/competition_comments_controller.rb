class CompetitionCommentsController < ApplicationController
  layout false

  before_filter :authenticate_user!, except: [ :new, :index ]
  before_filter :find_article

  def index
    @comments = @article.comments

    respond_to do |format|
      format.html { redirect_to competition_article_url(params[:year], @competition, @article) }
      format.json { render 'comments/index' }
    end
  end

  def new
    @comment = @article.comments.build

    respond_to do |format|
      format.html { request.xhr? ? render(layout: false) : redirect_to(competition_article_url(params[:year], @competition, @article)) }
    end
  end

  def create
    @comment = @article.comments.build(params[:comment].merge(user: current_user))

    respond_to do |format|
      if @comment.save
        #current_user.delay.network_publish(@comment.comment, competition_article_url(params[:year], @competition, @article), "#{@article.title} | #{I18n.t('site_title')}")

        format.html { redirect_to competition_article_url(params[:year], @competition, @article) }
        format.json { render 'comments/show' }
      else
        format.html { render :new }
        format.json { render nothing: true, status: 422 }
      end
    end

  end

  def destroy
    @comment = @article.comments.find(params[:id])
    @comment.destroy

    respond_to do |format|
      format.html { redirect_to competition_article_url(params[:year], @competition, @article) }
      format.json { render nothing: true, status: 204 }
    end
  end

  private
  def find_article
    @competition = Competition.find(params[:competition_id])
    @article = @competition.competition_articles.find(params[:article_id])
  end
end
