class PostsController < PageableController
  layout 'one'

  before_filter :authenticate_user!, :find_forum
  before_filter :find_post, except: [:create, :new]

  load_and_authorize_resource
  skip_authorize_resource only: [:create, :new]

  def new
    redirect_to forum_topic_url(@forum, @topic)
  end

  def create
    @post = @topic.posts.build(params[:post].merge(user: current_user))

    authorize! :create, @post

    if @post.save
      redirect_to @post.url_path
    else
      flash[:alert] = I18n.t('posts.not_created')
      redirect_to forum_topic_url(@forum, @topic)
    end
  end

  def update
    if @post.update_attributes(params[:post])
      redirect_to forum_topic_url(@forum, @topic, page: @post.page_for_pagination(@forum.posts_per_page)) + "#post-#{@post.id}"
    else
      render :edit
    end
  end

  def destroy
    @post.destroy

    flash[:notice] = t('posts.deleted')
    redirect_to forum_topic_url(@forum, @topic)
  end

  def cite
    respond_to do |format|
      format.js
    end
  end

  def answer
    respond_to do |format|
      format.js
    end
  end

  def complaint
    authorize! :complaint, @post

    Post.record_timestamps = false
    @post.review!
    Post.record_timestamps = true

    compliant_to @post

    respond_to do |format|
      format.js
    end
  end

  def spam
    Post.record_timestamps = false
    @post.spam!
    Post.record_timestamps = true

    respond_to do |format|
      format.js
    end
  end

  def approve
    Post.record_timestamps = false
    @post.approve!
    Post.record_timestamps = true

    respond_to do |format|
      format.js
    end
  end

  private

  def find_forum
    @forum = Forum.find(params[:forum_id])
    @topic = @forum.topics.find(params[:topic_id])
  end

  def find_post
    @post = @topic.posts.find(params[:id])
  end

  def compliant_to(post)
    User.forum_moderators.each do |user|
      ForumMailer.complaint(current_user.id, post.id, user.id).deliver
    end
  end
end
