class CompetitionsController < PageableController
  layout 'two'

  def index
    @competitions = Competition.ordered.by_year(params[:year])
  end

  def show
    @competition = Competition.find(params[:id])
    article = @competition.competition_articles.first
    if article
      redirect_to article.link.to_s == ''.freeze ? competition_article_url(@competition.year, @competition, article) : article.link
    else
      redirect_to competitions_url
    end
  end
end
