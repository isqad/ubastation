class CommentsController < ApplicationController
  layout false

  before_filter :authenticate_user!, except: [:new, :index]
  before_filter :find_page

  respond_to :json, :html

  def index
    @comments = @page.comments.with_users.ordered
  end

  def new
    @comment = @page.comments.build
  end

  def create
    @comment = @page.comments.build(params[:comment].merge(user: current_user))

    ActiveRecord::Base.transaction do
      @comment.save
      # activity
      @comment.create_activity key: 'comment.create', owner: current_user
    end

    respond_to do |format|
      format.json { render :show }
    end
  end

  def destroy
    @comment = @page.comments.find(params[:id])
    @comment.destroy

    respond_to do |format|
      format.json { render nothing: true }
    end
  end

  private

  def find_page
    @page = Page.find(params[:page_id])
  end
end
