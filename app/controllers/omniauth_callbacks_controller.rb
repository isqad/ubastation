class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  include Devise::Controllers::Rememberable

  def all
    auth = request.env['omniauth.auth']

    user = User.find_for_oauth(auth)

    if user.persisted?

      user.update_omniauth(auth.credentials.token, auth.credentials.secret)

      remember_me user

      sign_in_and_redirect user, :event => :authentication #this will throw if @user is not activated
    else
      session["devise.user_attributes"] = user.attributes

      redirect_to new_user_registration_url, :notice => I18n.t('users.required_additional_info')
    end
  end

  alias_method :twitter, :all
  alias_method :facebook, :all
  alias_method :vkontakte, :all

end
