class MessagesController < Inboxes::MessagesController
  layout "one"

  before_filter :authenticate_user!

  def index
    super
  end

  def create
    super
  end

end