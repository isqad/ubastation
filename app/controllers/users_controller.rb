class UsersController < PageableController
  layout 'one'

  def show
    @user = User.from_param(params[:id])

    set_seo_for @user

    redirect_to profile_url if user_signed_in? && @user == current_user
  end
end
