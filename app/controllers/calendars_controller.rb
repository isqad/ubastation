class CalendarsController < PageableController

  before_filter :find_homepage

  def index
    @lives = Calendar.live(params[:year])
    @archives = Calendar.archive(params[:year])
    @archive_years = Calendar.years

    respond_to do |format|
      format.html do
        if request.xhr?
          render layout: false
        else
          find_homepage

          render 'pages/home'
        end
      end
      format.json { render json: {archives: @archives, lives: @lives} }
    end
  end

  private

  def find_homepage
    @page = Page.ordered.where(link: '/'.freeze).first
    @page_title = @title = I18n.t(:home_page)
    @page_link = @page ? @page.nested_path : '/'.freeze
  end
end
