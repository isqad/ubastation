class BannersController < ApplicationController

  # GET /banners/:id
  def show
    banner = Banner.find(params[:id])
    banner.inc_click
    redirect_to banner.link
  end
end
