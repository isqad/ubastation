class PhotoAlbumsFinder
  include Findit::Collections
  include Findit::WillPaginate

  cache_key do
    ["photo_albums", @options]
  end

  # Public: constructor
  #
  # options - Hash
  #   :year - Integer
  #   :current_page - Integer
  def initialize(options)
    @options = options
    @year = options.fetch(:year)
    @current_page = options.fetch(:current_page)
  end

  private

  def find
    if latest_exist_year
      ::PhotoAlbum.by_year(latest_exist_year).paginate(page: @current_page)
    else
      []
    end
  end

  def exists?
    ::PhotoAlbum.by_year(@year).exists?
  end

  def latest_exist_year
    @latest_exist_year ||= exists? ? @year : ::PhotoAlbum.maximum(:year)
  end
end
