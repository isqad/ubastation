class ForumMailer < ActionMailer::Base
  include Resque::Mailer

  default :from => Settings.uba.email_from

  def new_topic(user_id, topic_id)
    I18n.locale = 'ru'
    @user = User.find(user_id)
    @topic = Topic.find(topic_id)
    mail(to: @user.email, subject: "#{I18n.t('forums.new_topic_notification')} | #{I18n.t('site_title')}")
  end

  def complaint(user_id, post_id, moderator_id)
    I18n.locale = 'ru'
    @user = User.find(user_id)
    @post = Post.find(post_id)
    moderator = User.find(moderator_id)
    mail(to: moderator.email, subject: "#{I18n.t('posts.complaint_notification')} | #{I18n.t('site_title')}")
  end

  def new_post(user_id, post_id)
    I18n.locale = 'ru'
    @user = User.find(user_id)
    @post = Post.find(post_id)
    mail(to: @user.email, subject: "#{I18n.t('posts.new_post_notification')} | #{I18n.t('site_title')}")
  end

  def new_topic_admin_notice(topic_id)
    @topic = Topic.find(topic_id)
    admin_emails = AdminUser.select('email').all.map!(&:email)
    mail(to: admin_emails, subject: "#{I18n.t('site_title')} | #{I18n.t('topic.created')}")
  end
end
