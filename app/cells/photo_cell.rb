# coding: utf-8
class PhotoCell < Cell::Rails
  helper PhotoHelper

  def show
    @photo = Photo.today_photo
    render
  end
end
