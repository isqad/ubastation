class BannerCell < Cell::Rails
  # TODO: исправить
  #cache :show, :expires_in => 45.minutes

  def show(args)
    place = args[:place]
    @banner = Banner.random_by(place)
    @banner.inc_view unless @banner.nil?
    render
  end

end
