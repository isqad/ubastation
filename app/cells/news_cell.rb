# encoding: utf-8
class NewsCell < Cell::Rails
  include ActiveadminSettings::Helpers

  helper_method :archive_years, :archive_months_by

  # Public: Дерево архива новостей по годам и месяцам
  def archive_tree(current_year = nil, current_month = nil)
    @current_year, @current_month = current_year.to_i, current_month.to_i
    render
  end

  private

  def archive_years
    @archive_years ||= News.published.select('COUNT(*) AS count_by_year, year').group('year').order('year DESC').map do |news|
      {:year => news.year, :count => news.count_by_year}
    end
  end

  def archive_months_by(year)
    News.published.where(:year => year).select('COUNT(*) AS count_by_month, month').group('month').order('month ASC').map do |news|
      {:month => news.month, :count => news.count_by_month}
    end
  end
end
