class CalendarCell < Cell::Rails

  def show
    @competitions = Calendar.latest
    render
  end

end
