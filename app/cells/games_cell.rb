class GamesCell < Cell::Rails

  def show
    @games = Competition.latest
    render
  end

end
