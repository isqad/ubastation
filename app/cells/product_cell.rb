class ProductCell < Cell::Rails

  cache :show, :expires_in => 1.years

  def show
    render
  end

end
