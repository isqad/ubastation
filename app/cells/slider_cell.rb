# coding: utf-8
class SliderCell < Cell::Rails

  def show
    @slides = Slider.ordered

    render
  end

end
