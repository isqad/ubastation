# == Schema Information
#
# Table name: ckeditor_assets
#
#  id                :integer          not null, primary key
#  data_file_name    :string(255)      not null
#  data_content_type :string(255)
#  data_file_size    :integer
#  assetable_id      :integer
#  assetable_type    :string(30)
#  type              :string(30)
#  width             :integer
#  height            :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Ckeditor::Picture < Ckeditor::Asset
  has_attached_file :data,
                    :url  => "/ckeditor_assets/pictures/:id/:style_:basename.:extension",
                    :path => ":rails_root/public/ckeditor_assets/pictures/:id/:style_:basename.:extension",
	                  :styles => {
                      :original => '1024x768>',
                      :content => '712>',
                      :thumb => '118x100#'
                    },
                    :convert_options => {
                      :original => '-strip -interlace Plane -quality 85',
                      :content => "-strip -interlace Plane -quality 85",
                      :thumb => "-strip -quality 85"
                    }

  validates_attachment_size :data, less_than: 4.megabytes
  validates_attachment_content_type :data, content_type: /\Aimage\/(pjpeg|jpeg|png|gif)\Z/
  validates_attachment_file_name :data, matches: [/png\Z/i, /p?jpe?g\Z/i, /gif\Z/i]

  def url_content
    url(:content)
  end
end
