# == Schema Information
#
# Table name: profiles
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  avatar_file_name    :string(255)
#  avatar_content_type :string(255)
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#  birthday            :date
#  bio                 :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  show_email          :boolean          default(FALSE), not null
#  gender              :string(255)
#  forum_sign          :string(255)
#  show_online         :boolean          default(TRUE), not null
#  avatar_meta         :text
#  publish_comments    :boolean          default(FALSE), not null
#

class Profile < ActiveRecord::Base
  attr_accessible :avatar, :birthday, :bio, :show_email, :show_online, :forum_sign, :gender, :publish_comments, :time_zone

  belongs_to :user

  has_attached_file :avatar,
                    :default_url => "/assets/:class/:style/missing.jpg",
                    :styles => {
                      :original => ['1024x768>', :jpg],
                      :medium => ["180x", :jpg],
                      :thumb => ["x100", :jpg],
                      :small => ['50x50', :jpg]
                    },
                    :convert_options => {
                      :original => '-strip -interlace Plane -quality 85',
                      :thumb => "-strip -quality 85 -gravity center -crop 133x100+0+0 +repage",
                      :medium => "-strip -quality 85 -gravity north -crop 180x240+0+0 +repage",
                      :small => "-strip -quality 85 -gravity north -crop 50x50+0+0 +repage"
                    },
                    :url  => "/system/:class/:id/:style/:hash.:extension",
                    :path => ":rails_root/public/system/:class/:id/:style/:hash.:extension",
                    :hash_secret => "uba_photos"

  validates :forum_sign, :length => { :maximum => 120 }

  validates_attachment_size :avatar, less_than: 4.megabytes
  validates_attachment_content_type :avatar, content_type: /\Aimage\/(pjpeg|jpeg|png|gif)\Z/
  validates_attachment_file_name :avatar, matches: [/png\Z/i, /p?jpe?g\Z/i, /gif\Z/i]
end
