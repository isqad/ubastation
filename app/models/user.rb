class User < ActiveRecord::Base
  ONLINE_THRESHOLD = 15.minutes

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :confirmable, :lockable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :last_request_at,
                  :provider, :uid, :profile_attributes, :name, :confirmed_at, :role_ids,
                  :omniauth_token, :omniauth_secret, :tos_agreement

  has_one :profile, :dependent => :destroy
  accepts_nested_attributes_for :profile

  has_many :posts, :dependent => :destroy
  has_many :comments, :dependent => :destroy
  has_many :topic, :dependent => :destroy

  has_many :memberships, foreign_key: 'member_id', dependent: :delete_all
  has_many :roles, through: :memberships, class_name: 'Group', source: :group

  has_many :subscriptions, dependent: :destroy
  has_many :feed_topics, through: :subscriptions, class_name: 'Topic', source: :topic

  has_many :subscription_forums, dependent: :destroy
  has_many :feed_forums, through: :subscription_forums, class_name: 'Forum', source: :forum

  validates :tos_agreement, acceptance: true, allow_nil: false, on: :create, unless: -> { Rails.env.test? }
  validates :profile, presence: true
  validates :name, presence: true, length: {minimum: 3, maximum: 30}

  delegate :avatar, :birthday, :bio, :show_email?, :show_online?, :forum_sign, :gender, :publish_comments?, :to => :profile

  # online users
  scope :online, -> { where('last_request_at > ?', ONLINE_THRESHOLD.ago) }
  # forum moderators
  scope :forum_moderators, -> { joins(:roles).merge(Group.where(name: 'forum_moderator')) }

  before_validation ->{ self.tos_agreement = '1' }, if: ->{ uid.present? }, on: :create

  has_inboxes

  class << self
    # Find user for oauth
    def find_for_oauth(auth)

      auth.uid = auth.uid.to_s

      where(auth.slice(:provider, :uid)).first_or_create do |user|
        user.provider = auth.provider
        user.uid = auth.uid
        user.email = auth.info.email
        user.name = auth.info.nickname
        user.omniauth_token = auth.credentials.token
        user.omniauth_secret = auth.credentials.secret
      end
    end

    def from_param(param)
      where(id: param.gsub(%r{^id-}, ''.freeze).to_i).first!
    end

    # copy session data before user sign up
    def new_with_session(params, session)
      return super unless session["devise.user_attributes"].present?

      new(session["devise.user_attributes"], :without_protection => true) do |user|
        user.attributes = params
        user.valid?
      end
    end
  end

  def network_publish(text, link, description)
    return unless publish_comments? && omniauth_token.present?

    facebook.feed!(message: text, link: link, description: description) if provider == 'facebook'
    twitter.update("#{text.truncate(separator: ' ')} #{link}") if provider == 'twitter'
  end

  def role?(role)
    !!roles.find_by_name(role.to_s.underscore)
  end

  # user is online?
  def online?
    return false unless cached_last_request_at.present?
    cached_last_request_at >= ONLINE_THRESHOLD.ago.in_time_zone
  end

  def cached_last_request_at
    return @cached_last_request_at if defined? @cached_last_request_at
    last_request_in_seconds = Redis.current.hget('users_online', id)
    @cached_last_request_at = last_request_in_seconds.present? ? Time.at(last_request_in_seconds.to_i).utc.in_time_zone : last_request_at
  end

  def password_required?
    super && provider.blank?
  end

  def confirmation_required?
    super && provider.blank?
  end

  def update_with_password(params, *options)
    return super unless encrypted_password.blank?
    update_attributes!(params, *options)
  end

  def to_param
    "id-#{id}"
  end

  def update_omniauth(token, secret)
    if omniauth_token.blank? || (provider == 'twitter' && omniauth_secret.blank?)
      update_attributes!(omniauth_token: token, omniauth_secret: secret)
    end
  end

  def profile
    super || build_profile
  end

  def update_online_status
    Redis.current.hset('users_online', id, Time.now.utc.to_i)
  end

  protected

  def facebook
    @fb_user ||= FbGraph::User.me(omniauth_token)
  end

  def twitter
    @tw_user ||= Twitter::Client.new(
      :consumer_key => Settings.uba.omniauth.twitter.app_id,
      :consumer_secret => Settings.uba.omniauth.twitter.app_secret,
      :oauth_token => omniauth_token,
      :oauth_token_secret => omniauth_secret
    )
  end
end
