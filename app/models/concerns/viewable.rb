# encoding: utf-8

module Concerns
  module Viewable
    extend ActiveSupport::Concern

    included do
      has_many :views, :dependent => :destroy, :as => :viewable
    end

    # Public: Учет просмотра пользователем
    #
    # user - User instance
    #
    # Returns nothing
    def register_view_by(user)
      return unless user.present?

      view = views.find_or_create_by_user_id(user.id)

      View.update_all('count = count + 1', :id => view.id)
    end

    # Public: Количество просмотров без учета просмотров самого автора топика
    #
    # Returns Integer
    def count_views
      views.where('user_id <> ?', user.id).count
    end
  end
end
