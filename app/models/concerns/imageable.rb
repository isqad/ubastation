# encoding: utf-8
module Concerns
  module Imageable
    extend ActiveSupport::Concern

    # Public: Максимальный размер загружаемых фотографий
    MAX_IMAGE_SIZE = 4.megabytes
    # Public: Допустимые форматы изображений
    ALLOWED_IMAGE_EXTENSIONS = %w(image/jpeg image/pjpeg image/png image/gif).freeze
    # Public: Путь к картинке
    DEFAULT_IMAGE_URL = '/system/images/:class/:subject_type/:id_:style.:extension'
  end
end
