# encoding: utf-8

module Concerns
  module Videoable
    extend ActiveSupport::Concern

    included do
      has_many :videos, :dependent => :destroy, :as => :videoable
    end

    # Public: Количество видеороликов
    #
    # Returns Integer
    def count_videos
      videos.count
    end
  end
end
