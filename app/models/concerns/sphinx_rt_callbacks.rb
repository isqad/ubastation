# encoding: utf-8
module Concerns
  module SphinxRtCallbacks
    extend ActiveSupport::Concern

    included do
      after_save :update_sphinx_rt_index
    end

    private

    def update_sphinx_rt_index
      Resque.enqueue SphinxRtCallbackJob, self.class.to_s.underscore.to_sym, id
    end

    def sphinx_rt_callback(domain)
      klass = domain.class.to_s.underscore.to_sym
      ThinkingSphinx::RealTime::Callbacks::RealTimeCallbacks.new(klass).after_save(domain)
    end
  end
end
