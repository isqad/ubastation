# encoding: utf-8

module Concerns
  module PgExtension
    extend ActiveSupport::Concern

    module ClassMethods
      private

      def at_time_zone
        return @at_time_zone if defined?(@at_time_zone)

        # check for Postgresql
        if ActiveRecord::Base.configurations[Rails.env]['adapter'] == 'postgresql'
          @at_time_zone = " AT TIME ZONE '#{Time.zone.tzinfo.identifier}'"
        end
      end
    end
  end
end
