# encoding: utf-8

module Concerns
  module Photoable
    extend ActiveSupport::Concern

    included do
      has_many :photos, :dependent => :destroy, :as => :photoable
    end

    # Public: Количество фотографий
    #
    # Returns Integer
    def count_photos
      photos.count
    end
  end
end
