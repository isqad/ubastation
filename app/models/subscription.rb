# == Schema Information
#
# Table name: subscriptions
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  topic_id   :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Subscription < ActiveRecord::Base
  attr_accessible :user, :topic

  belongs_to :user
  belongs_to :topic

  validates :user, :presence => true
  validates :topic, :presence => true
end
