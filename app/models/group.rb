# encoding: utf-8
# == Schema Information
#
# Table name: groups
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#

class Group < ActiveRecord::Base

  attr_accessible :name, :description

  scope :forum_moderators, ->{ where(:name => 'forum_moderator') }

  has_many :memberships
  has_many :members, :through => :memberships, :class_name => 'User'

  validates :name, :presence => true, :uniqueness => true

  def to_s
    name
  end
end
