# == Schema Information
#
# Table name: views
#
#  id            :integer          not null, primary key
#  user_id       :integer
#  viewable_id   :integer
#  viewable_type :string(255)
#  count         :integer          default(0)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class View < ActiveRecord::Base
  belongs_to :viewable, :polymorphic => true
  belongs_to :user

  validates :viewable_type, :viewable_id, :presence => true

  attr_accessible :user, :count
end
