# encoding: utf-8
# == Schema Information
#
# Table name: pages
#
#  id            :integer          not null, primary key
#  title         :string(255)
#  body          :text
#  parent_id     :integer
#  lft           :integer
#  rgt           :integer
#  depth         :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  slug          :string(255)
#  draft         :boolean          default(FALSE), not null
#  link          :string(255)
#  in_menu       :boolean          default(TRUE), not null
#  show_comments :boolean          default(FALSE), not null
#  joined_path   :text
#
class Page < ActiveRecord::Base
  extend FriendlyId
  include TheSortableTree::Scopes
  include Concerns::SphinxRtCallbacks

  scope :without_main, -> { where('link <> ?', '/'.freeze) }
  scope :ordered, ->{ order(arel_table[:lft].asc) }
  scope :live, ->{ where(:draft => false) }
  scope :in_menu, ->{ where(:in_menu => true) }
  scope :fast_menu, ->{ live.in_menu.includes(:parent).ordered }

  attr_accessible :body, :title, :draft, :parent_id, :link,
                  :in_menu, :path, :meta_description, :browser_title,
                  :show_comments

  validates :title, :presence => true

  after_validation :move_friendly_id_error_to_name
  before_save :generate_joined_path
  before_save :clean_title_and_body

  friendly_id :title, :use => [:slugged, :reserved],
              :reserved_words => %w(index new session login logout users admin media news register)

  is_seo_meta

  acts_as_nested_set :dependent => :destroy

  acts_as_commentable

  def self.find_by_path(path)
    find_by_joined_path("/#{path}")
  end

  # Public: Список нод в пути страницы
  #
  # Returns Array
  def path_nodes
    self_and_ancestors.map(&:slug)
  end

  # Public: Вложенный путь до страницы
  #
  # Returns String
  def nested_path
    return link if link.present?
    joined_path
  end

  def path_without_slash
    nested_path.gsub(%r{\A/}, ''.freeze)
  end

  # Public: Имеет ли указанный путь
  #
  # path - String
  #
  # Returns Boolean
  def path_eql?(path)
    return false unless path.is_a? String
    nested_path == path
  end

  def seo_title
    browser_title.blank? ? title : browser_title
  end

  def url_path
    return @url_path if defined? @url_path
    return link if link.present?
    @url_path = Rails.application.routes.url_helpers.page_url(joined_path)
  end

  private

  # Internal: очищаем от лишнего
  #
  # Returns nothing
  def clean_title_and_body
    self.browser_title = title if browser_title.blank?

    body.gsub!(%r{\A<p>\n*\s*&nbsp;\n*\s*</p>}, ''.freeze) unless body.blank?
  end

  def move_friendly_id_error_to_name
    errors.add :title, *errors.delete(:friendly_id) if errors[:friendly_id].present?
  end

  # Internal: генерация пути
  #
  # Returns nothing
  def generate_joined_path
    self.joined_path = if slug.present?
                         prefix = parent.present? ? parent.joined_path.to_s : ''.freeze
                         "#{prefix}/#{[prefix, slug].map { |s| s.to_s.gsub(/^#{prefix}/, ''.freeze).split('/'.freeze) }.flatten.delete_if { |n| n.blank? }.join('/'.freeze)}"
                       else
                         nil
                       end
  end
end
