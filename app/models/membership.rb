# == Schema Information
#
# Table name: memberships
#
#  id        :integer          not null, primary key
#  group_id  :integer
#  member_id :integer
#

class Membership < ActiveRecord::Base

  belongs_to :group
  belongs_to :member, :class_name => 'User'

  attr_accessible :member_id, :group_id
end
