# encoding: utf-8
# == Schema Information
#
# Table name: photos
#
#  id                 :integer          not null, primary key
#  photo_file_name    :string(255)
#  photo_content_type :string(255)
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#  description        :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  photoable_id       :integer
#  photoable_type     :string(255)
#  photo_meta         :text
#  position           :integer          default(1), not null
#  in_top             :boolean          default(FALSE), not null
#  top_at_once        :boolean          default(FALSE), not null
#  showed_at          :datetime
#  expired_at         :datetime
#

class Photo < ActiveRecord::Base
  attr_accessible :photo, :description, :in_top, :top_at_once, :top, :photoable, :position

  attr_accessor :album_link

  belongs_to :photoable, :polymorphic => true

  scope :ordered, ->{ order(arel_table[:position].asc, arel_table[:created_at].desc) }
  scope :show_at_once, ->{ where(:top_at_once => true) }

  after_create :touch_album

  delegate :name, :title, :photos, to: :photoable, prefix: true

  has_attached_file :photo,
                    default_url: '/assets/:class/:style/missing.jpg',
                    styles: {
                      original: ['1024x768>', :jpg],
                      thumb: ['x120', :jpg],
                      big: ['712x', :jpg],
                      show: ['800x600', :jpg],
                      top: ['180x', :jpg]
                    },
                    convert_options: {
                      original: '-strip -interlace Plane -quality 85',
                      thumb: '-strip -quality 85 -gravity center -crop 162x120+0+0 +repage',
                      show: '-strip -interlace Plane -quality 85',
                      big: '-strip -interlace Plane -quality 85 -gravity north -crop 712x534+0+0 +repage',
                      top: '-strip -quality 85 -gravity north -crop 180x120+0+0 +repage'
                    },
                    url: "/system/:class/:id/:style/:hash.:extension",
                    path: ":rails_root/public/system/:class/:id/:style/:hash.:extension",
                    hash_secret: "uba_photos"

  validates_attachment_size :photo, less_than: 4.megabytes
  validates_attachment_content_type :photo, content_type: /\Aimage\/(pjpeg|jpeg|png|gif)\Z/
  validates_attachment_file_name :photo, matches: [/png\Z/i, /p?jpe?g\Z/i, /gif\Z/i]

  acts_as_commentable(order: 'comments.created_at DESC')

  before_save :set_expired_top_onced, :if => proc { top_at_once_changed? }

  after_create do |p|
    p.photoable.update_attributes!(:coverable => p) if p.photoable.is_a?(PhotoAlbum) && p.photoable.no_cover?
  end

  class << self
    def rebuild(photoable)
      return unless photoable.respond_to?(:photos)

      photoable.photos.each_with_index do |photo, key|
        Photo.update_all({position: key + 1}, {id: photo.id})
      end
    end

    # Public: Фото дня
    #
    #
    #
    # Returns Photo instance
    def today_photo
      reset_top_photos

      # 1. Ищем фото с top_at_once
      photo_at_once = where(top_at_once: true).first

      if photo_at_once
        where(id: photo_at_once.id).update_all(showed_at: Time.current)
        return photo_at_once.reload
      end

      # 2. Берем из архива
      photos = where(in_top: true).order('showed_at ASC NULLS FIRST, position ASC, id ASC').to_a

      photo_of_day = photos.find { |photo| photo.expired_at.present? }

      return photo_of_day if photo_of_day.present?

      photo_of_day = photos.first

      return unless photo_of_day.present?

      where(id: photo_of_day.id).
        update_all(showed_at: Time.current, expired_at: top_photo_expiration)

      photo_of_day.reload
    end

    # Public: Срок действия фотографии в топе
    #
    # Returns Time
    def top_photo_expiration
      Time.current.hour >= 18 ? Time.current.end_of_day + 1.day + 4.hours : Time.current.end_of_day + 4.hours
    end

    private

    # Internal: Сбрасываем top_at_once
    #
    # Returns noting
    def reset_top_photos
      where(arel_table[:expired_at].not_eq(nil)).
        where(arel_table[:expired_at].lt(Time.current)).
        update_all(top_at_once: false, expired_at: nil)
    end
  end

  def cover?
    photoable.coverable.is_a?(Photo) && photoable.coverable.id == id
  end

  def new_in_top?
    (in_top? || top_at_once?) && top.nil?
  end

  private

  def touch_album
    return unless photoable.is_a?(PhotoAlbum)

    PhotoAlbum.where(id: photoable_id).update_all(created_at: Time.current, year: Time.current.year)
  end

  def set_expired_top_onced
    self.expired_at = top_at_once? ? self.class.top_photo_expiration : nil
    self.showed_at = nil unless top_at_once?
  end
end
