# encoding: utf-8
# == Schema Information
#
# Table name: videos
#
#  id                     :integer          not null, primary key
#  link                   :string(255)
#  video_id               :string(255)
#  title                  :string(255)
#  provider               :string(255)
#  description            :text
#  width                  :integer
#  height                 :integer
#  embed_code             :text
#  embed_url              :string(255)
#  videoable_id           :integer
#  videoable_type         :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  thumbnail_file_name    :string(255)
#  thumbnail_content_type :string(255)
#  thumbnail_file_size    :integer
#  thumbnail_updated_at   :datetime
#  thumbnail_meta         :text
#  published              :boolean          default(FALSE), not null
#  duration               :time
#

class Video < ActiveRecord::Base
  belongs_to :videoable, :polymorphic => true

  attr_accessible :link, :description, :title, :provider,
                  :duration, :video_id, :width, :height,
                  :embed_code, :embed_url, :thumbnail, :published,
                  :duration_time

  validates :link, :presence => true,
                   :format => {:with => /\A(https?:\/\/)?(www\.)?((vimeo\.com\/\d+)|(youtube\.com\/watch\?v=[-a-zA-Z0-9.%_]+)|(youtu\.be\/[-a-zA-Z0-9.%_]+))\z/i}

  has_attached_file :thumbnail,
                    :default_url => "/assets/:class/:style/missing.jpg",
                    :styles => {
                      :original => ['1024x768>', :jpg],
                      :thumb => ["640x", :jpg]
                    },
                    :convert_options => {
                      :original => '-strip -interlace Plane -quality 85',
                      :thumb => "-strip -interlace Plane -quality 85 -gravity north -crop 640x530+0+0 +repage"
                    },
                    :url  => "/system/:class/:id/:style/:hash.:extension",
                    :path => ":rails_root/public/system/:class/:id/:style/:hash.:extension",
                    :hash_secret => "uba_videos"

  do_not_validate_attachment_file_type :thumbnail

  acts_as_commentable

  before_save :parse_video_info, :if => proc { link_changed? }

  def cover?
    videoable.coverable.is_a?(Video) && videoable.coverable.id == id
  end

  private

  def parse_video_info
    movie = VideoInfo.new(link)

    self.duration = Time.at(movie.duration).utc.strftime('%H:%M:%S')
    self.provider = movie.provider
    self.title = movie.title
    self.description = movie.description
    self.width = movie.width
    self.height = movie.height
    self.embed_code = movie.embed_code
    self.embed_url = movie.embed_url
    self.videoable = videoable

    begin
      self.thumbnail = URI.parse(movie.thumbnail_large)
    rescue
      errors.add(:link, :invalid_thumbnail_url, link: link)
      false
    end
  end
end
