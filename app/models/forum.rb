# encoding: utf-8
# == Schema Information
#
# Table name: forums
#
#  id              :integer          not null, primary key
#  title           :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  slug            :string(255)
#  topics_count    :integer          default(0)
#  topics_per_page :integer
#  posts_per_page  :integer
#

class Forum < ActiveRecord::Base
  extend FriendlyId

  TOPICS_COUNT_FOR_HOME_PAGE = 10

  attr_accessible :title, :topics_per_page, :posts_per_page

  scope :ordered, ->{ order('created_at ASC') }

  has_many :topics, :dependent => :destroy

  has_many :subscription_forums, :dependent => :destroy
  has_many :feed_users, :through => :subscription_forums, :class_name => 'User', :source => :user, :before_add => :validate_include

  validates :title, uniqueness: true, :presence => true, :length => { minimum: 3, maximum: 120 }
  validates :topics_per_page, :posts_per_page, :presence => true, :numericality => { only_integer: true, greater_than_or_equal_to: 3, less_than: 50 }

  friendly_id :title, :use => :slugged

  def subscribe?(user)
    feed_users.include?(user)
  end

  private

  def validate_include(user)
    raise ActiveRecord::Rollback if subscribe?(user)
  end
end
