class Banner < ActiveRecord::Base
  include Extensions::ActiveRecord::CachedQueries

  if Rails.env.production?
    self.cached_queries_expires_in = 1.day
    self.cached_queries_with_tags = true
    self.cached_queries_local_expires_in = 1.day
  end

  PLACE_HEADER_LEFT = 'Левый в шапке'.freeze
  PLACE_HEADER_RIGHT = 'Правый в шапке'.freeze
  PLACE_STRETCH = 'Растяжка'.freeze
  PLACE_SIDEBAR_LEFT = 'Левый сайдбар'.freeze
  PLACE_SIDEBAR_RIGHT = 'Правый сайдбар'.freeze
  PLACE_FOOTER_LEFT = 'Левый в подвале'.freeze
  PLACE_FOOTER_RIGHT = 'Правый в подвале'.freeze

  BANNER_PLACES = [PLACE_HEADER_LEFT, PLACE_HEADER_RIGHT, PLACE_STRETCH, PLACE_SIDEBAR_LEFT,
                   PLACE_SIDEBAR_RIGHT, PLACE_FOOTER_LEFT, PLACE_FOOTER_RIGHT].freeze

  CLICKS_KEY = 'banners_clicks'.freeze
  VIEWS_KEY = 'banners_views'.freeze

  scope :published, -> { where(:published => true) }
  scope :actual, -> { where('max_clicks > 0 AND max_clicks < clicks OR max_clicks = 0') }

  attr_accessible :name, :banner, :max_clicks, :link, :place, :published

  after_save :cache_in_redis, :if => proc { published_changed? }

  has_attached_file :banner,
                    :default_url => '/assets/banners/:style/missing.jpg',
                    :styles => {
                      :thumb => ['200x', :gif]
                    },
                    :convert_options => {
                      :original => '-srtip -quality 85',
                      :thumb => '-strip -quality 85'
                    },
                    :url  => '/system/uba_images/:id/:style/:hash.:extension',
                    :path => ':rails_root/public/system/uba_images/:id/:style/:hash.:extension',
                    :hash_secret => 'banners'

  validates :name, :presence => true,
                   :length => {:maximum => 255, :minimum => 3}

  validates :link, :url => true

  validates :max_clicks, :presence => true, :numericality => {:only_integer => true}

  validates :place, :presence => true,
                    :inclusion => {:in => BANNER_PLACES}


  validates_attachment_size :banner, less_than: 4.megabytes
  validates_attachment_content_type :banner, content_type: /\Aimage\/(pjpeg|jpeg|png|gif)\Z/
  validates_attachment_file_name :banner, matches: [/png\Z/i, /p?jpe?g\Z/i, /gif\Z/i]

  class << self
    # TODO: исправить
    # Public: Получить случайный баннер по месту
    #
    # place - String, место баннера
    #
    # Returns Banner instance
    def random_by(place)
      scope = actual.published.where(:place => place)
      offset_record = rand(scope.count)
      scope.offset(offset_record).first
    end
  end

  def ctr
    return 0 if views.zero?
    ((clicks.to_f / views).round(2) * 100).round
  end

  def inc_view
    Redis.current.hincrby(VIEWS_KEY, id, 1)
  end

  def inc_click
    Redis.current.hincrby(CLICKS_KEY, id, 1)
  end

  private

  # Internal: Кешируем в Redis
  def cache_in_redis
    current_ids = Redis.current.hget('banners', place).to_s.split(',').map(&:to_i)
    return if current_ids.include?(id)

    Redis.current.hset('banners', place, (current_ids << id).join(','))
  end
end
