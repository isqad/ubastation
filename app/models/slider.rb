# coding: utf-8
# == Schema Information
#
# Table name: sliders
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  slide_file_name    :string(255)
#  slide_content_type :string(255)
#  slide_file_size    :integer
#  slide_updated_at   :datetime
#  slide_meta         :text
#  position           :integer          default(0), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Slider < ActiveRecord::Base
  include Concerns::Imageable

  attr_accessible :name, :slide, :position

  scope :ordered, ->{ order('position ASC, created_at DESC, id DESC') }

  has_attached_file :slide,
                    :default_url => "/assets/:class/:style/missing.jpg",
                    :styles => {
                      :original => ['1024x768>', :jpg],
                      :thumb => ['180x', :jpg],
                      :big => ['940x', :jpg]
                    },
                    :convert_options => {
                      :original => '-strip -interlace Plane -quality 85',
                      :thumb => '-strip -quality 85 -gravity center -crop 180x35+0+0 +repage',
                      :big => '-strip -interlace Plane -quality 85 -gravity center -crop 940x190+0+0 +repage'
                    },
                    :url  => DEFAULT_IMAGE_URL

  validates_attachment_presence :slide
  validates_attachment_size :slide, less_than: 4.megabytes
  validates_attachment_content_type :slide, content_type: /\Aimage\/(pjpeg|jpeg|png|gif)\Z/
  validates_attachment_file_name :slide, matches: [/png\Z/i, /p?jpe?g\Z/i, /gif\Z/i]

  validates :name, :length => {:maximum => 120}

  after_destroy :normalize_position
  before_create :set_position

  class << self
    # Public: нормализация порядковых номеров
    #
    #
    # Returns nothing
    def normalize_positions
      table = quoted_table_name
      connection.execute <<-SQL
        UPDATE #{table} i SET position = t.real_position
        FROM
        (
          SELECT
            id,
            row_number() OVER (ORDER BY position, created_at DESC, id DESC) AS real_position
          FROM
            #{table}
        ) t
        WHERE i.id = t.id
      SQL
    end
  end

  def parent_id
    nil
  end

  def rebuild_position(prev_id, next_id)
    position_prev = self.class.find_by_id(prev_id).try(:position)
    position_next = self.class.find_by_id(next_id).try(:position)

    if position_prev.present? && position > position_prev
      delta = 1
      conditions = ['position < ? AND position > ?', position, position_prev]
      current_position = position_prev + 1
    elsif position_prev.nil? && position_next.present? && position > position_next
      delta = 1
      conditions = ['position >= ?', position_next]
      current_position = 1
    elsif position_prev.present? && position < position_prev
      delta = -1
      conditions = ['position <= ? AND position > ?', position_prev, position]
      current_position = position_prev
    end

    ActiveRecord::Base.transaction do
      self.class.where(conditions).update_all("position = position + #{delta}")

      self.class.update_all({:position => current_position}, {:id => id})
    end
  end

  protected

  def set_position
    self.position = self.class.maximum(:position).to_i + 1
  end

  def normalize_position
    self.class.normalize_positions
  end
end
