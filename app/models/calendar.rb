# == Schema Information
#
# Table name: calendars
#
#  id            :integer          not null, primary key
#  title         :string(255)      not null
#  start_at      :datetime         not null
#  finish_at     :datetime         not null
#  place         :string(255)      not null
#  organizations :string(255)      not null
#  disciplines   :string(255)      not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Calendar < ActiveRecord::Base
  include Concerns::PgExtension

  # Public: Кол-во последних событий
  MAX_LATEST_EVENTS = 5

  attr_accessible :title, :start_at, :finish_at, :place, :organizations, :disciplines

  validates :title, :place, :organizations, :disciplines, :presence => true, :length => {:maximum => 255, :minimum => 3}
  validates :start_at, :finish_at, :presence => true

  validate :start_must_be_before_end_time

  class << self
    def ordered
      select('calendars.*')
      .select('(start_at >= current_timestamp) AS future_flag')
      .select('(start_at < current_timestamp AND finish_at >= current_timestamp) AS actual_flag')
      .order('actual_flag DESC, future_flag DESC, start_at ASC')
    end

    def live(year)
      year = Time.current.year unless year

      ordered
      .where("(EXTRACT(DAY FROM calendars.finish_at#{at_time_zone} - current_timestamp#{at_time_zone}) >= 0) = ?", true)
      .where('EXTRACT(YEAR FROM calendars.start_at) = ?', year)
    end

    def archive(year)
      year = Time.current.year unless year

      where("(EXTRACT(DAY FROM calendars.finish_at#{at_time_zone} - current_timestamp#{at_time_zone}) < 0) = ?", true)
      .where('EXTRACT(YEAR FROM calendars.start_at) = ?', year)
      .order('calendars.start_at DESC')
    end

    def latest
      ordered.limit MAX_LATEST_EVENTS
    end

    # Get exists years
    def years
      years = []

      where('calendars.start_at IS NOT NULL')
      .group("EXTRACT(YEAR FROM calendars.start_at#{at_time_zone})")
      .count
      .sort_by { |key, _| -key.to_i }
      .each { |year, _| years << year.to_i }

      years
    end
  end

  private

  def start_must_be_before_end_time
    return if start_at.present? && finish_at.present? && start_at <= finish_at
    errors.add(:start_at, :start_must_be_before_end_time)
  end
end
