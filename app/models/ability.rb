class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    # Discussions ability
    can [:index, :create], Discussion
    can :read, Discussion do |discussion|
      discussion.can_participate?(user)
    end

    # Forum ability
    can [:index, :show, :subscribe, :unsubscribe], Forum
    # Topic
    can [:show, :new, :create, :subscribe, :unsubscribe], Topic
    can [:destroy, :close, :open, :edit, :update], Topic do |topic|
      user.role?(:forum_moderator) || topic.try(:user) == user
    end
    can [:pin, :unpin], Topic do
      user.role? :forum_moderator
    end
    can :manage, Topic do |topic|
      user.role?(:forum_moderator) || topic.try(:user) == user
    end
    # Post
    can :create, Post do |post|
      !post.try(:topic).closed? && user.persisted?
    end
    can [:edit, :update], Post do |post|
      post.try(:user) == user && !post.topic.closed?
    end

    can [:answer, :cite], Post do |post|
      post.try(:user) != user && !post.topic.closed? && user.persisted?
    end
    can :destroy, Post do |post|
      (user.role?(:forum_moderator) || post.try(:user) == user) && !post.first?
    end
    can :complaint, Post do |post|
      user.persisted? && !post.topic.closed? && post.try(:user) != user &&
        !user.role?(:forum_moderator) && post.approved? && !post.try(:user).role?(:forum_moderator)
    end

    # Forum moderation
    can :moderate, [Forum, Topic] do |forum|
      user.role? :forum_moderator
    end
    can [:moderate, :spam, :approve], Post do |post|
      user.role?(:forum_moderator) && post.pending_review?
    end

    can :moderate, Comment do |forum|
      user.role? :comment_moderator
    end


    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
