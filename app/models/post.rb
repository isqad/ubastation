# encoding: utf-8
# == Schema Information
#
# Table name: posts
#
#  id         :integer          not null, primary key
#  body       :text
#  user_id    :integer
#  topic_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  state      :string(255)      default("approved")
#

class Post < ActiveRecord::Base
  CHANGED_THRESHOLD = 1.minute

  include Workflow
  include PublicActivity::Model
  include Concerns::SphinxRtCallbacks

  scope :ordered, -> { order(arel_table[:position].asc) }

  belongs_to :user
  belongs_to :topic, :counter_cache => true

  attr_accessible :body, :topic, :user, :documents, :state

  validates :body, :presence => true, :length => {:minimum => 2, :maximum => 10000}

  delegate :name, :to => :user, :prefix => true
  delegate :title, :to => :topic, :prefix => true

  workflow_column :state
  workflow do
    state :approved do
      event :review, :transitions_to => :pending_review
      event :spam,    :transitions_to => :spam
      event :approve, :transitions_to => :approved
    end
    state :pending_review do
      event :spam,    :transitions_to => :spam
      event :approve, :transitions_to => :approved
    end
    state :spam
  end

  after_create :touch_topic, :set_create_activity, :subscribed_users_notification
  after_update :set_update_activity

  before_destroy { activities.each { |a| a.destroy } }

  acts_as_list scope: :topic_id

  def title
    body
  end

  def page_for_pagination(per_page)
    page = (position.to_f / per_page).round
    return 1 unless page > 0
    page
  end

  def edited?
    updated_at - created_at > CHANGED_THRESHOLD
  end

  def url_path
    return @url_path if defined? @url_path
    page = page_for_pagination(topic.forum.posts_per_page)
    @url_path = "#{Rails.application.routes.url_helpers.forum_topic_url(topic.forum, topic, page: page)}#post-#{id}"
  end

  private

  def set_create_activity
    create_activity :key => 'post.create', :owner => user
  end

  def set_update_activity
    create_activity :key => 'post.update', :owner => user
  end

  def subscribed_users_notification
    Resque.enqueue ForumPostNotificationJob, id
  end

  def touch_topic
    topic.touch
  end
end
