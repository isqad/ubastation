# == Schema Information
#
# Table name: competitions
#
#  id                           :integer          not null, primary key
#  title                        :string(255)      not null
#  preview                      :text             not null
#  slug                         :string(255)
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  thumbnail_image_file_name    :string(255)
#  thumbnail_image_content_type :string(255)
#  thumbnail_image_file_size    :integer
#  thumbnail_image_updated_at   :datetime
#  thumbnail_image_meta         :text
#  thumbnail_video              :text
#  start_at                     :datetime
#  video_link                   :string(255)
#  year                         :integer          not null
#

class Competition < ActiveRecord::Base
  include Concerns::PgExtension
  extend FriendlyId

  attr_accessible :title, :preview, :thumbnail_video, :thumbnail_image, :start_at, :video_link

  scope :ordered, order(arel_table[:start_at].desc)

  has_many :competition_articles, :dependent => :destroy, :order => 'competition_articles.created_at DESC'

  validates :title, :presence => true, :length => {:maximum => 255, :minimum => 3}
  validates :preview, :presence => true, :length => {:minimum => 3}
  validates :start_at, :presence => true

  validates :video_link, :allow_blank => true,
                         :format => {:with => /\A(https?:\/\/)?(www\.)?((vimeo\.com\/\d+)|(youtube\.com\/watch\?v=[-a-zA-Z0-9.%_]+)|(youtu\.be\/[-a-zA-Z0-9.%_]+))\z/i}

  after_initialize :default_start_at

  before_save :sanitize_preview, :if => proc { preview_changed? }
  before_validation :check_video

  friendly_id :title, :use => :slugged

  has_attached_file :thumbnail_image,
                    :default_url => '/assets/:class/:style/missing.jpg',
                    :styles => {
                      :original => ['1024x768>', :jpg],
                      :thumb => ['712x', :jpg],
                      :mini => ['x120', :jpg]
                    },
                    :convert_options => {
                      :original => '-strip -interlace Plane -quality 85',
                      :thumb => '-strip  -interlace Plane -quality 85 -gravity north -crop 712x534+0+0 +repage',
                      :mini => '-strip -quality 85 -gravity center -crop 160x120+0+0 +repage'
                    },
                    :url  => '/system/:class/:id/:style/:basename.:extension',
                    :path => ':rails_root/public/system/:class/:id/:style/:filename'

  validates_attachment_size :thumbnail_image, less_than: 4.megabytes
  validates_attachment_content_type :thumbnail_image, content_type: /\Aimage\/(pjpeg|jpeg|png|gif)\Z/
  validates_attachment_file_name :thumbnail_image, matches: [/png\Z/i, /p?jpe?g\Z/i, /gif\Z/i]

  class << self
    # Get exists years
    def years
      select('year').group('year').order(arel_table[:year].desc).map(&:year)
    end

    def by_year(year = nil)
      return ordered unless year.present?

      where(:year => year).ordered
    end
  end

  private

  def default_start_at
    self.start_at ||= Time.current
  end

  def sanitize_preview
    preview.gsub!(%r{^<p>\n*\s*&nbsp;\n*\s*</p>}, ''.freeze)
    preview.gsub!(%r{<img.*(style=".*").*}) { |m| m.gsub!($1, ''.freeze) }
  end

  def check_video

    return unless video_link.present?

    video_info = VideoInfo.new(video_link)

    unless video_info.available?
      errors.add(:video_link, :invalid_video_url, :link => video_link)
      return false
    end

    self.thumbnail_video = video_info.embed_code
    self.thumbnail_image = nil if thumbnail_image.present?
  end

end
