class Topic < ActiveRecord::Base
  extend FriendlyId
  include PublicActivity::Model

  HIT_VIEW_THRESHOLD = 5.minutes

  STATE_PENDING = 'pending'.freeze
  STATE_ACCEPTED = 'accepted'.freeze
  STATE_REJECTED = 'rejected'.freeze

  attr_accessible :title, :posts_attributes, :user, :forum, :pinned, :closed, :closer, :hits, :state

  belongs_to :user
  belongs_to :forum, :counter_cache => true

  belongs_to :closer, :class_name => 'User', :foreign_key => 'closer_id'

  has_many :posts, :dependent => :destroy

  has_many :subscriptions, dependent: :destroy
  has_many :feed_users, through: :subscriptions, class_name: 'User', source: :user, before_add: :validate_include

  validates :title, :presence => true,
                    :length => {:minimum => 3, :maximum => 120}

  validates :user, :presence => true

  delegate :name, :to => :user, :prefix => true, :allow_nil => true
  delegate :title, :posts_per_page, :to => :forum, :prefix => true, :allow_nil => true
  delegate :name, :to => :closer, :prefix => true, :allow_nil => true

  accepts_nested_attributes_for :posts

  friendly_id :title, :use => :slugged

  scope :published, -> { where(state: STATE_ACCEPTED) }
  scope :ordered, ->{ order(arel_table[:updated_at].desc) }
  scope :pinned, ->{ order(arel_table[:pinned].desc) }
  scope :latest, ->(limit) { ordered.includes(:forum).limit(limit) }

  # workaround
  before_validation { state.nil? && (self.state = STATE_PENDING); nil }

  before_create :set_first_post_user, if: proc { posts.present? }
  after_create :send_notifications_for_subscribers, :create_topic_activity

  before_destroy { activities.each(&:destroy) }

  def subscribe?(user)
    return @subscribed if defined? @subscribed
    @subscribed = feed_users.map(&:id).include?(user.id)
  end

  def add_subscriber(user)
    feed_users << user
  end

  def remove_subscriber(user)
    feed_users.delete user
  end

  def open_by!(user)
    return unless closed?
    self.class.update_all({closed: false, closer_id: nil}, {id: id})
  end

  def close_by!(user)
    return if closed?
    self.class.update_all({closed: true, closer_id: user.id}, {id: id})
  end

  def pin!
    return if pinned?
    self.class.update_all({pinned: true}, {id: id})
  end

  def unpin!
    return unless pinned?
    self.class.update_all({pinned: false}, {id: id})
  end

  def last_page(per_page = 10)
    (posts_count.to_f / per_page.to_i).ceil
  end

  def last_post
    posts.ordered.last
  end

  def register_view
    return unless allow_register_hit?
    self.class.update_all({hits: hits + 1}, {id: id})
  end

  def allow_register_hit?
    created_at.in_time_zone + HIT_VIEW_THRESHOLD <= Time.current
  end

  protected

  def validate_include(user)
    raise ActiveRecord::Rollback if feed_users.include? user
  end

  private

  def set_first_post_user
    posts.first.user_id = user_id
  end

  def send_notifications_for_subscribers
    forum.feed_users.where('users.id <> ?', user.id).each do |subscriber|
      ForumMailer.new_topic(subscriber.id, id).deliver
    end
  end

  def create_topic_activity
    create_activity key: 'topic.create', owner: user
  end
end
