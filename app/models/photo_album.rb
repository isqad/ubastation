# encoding: utf-8
# == Schema Information
#
# Table name: photo_albums
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  description        :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  slug               :string(255)
#  published          :boolean          default(FALSE), not null
#  coverable_id       :integer
#  coverable_type     :string(255)
#  cover_file_name    :string(255)
#  cover_content_type :string(255)
#  cover_file_size    :integer
#  cover_updated_at   :datetime
#

class PhotoAlbum < ActiveRecord::Base
  extend FriendlyId
  include Concerns::Imageable
  # поведение Photoable
  include Concerns::Photoable
  # поведение Videoable
  include Concerns::Videoable
  include Concerns::SphinxRtCallbacks

  friendly_id :name, :use => :slugged

  # модель имеет обложку (Фото либо Видео)
  belongs_to :coverable, :polymorphic => true

  attr_accessible :name, :description, :coverable, :cover,
                  :photos_attributes, :videos_attributes, :published, :year, :sort, :order

  has_attached_file :cover,
                    :default_url => '/assets/:class/:style/missing.jpg',
                    :styles => {
                      :original => {
                        :geometry => '1024x768>',
                        :animated => false,
                        :format => 'jpg png gif'
                      },
                      :big => {
                        :geometry => '712x',
                        :animated => false,
                        :format => 'jpg png gif'
                      },
                      :thumb => {
                        :geometry => 'x120',
                        :animated => false,
                        :format => 'jpg png gif'
                      }
                    },
                    :convert_options => {
                      :original => '-strip -interlace Plane -quality 85',
                      :big => '-strip -interlace Plane -quality 85 -gravity north -crop 712x534+0+0 +repage',
                      :thumb => '-strip -quality 85 -gravity center -crop 162x120+0+0 +repage'
                    },
                    :url  => DEFAULT_IMAGE_URL

  scope :ordered, ->{ order(arel_table[:created_at].desc) }
  scope :live, ->{ where(:published => true).ordered }
  scope :by_year, ->(year) { live.where(:year => year) }

  accepts_nested_attributes_for :photos, :allow_destroy => true
  accepts_nested_attributes_for :videos, :allow_destroy => true

  validates_attachment_size :cover, less_than: 4.megabytes
  validates_attachment_content_type :cover, content_type: /\Aimage\/(pjpeg|jpeg|png|gif)\Z/
  validates_attachment_file_name :cover, matches: [/png\Z/i, /p?jpe?g\Z/i, /gif\Z/i]

  validates :name, :presence => true,
                   :length => {:maximum => 50, :minimum => 3},
                   :uniqueness => {:case_sensitive => false}

  validates :description, :length => {:maximum => 4000}

  # очищаем от лишнего
  # before_save :sanitize_description, :if => proc { description.present? && description_changed? }

  before_create :set_year
  after_save :clear_coverable

  def self.years
    select('year').order(arel_table[:year].desc).group('year').map(&:year)
  end

  self.per_page = 10

  def title
    name
  end

  def no_cover?
    !cover.exists? && coverable.nil?
  end

  def url_path
    @url_path ||= Rails.application.routes.url_helpers.media_album_url(year, self)
  end

  def body
    description
  end

  private

  def sanitize_description
    self.description = description.gsub!(%r{\A<p>\n*\s*&nbsp;\n*\s*</p>}i, '')
  end

  def clear_coverable
    update_attributes(:coverable => nil) if cover.exists? && coverable.present?
  end

  def set_year
    self.year = created_at.present? ? created_at.year : Time.now.utc.year
  end
end
