# == Schema Information
#
# Table name: subscription_forums
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  forum_id   :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SubscriptionForum < ActiveRecord::Base
  attr_accessible :user, :forum

  belongs_to :forum
  belongs_to :user

  validates :user, :presence => true
  validates :forum, :presence => true

  after_create :subscribe_topics
  before_destroy :unsubscribe_topics

  protected
  def subscribe_topics
    forum.topics.each do |t|
      t.feed_users << user
    end
  end

  def unsubscribe_topics
    forum.topics.each do |t|
      t.subscriptions.where(user_id: user.id).delete_all
    end
  end
end
