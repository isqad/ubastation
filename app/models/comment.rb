# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  title            :string(50)       default("")
#  comment          :text
#  commentable_id   :integer
#  commentable_type :string(255)
#  user_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Comment < ActiveRecord::Base
  include ActsAsCommentable::Comment
  include PublicActivity::Model

  attr_accessible :comment, :user

  belongs_to :commentable, polymorphic: true

  belongs_to :user

  scope :ordered, ->{ order(arel_table[:created_at].desc) }
  scope :with_users, ->{ includes(:user) }

  delegate :name, :avatar, to: :user, prefix: true

  before_destroy :clean_activities

  def share(link)
    Resque.enqueue ShareToSocialNetworkJob, user_id, comment, link
  end

  private

  def clean_activities
    activities.destroy_all
  end
end
