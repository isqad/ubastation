# == Schema Information
#
# Table name: competition_articles
#
#  id             :integer          not null, primary key
#  title          :string(255)      not null
#  body           :text             not null
#  link           :string(255)
#  show_comments  :boolean          default(TRUE), not null
#  competition_id :integer          not null
#  slug           :string(255)      not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class CompetitionArticle < ActiveRecord::Base
  extend FriendlyId

  attr_accessible :title, :body, :link, :show_comments, :competition_id

  scope :ordered, order('created_at DESC')

  belongs_to :competition

  validates :title, :presence => true, :length => {:maximum => 255, :minimum => 3}
  validates :body, :allow_blank => true, :length => {:minimum => 3}
  validates :link, :allow_blank => true, :url => true
  validates :competition, :presence => true

  before_save :clean_body

  friendly_id :title, :use => :slugged

  acts_as_commentable

  private

  def clean_body
    body.gsub!(%r{\A<p>\n*\s*&nbsp;\n*\s*</p>}, ''.freeze)
  end
end
