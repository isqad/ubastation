class TwitterClient
  include Singleton

  def client
    @client ||= Twitter::REST::Client.new(
      consumer_key: Settings.uba.twitter_stream.consumer_key,
      consumer_secret: Settings.uba.twitter_stream.consumer_secret,
      access_token: Settings.uba.twitter_stream.access_token,
      access_token_secret: Settings.uba.twitter_stream.access_token_secret
    )
  end
end
