# == Schema Information
#
# Table name: news
#
#  id                 :integer          not null, primary key
#  title              :string(255)      not null
#  slug               :string(255)
#  anonce             :string(255)      not null
#  body               :text
#  photo_file_name    :string(255)
#  photo_content_type :string(255)
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#  published          :boolean          default(TRUE), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  photo_meta         :text
#  external_link      :string(255)
#  year               :integer          not null
#  month              :integer          not null
#

class News < ActiveRecord::Base
  extend FriendlyId

  include Concerns::PgExtension
  # поведение Photoable
  include Concerns::Photoable
  # поведение Videoable
  include Concerns::Videoable
  include Concerns::Viewable
  include Concerns::SphinxRtCallbacks


  attr_accessible :title, :body, :anonce, :published,
                  :photo, :meta_description, :browser_title,
                  :photos_attributes, :videos_attributes,
                  :external_link

  accepts_nested_attributes_for :photos, :allow_destroy => true
  accepts_nested_attributes_for :videos, :allow_destroy => true

  friendly_id :title, :use => :slugged

  has_attached_file :photo,
                    :default_url => "/assets/:class/:style/missing.jpg",
                    :styles => {
                      :original => ['1024x768>', :jpg],
                      :medium => ["180x", :jpg],
                      :thumb => ["x80", :jpg]
                    },
                    :convert_options => {
                      :original => '-strip -interlace Plane -quality 85',
                      :thumb => "-strip -quality 85 -gravity center -crop 110x80+0+0 +repage",
                      :medium => "-strip -quality 85 -gravity north -crop 180x240+0+0 +repage"
                    },
                    :url  => "/system/:class/:id/:style/:hash.:extension",
                    :path => ":rails_root/public/system/:class/:id/:style/:hash.:extension",
                    :hash_secret => "uba_news"

  scope :published, -> { where(:published => true) }
  scope :live, -> { published.order(arel_table[:created_at].desc) }

  validates_attachment_presence :photo
  validates_attachment_size :photo, less_than: 4.megabytes
  validates_attachment_content_type :photo, content_type: /\Aimage\/(pjpeg|jpeg|png|gif)\Z/
  validates_attachment_file_name :photo, matches: [/png\Z/i, /p?jpe?g\Z/i, /gif\Z/i]

  validates :title, :presence => true,
                    :length => {:minimum => 3, :maximum => 255},
                    :uniqueness => {:case_sensitive => false}

  validates :anonce, :presence => true, :length => {:minimum => 3, :maximum => 233}

  validates :external_link, :url => true, :allow_blank => true

  is_seo_meta

  acts_as_commentable

  self.per_page = 10

  # очищаем от лишнего
  before_save do |news|
    news.body.gsub!(%r{^<p>\n*\s*&nbsp;\n*\s*</p>}, ''.freeze)
  end

  before_create :set_year_and_month

  after_destroy :expire_cache

  class << self
    # Public: Вычисляет по году и месяцу
    #
    #  options - Hash, :year - Integer, :month - Integer
    #
    # Returns: News instance
    def archive_node(options = {})
      options.reverse_merge!(:year => Time.current.year, :month => Time.current.month)

      where(:year => options[:year].to_i, :month => options[:month].to_i)
    end

    def latest(nums = nil)
      limit = nums || homepage_size
      live.limit(limit)
    end

    def homepage_size
      size_setting = ActiveadminSettings::Helpers.settings_value('Количество новостей на главной')
      size_setting.present? ? size_setting.to_i : HOMEPAGE_SIZE
    end
  end

  def parsed_external_link
    return unless external?

    @uri ||= Addressable::URI.parse(external_link)
  end

  def external?
    !external_link.blank?
  end

  def internal?
    parsed_external_link && parsed_external_link.host =~ /\A(www\.)?#{::HOST}\Z/
  end

  def related_page
    return unless internal?

    @page ||= ::Page.where(joined_path: parsed_external_link.path).first
  end

  def related_comments
    return @_related_comments if defined?(@_related_comments)

    return (@_related_comments = related_page.comments) if related_page && related_page.show_comments?
    return (@_related_comments = nil) if external?

    @_related_comments = comments
  end

  def url_path
    @url_path ||= Rails.application.routes.url_helpers.news_tiding_url(year, month.to_s.rjust(2, '0'), self)
  end

  private

  def set_year_and_month
    now = Time.current
    self.year = now.year
    self.month = now.month
  end

  def expire_cache
    ActionController::Base.new.expire_fragment(NewsPresenter.latest_cache_key)
  end
end
