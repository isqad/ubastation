# encoding: utf-8
class SphinxRtCallbackJob
  @queue = :sphinx_rt_callbacks

  def self.perform(domain, instance_id)
    instance = domain.to_s.classify.constantize.find_by_id(instance_id)
    ThinkingSphinx::RealTime::Callbacks::RealTimeCallbacks.new(domain).after_save(instance) if instance.present?
  end
end
