class ShareToSocialNetworkJob
  @queue = :base

  def self.perform(user_id, comment, link)
    user = User.find(user_id)
    user.network_publish(comment, link, I18n.t('site_title'))
  end
end
