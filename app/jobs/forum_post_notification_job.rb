# encoding: utf-8
class ForumPostNotificationJob
  @queue = :base

  def self.perform(post_id)
    post = Post.find(post_id)
    post.topic.feed_users.select { |user| user.id != post.user_id }.each do |user|
      ForumMailer.new_post(user.id, post.id).deliver
    end
  end
end
