class TwitterStreamJob
  @queue = :cron

  def self.perform
    stream = TwitterClient.instance.client
      .user_timeline(Settings.uba.twitter_stream.user,
                     count: Settings.uba.twitter_stream.max_size_twits.to_i)

    Redis.current.set Settings.uba.twitter_stream.cache_key, Marshal.dump(stream)
  end
end
