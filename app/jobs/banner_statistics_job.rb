# Public: сброс статистики по баннерам
class BannerStatisticsJob
  def self.perform
    clicks, views = Redis.current.pipelined do
      Redis.current.hgetall(Banner::CLICKS_KEY)
      Redis.current.hgetall(Banner::VIEWS_KEY)
    end

    clicks.each do |banner_id, click_count|
      Banner.where(id: banner_id).update_all("clicks = clicks + #{click_count.to_i}")
    end

    views.each do |banner_id, view_count|
      Banner.where(id: banner_id).update_all("views = views + #{view_count.to_i}")
    end

    Redis.current.pipelined do
      Redis.current.del(Banner::CLICKS_KEY)
      Redis.current.del(Banner::VIEWS_KEY)
    end
  end
end
