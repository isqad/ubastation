class LastWeatherJob
  @queue = :cron

  def self.perform
    table = Nokogiri::HTML(open(Settings.uba.windguru.weather_url))
      .css('table.forecast-ram')
      .to_s
      .gsub(/<img(.+)src="(.+)"/i, "<img\\1src=\"https://www.windguru.cz/int/\\2\"")

    Redis.current.set(Settings.uba.windguru.cache_key, table)
  end
end
