jQuery ($) ->
  $.ajaxSetup
    cache: true

  $(document).on "click", ".archives .toggle, .competitions-archive .toggle", (event) ->
    event.preventDefault()
    ul = $(this).parent().find("ul:eq(0)")

    if ul.length == 0
      return false

    if ul.css("display") == "none"
      ul.fadeIn()
      $(this).html(" - ")
    else
      ul.hide()
      $(this).html(" + ")

    return false

  $(".news-item .hover, .competition-item .hover").on "mouseenter", () ->
    $(this).parent().parent().find("h1").find(".hover").css
      "text-decoration": "underline"

  $(".news-item .hover, .competition-item .hover").on "mouseleave", () ->
    $(this).parent().parent().find("h1").find(".hover").css
      "text-decoration": "none"

  $(".random-photo").on "mouseenter", () ->
    $(this).find(".description-overlay").css
      "padding": "3px"
      "height": "32px"

  $(".random-photo").on "mouseleave", () ->
    $(this).find(".description-overlay").css
      "padding": "0 3px"
      "height": "0"

  $("#slider-carousel").on "mouseenter", () ->
    $(this).find(".carousel-control").css
      "display": "block"

  $("#slider-carousel").on "mouseleave", () ->
    $(this).find(".carousel-control").css
      "display": "none"

  #$("a.show_images").colorbox
  #  initialWidth: 800
  #  scrolling: false
  #  fastIframe: false
  #  title: ""
  #  current: "Фотография {current} из {total}"
  #  onComplete: () ->
  #    $.colorbox.resize()
  #    $(".preview").find("a.next-photo").on "click", (e) ->
  #      $.colorbox.next()

  $(window).on "load", (event) ->
    if $("#messages_box").length > 0
      $("#messages_box").scrollTop($("#messages_box").get(0).scrollHeight + 10)
