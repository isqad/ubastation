CKEDITOR.plugins.setLang('embed', 'ru',
{
  embed :
  {
    title : "Вставить embed",
    button : "Вставить embed",
    pasteMsg : "Пожалуйста, вставьте embed-код с Youtube, Flickr и других ресурсов в прямоугольник, используя сочетание клавиш (Ctrl+V), и нажмите OK."
  }
});