﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function(config) {
  config.allowedContent = true;
  config.contentsCss = ['/assets/cke_forum.css'];
  // Define changes to default configuration here. For example:
  config.language = 'ru';
  //config.uiColor = '#ca0000';

  /* Filebrowser routes */
  // The location of an external file browser, that should be launched when "Browse Server" button is pressed.
  config.filebrowserBrowseUrl = '/ckeditor/attachment_files';

  // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Flash dialog.
  config.filebrowserFlashBrowseUrl = '/ckeditor/attachment_files';

  // The location of a script that handles file uploads in the Flash dialog.
  config.filebrowserFlashUploadUrl = '/ckeditor/attachment_files';

  // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Link tab of Image dialog.
  config.filebrowserImageBrowseLinkUrl = '/ckeditor/pictures';

  // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Image dialog.
  config.filebrowserImageBrowseUrl = '/ckeditor/pictures';

  // The location of a script that handles file uploads in the Image dialog.
  config.filebrowserImageUploadUrl = '/ckeditor/pictures';

  // The location of a script that handles file uploads.
  config.filebrowserUploadUrl = '/ckeditor/attachment_files';

  // Rails CSRF token
  config.filebrowserParams = function() {
    var csrf_token, csrf_param, meta,
        metas = document.getElementsByTagName('meta'),
        params = {};

    for (var i = 0 ; i < metas.length; i++) {
      meta = metas[i];

      switch(meta.name) {
        case 'csrf-token':
          csrf_token = meta.content;
          break;
        case 'csrf-param':
          csrf_param = meta.content;
          break;
        default:
          continue;
      }
    }

    if (csrf_param && csrf_token) {
      params[csrf_param] = csrf_token;
    }

    return params;
  };

  config.addQueryString = function(url, params){
    var queryString = [];

    if (!params) {
      return url;
    } else {
      for ( var i in params )
        queryString.push(i + "=" + encodeURIComponent(params[i]));
    }

    return url + ( ( url.indexOf( "?" ) != -1 ) ? "&" : "?" ) + queryString.join( "&" );
  };

  // Integrate Rails CSRF token into file upload dialogs (link, image, attachment and flash)
  CKEDITOR.on( 'dialogDefinition', function( ev ){
    // Take the dialog name and its definition from the event data.
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;
    var content, upload;

    if (CKEDITOR.tools.indexOf(['link', 'image', 'attachment', 'flash'], dialogName) > -1) {
      content = (dialogDefinition.getContents('Upload') || dialogDefinition.getContents('upload'));
      upload = (content == null ? null : content.get('upload'));

      if (upload && upload.filebrowser['params'] == null) {
        upload.filebrowser['params'] = config.filebrowserParams();
        upload.action = config.addQueryString(upload.action, upload.filebrowser['params']);
      }
    }

    if (dialogName == 'link') {
      dialogDefinition.getContents('target').get('linkTargetType')['default']='_blank';
    }

    if (dialogName == 'image') {
      dialogDefinition.getContents('info').get('txtWidth')['default']='712';
      dialogDefinition.getContents('info').get('txtHeight')['default']='534';
    }

    if ( dialogName == 'table' ) {

        var addCssClass = dialogDefinition.getContents('advanced').get('advCSSClasses');

        addCssClass['default'] = 'table table-bordered table-stripped';

        var infoTab = dialogDefinition.getContents('info');
        var cellSpacing = infoTab.get('txtCellSpace');
        cellSpacing['default'] = "0";
        var cellPadding = infoTab.get('txtCellPad');
        cellPadding['default'] = "0";
        var border = infoTab.get('txtBorder');
        border['default'] = "0";
        var width = infoTab.get('txtWidth');
        width['default'] = "100%";

    }
  });

  /* Extra plugins */
  // works only with en, ru, uk locales
  config.extraPlugins = 'lineutils,widget,oembed';

  /* Toolbars */
  config.toolbar = 'Easy';

  config.toolbar_Easy =
    [
      ['Undo','Redo','-','Cut','Copy','Paste','PasteText','PasteFromWord'],['Maximize'],
      ['SelectAll','RemoveFormat'],
      ['Maximize'], '/',
      ['Bold','Italic','Underline','Strike','TextColor','-','NumberedList','BulletedList','-','Blockquote'],
      ['Link','Unlink'], ['Image', 'oembed', 'Smiley']
    ];

  config.toolbar_Admin = [
    ['Source', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
    ['Maximize'],
    ['Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
    ['Link','Unlink'],
    ['Image', 'oembed'], '/',
    ['Format', 'Font', 'FontSize','-','Subscript', 'Superscript', 'TextColor', 'RemoveFormat'],
    ['Bold', 'Italic', 'Underline', 'Strike', '-', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'],
    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']
  ];

  //smiles
  config.smiley_path = '/assets/smiles/';

  config.smiley_images = [
    'sm4.gif',
    'sm5.gif',
    'sm6.gif',
    'sm7.gif',
    'sm8.gif',
    'sm9.gif',
    'sm3.gif',
    'sm2.gif',
    'sm11.gif',
    'sm12.gif',
    'sm13.gif',
    'sm10.gif',
    'sm14.gif',
    'sm15.gif',
    'sm16.gif',
    'sm17.gif',
    'sm1.gif',
    'sm18.gif',
    'sm19.gif',
    'sm20.gif',
    'sm21.gif',
    'sm22.gif'
  ];

  config.smiley_descriptions = [
    ':angel:',
    ':crazy:',
    ':cry:',
    ':dance:',
    ':glasses:',
    ':hoho:',
    ';)',
    ':D',
    ':look:',
    ':love:',
    ':music:',
    ':rofl:',
    ':(',
    ':scratch:',
    ':0',
    ':sick:',
    ':)',
    ':smoke:',
    ':stuk:',
    ':v:',
    ':zlo:',
    ':zst:'
  ];
};
