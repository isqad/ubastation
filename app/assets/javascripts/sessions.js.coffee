# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ($) ->
  $("a#user-sign-in").colorbox
    initialWidth: 800
    initialHeight: 382
    scrolling: false