//= require active_admin/base
//= require activeadmin_settings
//= require jquery.ui.nestedSortable
//= require custom-sortable
//= require ckeditor/init
//= require ui.datepicker-ru
//= require file-uploader/util
//= require file-uploader/button
//= require file-uploader/handler.base
//= require file-uploader/handler.form
//= require file-uploader/handler.xhr
//= require file-uploader/uploader.basic
//= require file-uploader/dnd
//= require file-uploader/uploader
//= require file-uploader/jquery-plugin
//= require rails-timeago
//= require ./vendor/jquery-treeview
//= require_self
//= require locales/jquery.timeago.ru.js
//= require admin_photo_albums
//= require admin_photos
//= require ./vendor/limit
//= require admin_news
//= require admin_pages

jQuery.timeago.settings.lang = 'ru';

$(document).ready(function () {
  $("a[_cke_saved_href]").each(function () {
    this.href = $(this).attr("_cke_saved_href");
  });

});


