jQuery ($) ->
  $("ul.dropdown li").hover (->
    $(this).addClass "hover"
    $("ul:first", this).css "visibility", "visible"
  ), ->
    $(this).removeClass "hover"
    $("ul:first", this).css "visibility", "hidden"