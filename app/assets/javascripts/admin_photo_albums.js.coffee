jQuery ($) ->

  url_show_in_top = $("#uploaded").data("topurl")
  url_in_top = $("#uploaded").data("intopurl")

  $(document).on "click", ".show_top_at_once", (event) ->
    cur = !$(@).prop("checked")

    $(".show_top_at_once").each(() ->
      $(@).prop("checked", false)
    )
    $(@).prop("checked", !cur)

    $.ajax
      url: url_show_in_top
      type: "POST"
      data:
        photo_id: $(@).data("id")
        at_once: Number(!cur)

  $(document).on "click", ".in_top", (event) ->
    cur = $(@).prop("checked")

    $.ajax
      url: url_in_top
      type: "POST"
      data:
        photo_id: $(@).data("id")
        top: Number(cur)


  $("#uploaded").sortable
    revert: 300
    tolerance: "pointer"
    update: (event, ui) ->
      url = $(@).data("rebuildurl")
      if typeof (url) isnt "undefined"
        $.ajax
          url: url
          type: "POST"
          data: $(@).sortable("serialize")
          error: () ->
            alert("Error with rebuild photos.")
  #$("#uploaded").disableSelection()

  errorHandler = (event, id, fileName, reason) ->
    qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason)

  successHandler = (event, id, fileName, response) ->
    $("#uploaded").prepend(response.template)

  if $("#file-upload").length == 1
    options =
      debug: false
      request:
        endpoint: $("#file-upload").data("path")
      text:
        uploadButton: "Выбрать файлы"
        cancelButton: "Отмена"
        waitingForResponse: "Обработка..."
        dropProcessing: "Обработка..."
        retryButton: "Повторить"
        failUpload: "Ошибки при загрузке"
        dragZone: "Положите файлы сюда"
      validation:
        allowedExtensions: ['jpeg', 'jpg', 'png', 'gif']
        sizeLimit: 4194304

    $("#file-upload").fineUploader(options).on("error", errorHandler).on("complete", successHandler)

  $("#add-video").on "click", (event) ->
    event.preventDefault()
    number_items = $(@).parent().find("li.string").length

    inputLi = $("<li></li>")
      .attr("id", "photo_album_videos_attributes_" + number_items + "_link_input")
      .addClass("string input optional")

    hiddenInputLi = $("<li></li>")
      .attr("id", "photo_album_videos_attributes_" + number_items + "_destroy_input")
      .addClass("hidden input optional")

    label = $("<label></label>")
      .attr("for", "photo_album_videos_attributes_" + number_items + "_link")
      .addClass("label")
      .text("Ссылка*")

    input = $("<input />")
      .attr({
        type: "text",
        id: "photo_album_videos_attributes_" + number_items + "_link",
        name: "photo_album[videos_attributes][" + number_items + "][link]"
      })

    destroyInput = $("<input />")
      .attr({
        type: "hidden",
        id: "photo_album_videos_attributes_" + number_items + "__destroy",
        name: "photo_album[videos_attributes][" + number_items + "][_destroy]",
        value: "false"
      })

    deleteLink = $("<p></p>")
      .addClass("inline-hints")
      .append($("<a></a>").addClass("js-delete-video").attr("href", "#").data("element", number_items).text("Удалить"))

    element = inputLi.append(label).append(input).append(deleteLink)

    $(@).parent().append(element).append(hiddenInputLi.append(destroyInput))

  $(document).on "click", ".js-delete-video", (event) ->
    event.preventDefault()

    $liElement = $(@).closest("li")
    $hiddenLi = $liElement.next("li")
    $liElement.hide()
    $hiddenLi.hide()
    $hiddenLi.find("input[type=hidden]").val(true)
