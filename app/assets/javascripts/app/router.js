define(["jquery", "underscore", "backbone", "views/photos/photos_view", "views/videos/videos_view", "collections/photos", "collections/videos", "views/top_photo/top_photo", "views/news/comment_form_view", "views/news/comments_view"], function(e, t, n, r, i, s, o, u, a, f) {
    var l = n.Router.extend({
            routes: {
                "": "defaultAction",
                "calendar(/:year)": "calendar",
                "competitions(/:year)/:competition_id/articles/:id": "articleComment",
                "user/login": "login",
                roadmap: "roadmap",
                "media(/:year)/:album_id/photos/:id/top.html": "topPhoto",
                "news/:year/:month/:album_id/photos/:id/top.html": "topPhoto",
                "media(/:year)/:album_id/photos/:id": "mediaAlbum",
                "media(/:year)/:album_id/videos/:id": "mediaAlbum",
                "news/:year/:month/:tiding_id/photos/:id": "mediaAlbum",
                "news/:year/:month/:tiding_id/videos/:id": "mediaAlbum",
                "media(/:year)/:album_id": "mediaAlbum",
                "news/:year/:month/:id": "mediaAlbum",
                "forums/:forum_id/topics/:topic_id/posts/new": "answerForum",
                "*path": "pageComments"
            }
        }),
        c = function() {
            var c;
            c = new l, c.on("route:answerForum", function() {
                e(window).scrollTop(document.body.scrollHeight);
            }), c.on("route:articleComment", function() {
                var r, i;
                i = location.pathname.split("/").pop();
                if (/^\/competitions/.test(location.pathname) && typeof i != "undefined" && e("#comments-news").length === 1) {
                    r = "/" + n.history.fragment, t.extend(a.prototype, {
                        baseUrl: r
                    }), t.extend(f.prototype, {
                        baseUrl: r
                    });
                    var s = new a;
                    s.render();
                    var o = new f;
                    o.render()
                }
            }), c.on("route:pageComments", function() {
                var n, r;
                r = location.pathname.split("/").pop();
                if (/^\/competitions/.test(location.pathname) === !1 && typeof r != "undefined" && e("#comments-news").length === 1) {
                    n = "/pages/" + r, t.extend(a.prototype, {
                        baseUrl: n
                    }), t.extend(f.prototype, {
                        baseUrl: n
                    });
                    var i = new a;
                    i.render();
                    var s = new f;
                    s.render()
                }
            }), c.on("route:defaultAction", function() {
                e.colorbox.close()
            }), c.on("route:mediaAlbum", function(n, u, l, c) {
                var h, p, d, v;
                typeof c != "undefined" ? /^.*\/videos\/\d+$/.test(location.pathname) ? d = new RegExp("^(.*)/videos/" + c + "$") : d = new RegExp("^(.*)/photos/" + c + "$") : (d = new RegExp("^(.*)/photos$"), e.colorbox.close()), v = location.pathname.replace(d, "$1"), t.extend(s.prototype, {
                    baseUrl: v
                }), t.extend(o.prototype, {
                    baseUrl: v
                }), h = new r, h.render(), p = new i, p.render(), typeof c != "undefined" && (/^.*\/photos\/\d+$/.test(location.pathname) ? window.setTimeout(function() {
                    h.collection.get(Number(c)).toggleShow()
                }, 700) : (e('a[href="#video-list"]').tab("show"), window.setTimeout(function() {
                    p.collection.get(Number(c)).toggleShow()
                }, 700)));
                if (/^\/news\/.*$/.test(location.pathname)) {
                    t.extend(a.prototype, {
                        baseUrl: v
                    }), t.extend(f.prototype, {
                        baseUrl: v
                    });
                    var m = new a;
                    m.render();
                    var g = new f;
                    g.render()
                }
            }), c.on("route:topPhoto", function(e, r, i, s) {
                var o, a;
                o = "/" + n.history.fragment.replace(/\.html$/, "") + ".json", t.extend(u.prototype, {
                    modelUrl: o
                }), a = new u
            }), c.on("route:calendar", function() {
                var t = e(window).scrollTop();
                e.colorbox({
                    href: "/" + n.history.fragment,
                    onComplete: function() {
                        e.colorbox.resize()
                    },
                    onClosed: function() {
                        c.navigate("/", {
                            trigger: !1,
                            replace: !0
                        }), e(window).scrollTop(t)
                    }
                })
            }), c.on("route:login", function() {
                var t = e(window).scrollTop();
                e.colorbox({
                    href: "/" + n.history.fragment,
                    onClosed: function() {
                        c.navigate("/", {
                            trigger: !1,
                            replace: !0
                        }), e(window).scrollTop(t)
                    }
                })
            }), c.on("route:roadmap", function() {
                var t = e(window).scrollTop();
                e.colorbox({
                    href: "/" + n.history.fragment,
                    title: "Схема проезда",
                    initialWidth: 800,
                    initialHeight: 600,
                    scrolling: !1,
                    onClosed: function() {
                        window.history.back(), e(window).scrollTop(t)
                    },
                    onComplete: function() {
                        return e.ajax({
                            url: "https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU",
                            dataType: "script",
                            success: function() {
                                return ymaps.ready(function() {
                                    var t, n, r, i, s, o, u = e("#roadmap");
                                    t = Number(u.attr("data-placemarkx")), n = Number(u.attr("data-placemarky")), r = u.attr("data-description"), i = new ymaps.Map("roadmap", {
                                        center: [n, t],
                                        zoom: 12
                                    }), i.controls.add("zoomControl"), i.controls.add("mapTools"), i.controls.add("typeSelector"), i.controls.add("scaleLine"), i.controls.add("miniMap"), i.controls.add("searchControl"), o = {
                                        iconImageHref: "/assets/marker.png",
                                        iconImageSize: [55, 32]
                                    }, s = new ymaps.Placemark([n, t], {
                                        content: r,
                                        balloonContent: r
                                    }, o), i.geoObjects.add(s)
                                })
                            }
                        })
                    }
                })
            }), n.history.start({
                pushState: Modernizr.history,
                silent: !0
            });
            if (!Modernizr.history) {
                var h = n.history.options.root.length,
                    p = window.location.pathname.substr(h);
                n.history.navigate(p, {
                    trigger: !0
                })
            } else n.history.loadUrl(n.history.getFragment());
            e(document).on("click", "a[rel=backbone]", function(t) {
                t.preventDefault();
                var n = e(this);
                c.navigate(n.attr("href").substring(1).replace(/#.*$/, ""), {
                    trigger: !0
                })
            })
        };
    return {
        initialize: c
    }
});
