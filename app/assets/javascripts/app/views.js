/*jshint devel:true */
/*global Backbone */
/*global app */
/*global console */
/*global JST */
app.views.Base=Backbone.View.extend({presenter:function(){"use strict";return this.defaultPresenter()},defaultPresenter:function(){"use strict";var e=this.model&&this.model.attributes?_.clone(this.model.attributes):{};return _.extend(e,{})},render:function(){"use strict";return this.template=JST["templates/"+this.templateName+"_tpl"],this.template||console.log(this.templateName?"no template for "+this.templateName:"no templateName specified"),this.$el.html(this.template(this.presenter())).attr("data-template",_.last(this.templateName.split("/"))),this}});