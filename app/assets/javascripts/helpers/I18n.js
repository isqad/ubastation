Anclav.I18n = {
    language: "ru",
    locale: {},
    loadLocale: function(locale, language) {
        this.locale = locale;
        this.language = language;
    },
    t: function(item) {
        var items = item.split("."),
            translatedMessage,
            nextNamespace;

        while(nextNamespace = items.shift()) {
            translatedMessage = (translatedMessage) ? translatedMessage[nextNamespace] : this.locale[nextNamespace];

            if(typeof translatedMessage === "undefined") {
                return "";
            }
        }

        return translatedMessage;
    }
};