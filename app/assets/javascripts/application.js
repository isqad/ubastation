// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.

//= require modernizr
//= require jquery
//= require jquery_ujs

//= require ckeditor/init
//= require vendor/localize
//= require colorbox
//= require bootstrap-transition
//= require bootstrap-carousel
//= require bootstrap-dropdown
//= require bootstrap-tab
//= require rails-timeago
//= require_self
//= require locales/jquery.timeago.ru.js
//= require file-uploader/util
//= require file-uploader/button
//= require file-uploader/handler.base
//= require file-uploader/handler.form
//= require file-uploader/handler.xhr
//= require file-uploader/uploader.basic
//= require file-uploader/dnd
//= require file-uploader/uploader
//= require file-uploader/jquery-plugin
//= require slider
//= require sessions
//= require main
//= require topics


jQuery.timeago.settings.lang = "ru";

/*jQuery.localize.fullMonths = [
    "Январь", "Февраль", "Март",
    "Апрель", "Май", "Июнь", "Июль",
    "Август", "Сентябрь", "Октябрь",
    "Ноябрь", "Декабрь"
];*/

jQuery.localize.format = ", dd.mm.yyyy, HH:MM";

$(document).ready(function () {
  $("a[_cke_saved_href]").each(function () {
    this.href = $(this).attr("_cke_saved_href");
  });
});
