jQuery ($) ->

  $("#sortable-photos").sortable
    revert: 300
    tolerance: "pointer"
    update: (event, ui) ->
      url = $(this).data("rebuildurl")
      $.ajax
        url: url
        type: "POST"
        data: $(this).sortable("serialize")
        error: () ->
          alert("Error with rebuild photos.")
  $("#sortable-photos").disableSelection()