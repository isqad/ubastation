class NewsPresenter
  # Public: ActionView::Base
  attr_reader :view

  def initialize(view)
    @view = view
  end

  def latest
    view.cache(self.class.latest_cache_key, expires_in: 15.minutes) do
      news = News.live.latest

      view.safe_concat(
        view.render('presenters/news/latest', :news => news)
      )
    end
  end

  def self.latest_cache_key
    last_updated_at = News.select('updated_at').
      order(News.arel_table[:updated_at].desc).
      first.
      try(:updated_at)

    cache_options = [last_updated_at]
    ActiveSupport::Cache.expand_cache_key(cache_options, 'presenters/news/latest')
  end
end
