class WeatherPresenter
  # Public: ActionView::Base
  attr_reader :view

  def initialize(view)
    @view = view
  end

  def show
    view.safe_concat(
      view.render('presenters/weather/show', :weather_table => weather_table)
    )
  end

  private

  def weather_table
    Redis.current.get(Settings.uba.windguru.cache_key)
  end
end
