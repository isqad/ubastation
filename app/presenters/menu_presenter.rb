class MenuPresenter
  # Public: ActionView::Base
  attr_reader :view

  def initialize(view)
    @view = view
  end

  def show(page_link)
    view.cache(show_cache_key(page_link)) do
      path = "/#{page_link.split('/'.freeze).reject(&:blank?).shift}" if page_link.present? && page_link != '/'.freeze
      pages = Page.fast_menu.arrange

      view.safe_concat(
        view.render('presenters/menu/show', path: path, pages: pages)
      )
    end
  end

  private

  def show_cache_key(page_link)
    last_updated_at = Page.select('updated_at').
      order(Page.arel_table[:updated_at].desc).
      first.
      try(:updated_at)

    cache_options = [page_link, last_updated_at]
    ActiveSupport::Cache.expand_cache_key(cache_options, 'presenters/menu/show')
  end
end
