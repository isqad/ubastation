class ForumPresenter
  COUNT_TOPICS_KEY = 'Количество последних постов виджета Форум'.freeze
  # Public: ActionView::Base
  attr_reader :view

  def initialize(view)
    @view = view
  end

  def latest_topics(maximum = 6)
    @maximum_topics = maximum

    view.cache(latest_topics_cache_key, expires_in: 10.minutes) do
      view.safe_concat(
        view.render('presenters/forum/latest_topics', topics: topics)
      )
    end
  end

  private

  def topics
    Topic.published.latest(@maximum_topics).to_a
  end

  def latest_topics_cache_key
    ActiveSupport::Cache.expand_cache_key([@maximum_topics], 'presenters/forum/latest_topics')
  end
end
