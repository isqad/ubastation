class FooterPresenter
  # Public: ActionView::Base
  attr_reader :view

  def initialize(view)
    @view = view
  end

  def show
    view.cache(show_cache_key) do
      view.safe_concat(
        view.render('presenters/footer/show')
      )
    end
  end

  private

  def show_cache_key
    last_updated_at = ActiveadminSettings::Setting.select('updated_at')
      .order(ActiveadminSettings::Setting.arel_table[:updated_at].desc)
      .first
      .updated_at

    cache_options = [last_updated_at]
    ActiveSupport::Cache.expand_cache_key(cache_options, 'presenters/footer/show')
  end
end
