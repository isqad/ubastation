class TwitterPresenter
  # Public: ActionView::Base
  attr_reader :view

  def initialize(view)
    @view = view
  end

  def show
    view.safe_concat(
      view.render('presenters/twitter/show', :twits => twits)
    )
  end

  private

  def twits
    stream = Redis.current.get(Settings.uba.twitter_stream.cache_key).to_s
      .force_encoding('ASCII-8BIT')

    stream.present? ? Marshal.load(stream) : []
  end
end
