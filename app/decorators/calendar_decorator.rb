# encoding: utf-8
class CalendarDecorator < Draper::Decorator
  delegate_all

  def due_days
    return @due_days if defined? @due_days

    now_day = Time.current.change :hour => 9, :min => 0

    @due_days = if now_day < model.start_at
      diff = (model.start_at - now_day).abs / 3600
      diff = diff > 24 ? (diff / 24).round : 1
      "#{diff} #{I18n.t('time.day', :count => diff)}"
    elsif now_day >= model.start_at && now_day <= model.finish_at
      %Q{<span style="color:#FE5720;">#{I18n.t('activerecord.attributes.calendar.now')}</span>}
    else
      I18n.t('activerecord.attributes.calendar.died')
    end
  end
end
