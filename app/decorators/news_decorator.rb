# encoding: utf-8
class NewsDecorator < Draper::Decorator
  delegate_all

  def url
    @url ||= external? ? external_link : h.news_tiding_url(year, month.to_s.rjust(2, '0'), object)
  end

  def target
    return @target if defined? @target

    return (@target = '_self') unless external?

    begin
      host = Addressable::URI.parse(external_link).host.gsub(/\Awww\./, ''.freeze)
    rescue
      return '_blank'
    end

    @target = host == Addressable::URI.parse(h.root_url).host.gsub(/\Awww\./, ''.freeze) ? '_self' : '_blank'
  end
end
