ActiveAdmin.register Photo do
  belongs_to :photo_album

  actions :show, :edit, :update, :destroy

  collection_action :rebuild, :method => :post do
    @photo_album = PhotoAlbum.find(params[:photo_album_id])

    params[:pic].each_with_index do |pic, key|
      Photo.update_all({position: key + 1}, {id: pic.to_i})
    end

    render :json => {:success => true}
  end

  collection_action :topurl, :method => :post do
    @photo_album = PhotoAlbum.find(params[:photo_album_id])

    @photo = @photo_album.photos.find(params[:photo_id])

    @photo.update_attributes(top_at_once: params[:at_once].to_i == 1)

    render :json => {:success => true}
  end

  collection_action :intop, :method => :post do
    @photo_album = PhotoAlbum.find(params[:photo_album_id])

    @photo = @photo_album.photos.find(params[:photo_id])

    @photo.update_attributes(in_top: params[:top].to_i == 1)

    render :json => {:success => true}
  end

end
