ActiveAdmin.register Video do
  belongs_to :photo_album

  filter :title
  filter :created_at

  index :as => :grid, :columns => 5 do |video|
    div link_to(image_tag(video.thumbnail.url(:thumb)), admin_photo_album_video_path(video.videoable, video))
    div video.title
    div "#{I18n.t('activerecord.attributes.video.duration')}: #{I18n.l(video.duration, :format => :duration)}"
    div do
      span link_to I18n.t('active_admin.edit'), edit_admin_photo_album_video_path(video.videoable, video), :class => 'member_link edit_link'
      span link_to I18n.t('active_admin.delete'), admin_photo_album_video_path(video.videoable, video),
                   :class => 'member_link edit_link',
                   :method => :delete,
                   :confirm => I18n.t('active_admin.delete_confirmation'), :class => 'member_link delete_link'
    end
  end
end
