ActiveAdmin.register Topic do
  menu :priority => 3

  index do
    selectable_column
    column :title
    column :state do |topic|
      if topic.state == Topic::STATE_ACCEPTED
        image_tag '/assets/online.png', :alt => 'yes', :title => 'Одобрен'
      else
        image_tag '/assets/offline.png', :alt => 'no', :title => 'Отлонен'
      end
    end
    column :pinned
    column :closed
    column :created_at
    column :updated_at
    default_actions
  end

  show do |forums|
    attributes_table do
      row :title
      row :state
      row :pinned
      row :closed
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  form do |f|
    f.inputs I18n.t('formtastic.details') do
      f.input :title
      f.input :closed
      f.input :pinned
      f.input :state, as: :radio, collection: [Topic::STATE_PENDING, Topic::STATE_ACCEPTED, Topic::STATE_REJECTED]
    end
    f.buttons
  end

  batch_action :accept do |ids|
    Topic.find(ids).each do |topic|
      topic.update_attributes!(state: Topic::STATE_ACCEPTED)
    end
    redirect_to collection_path
  end

  batch_action :reject do |ids|
    Topic.find(ids).each do |topic|
      topic.update_attributes!(state: Topic::STATE_REJECTED)
    end
    redirect_to collection_path
  end
end
