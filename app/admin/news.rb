# coding: utf-8
ActiveAdmin.register News do

  menu :priority => 5

  filter :title
  filter :created_at

  # Форма
  form :partial => 'form'

  # скоупы
  scope :all, :default => true

  scope :published do |news|
    news.where(:published => true)
  end

  scope :not_published do |news|
    news.where(:published => false)
  end

  # Главная страница
  index do
    selectable_column
    column :title do |news|
      link_to news.title, edit_admin_news_path(news)
    end
    column :published do |news|
      if news.published?
        image_tag '/assets/online.png', :alt => 'yes', :title => 'published'
      else
        image_tag '/assets/offline.png', :alt => 'no', :title => 'not published'
      end
    end
    column :created_at
    column :updated_at
    default_actions
  end

  # Страница показа
  show do |_|
    attributes_table do
      row :title
      row :photo do |news|
        image_tag news.photo.url(:thumb), :alt => news.title, :title => news.title
      end
      row :anonce
      row :created_at
      row :updated_at
      row :published do |news|
        news.published? ? I18n.t(:yes) : I18n.t(:no)
      end
      row :meta_description
      row :body do |page|
        page.body.html_safe
      end
    end
    active_admin_comments
  end

  member_action :rebuild, :method => :post do
    @news = News.find(params[:id])

    params[:pic].each_with_index do |pic, key|
      photo = @news.photos.find(pic.to_i)
      photo.update_attribute(:position, (key+1))
    end

    render :json => { :success => true }
  end

  member_action :topurl, :method => :post do
    @news = News.find(params[:id])

    @photo = @news.photos.find(params[:photo_id])

    if params[:at_once].to_i == 1
      Photo.transaction do
        Photo.find_each do |p|
          p.update_attributes!(:top_at_once => false)

          if p.top.present?
            if !p.in_top?
              p.top.destroy
            else
              p.top.update_attributes!(:show_in_top => false, :expired_at => nil)
            end
          end
        end

        @photo.update_attributes!(:top_at_once => true)
        if @photo.top.present?
          @photo.top.update_attributes!(:show_in_top => true, :expired_at => Time.now.in_time_zone('Ekaterinburg').end_of_day + 4.hours)
        else
          @photo.create_top!(:show_in_top => true, :expired_at => Time.now.in_time_zone('Ekaterinburg').end_of_day + 4.hours)
        end
      end
    else
      @photo.update_attributes!(:top_at_once => false)
      if @photo.top.present? && @photo.in_top?
        @photo.top.update_attributes!(:show_in_top => false, :expired_at => nil)
      elsif @photo.top.present? && !@photo.in_top?
        @photo.top.destroy
      end
    end

    render :json => { :success => true }

  end

  member_action :intop, :method => :post do
    @news = News.find(params[:id])

    @photo = @news.photos.find(params[:photo_id])

    Photo.transaction do
      if params[:top].to_i == 1
        @photo.update_attributes!(:in_top => true)
        if @photo.top.nil?
          @photo.create_top!
        end
      else
        @photo.update_attributes!(:in_top => false)
        if @photo.top.present? && !@photo.top_at_once?
          @photo.top.destroy
        end
      end
    end

    render :json => { :success => true }
  end

  # Прикрепление фотографии
  member_action :attach, :method => :post do

    if params.has_key?(:id)
      news = News.find(params[:id])
      @photo = news.photos.build
    else
      @photo = Photo.new
    end

    # For XHR file uploads, request.params[:qqfile] will be the path to the temporary file
    # For regular form uploads (such as those made by Opera), request.params[:qqfile] will be an UploadedFile which can be returned unaltered.
    if not request.params[:qqfile].is_a?(String)
      @photo.photo = params[:qqfile]
    else
      ######################## dealing with local files #############
      # get file name
      file_name = params[:qqfile]
      # get file content type
      att_content_type = (request.content_type.to_s == "") ? "application/octet-stream" : request.content_type.to_s
      # create tempora##l file
      begin
        file = Tempfile.new(file_name, {:encoding =>  'BINARY'})
        file.print request.raw_post.force_encoding('BINARY')
      rescue RuntimeError => e
        raise e unless e.message.include?('cannot generate tempfile')
        file = Tempfile.new(file_name) # Ruby 1.8 compatibility
        file.binmode
        file.print request.raw_post
      end
      # put data into this file from raw post request

      # create several required methods for this temporal file
      Tempfile.send(:define_method, "content_type") {return att_content_type}
      Tempfile.send(:define_method, "original_filename") {return file_name}
      @photo.photo = file
      # @photo.description = File.basename(file.original_filename, '.*')
    end

    if @photo.save
      # Photo.rebuild(album) if params.has_key?(:album_id)

      respond_to do |format|
        format.plaintext
      end
    else
      render :json => @photo.errors.to_json
    end
  end
end
