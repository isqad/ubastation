ActiveAdmin.register CompetitionArticle do
  belongs_to :competition

  form :partial => 'form'

  filter :title
  filter :competition

  index do
    selectable_column
    column :title
    column :link
    column :show_comments do |a|
      if a.show_comments?
        image_tag '/assets/online.png', :alt => 'yes'
      else
        image_tag '/assets/offline.png', :alt => 'no'
      end
    end
    column :created_at
    column :updated_at

    default_actions
  end

  show do |c|
    attributes_table do
      row :title
      row :competition
      row :show_comments
      row :body do |a|
        a.body.html_safe
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end
end
