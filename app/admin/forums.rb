ActiveAdmin.register Forum do
  menu :priority => 2

  index do
    selectable_column
    column :title
    column :topics_per_page
    column :posts_per_page
    column :created_at
    default_actions
  end

  show do |forums|
    attributes_table do
      row :title
      row :topics_per_page
      row :posts_per_page
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  form do |f|
    f.inputs I18n.t('formtastic.details') do
      f.input :title
      f.input :topics_per_page
      f.input :posts_per_page
    end
    f.buttons
  end

  filter :title
  filter :created_at
end
