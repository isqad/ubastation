ActiveAdmin.register User do

  before_filter do
    User.class_eval do
      def to_param
        id.to_s
      end
    end
  end

  form :partial => 'form'

  menu :priority => 7

  scope :all, :default => true

  scope :online do |users|
    users.where('last_request_at > ?', 15.minutes.ago)
  end

  index do
    selectable_column
    column :name
    column :email
    column :last_request_at
    column :last_sign_in_at
    column :sign_in_count
    default_actions
  end

  filter :email
  filter :name

  controller do
    def update
      if params[:user][:password].blank?
        params[:user].delete('password')
        params[:user].delete('password_confirmation')
      end

      @user = User.find(params[:id])

      if @user.update_attributes(params[:user])
        flash[:notice] = I18n.t('users.successfuly_updated')
        redirect_to edit_admin_user_url(@user)
      else
        render :edit
      end

    end
  end
end
