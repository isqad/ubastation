include PublicActivity::ViewHelpers

ActiveAdmin.register_page "Dashboard" do

  menu :priority => 1, :label => proc { I18n.t("active_admin.dashboard") }

  content :title => proc { I18n.t("active_admin.dashboard") } do
    activities = PublicActivity::Activity.limit(25).order(PublicActivity::Activity.arel_table[:created_at].desc).to_a

    if activities.present?
      columns do
        column do
          panel I18n.t("active_admin.activity_feed") do
            table_for activities.each do
              column(''.freeze) { |activity_item| render_activity(activity_item, layout: :activity_wrapper) }
            end
          end
        end
      end
    end
  end
end
