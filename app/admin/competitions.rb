ActiveAdmin.register Competition do
  menu :parent => I18n.t('competitions.events')

  form :partial => 'form'

  index do
    selectable_column
    column :title
    column :start_at do |competition|
      I18n.l(competition.start_at)
    end
    column ''.freeze do |c|
      link_to "#{I18n.t('activerecord.models.competition_article.other')} (#{c.competition_articles.size})", admin_competition_competition_articles_path(c)
    end
    default_actions
  end

  show do |c|
    attributes_table do
      row :title
      row :start_at do |competition|
        I18n.l(competition.start_at)
      end
      row :preview do |competition|
        competition.preview.html_safe
      end
    end
    active_admin_comments
  end

  filter :title
  filter :start_at

end
