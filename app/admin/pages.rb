# encoding: utf-8

ActiveAdmin.register Page do
  menu :priority => 6

  config.sort_order = "lft_asc"
  config.paginate = false

  # Кнопка на изменение порядка страниц
  action_item :only => [ :tree ] do
    link_to I18n.t("active_admin.pages.sortable"), manage_admin_pages_path
  end

  # фильтры
  filter :title
  filter :body
  filter :updated_at
  filter :created_at

  # Форма
  form :partial => 'form'

  # скоупы
  #scope :all, :default => true

  #scope :draft do |pages|
  #  pages.where('draft = ?', true)
  #end

  # Страница показа
  show do |page|
    attributes_table do
      row :title
      row :parent
      row :created_at
      row :updated_at
      row :draft
      row :nested_path
      row :link
      row :browser_title
      row :meta_description
      row :body do |page|
        page.body.html_safe
      end
    end
    active_admin_comments
  end

  # Главная страница
  #index do
    #column :title, :sortable => false do |page|
    #  if page.depth.to_i > 0
    #    span do
    #      "#{'&nbsp;&nbsp;'*page.depth.to_i*2}&raquo; #{link_to(page.title, edit_admin_page_path(page))}".html_safe
    #    end
    #  else
    #    link_to page.title, edit_admin_page_path(page)
    #  end
    #end
    #column :parent, :sortable => false
    #column "", :draft, :sortable => false do |page|
    #  if page.draft?
    #    I18n.t(:draft)
    #  end
    #end
    #column "", :in_menu, :sortable => false do |page|
    #  if !page.in_menu?
    #    I18n.t(:page_hidden)
    #  end
    #end
    #column :link, :sortable => false
    #default_actions
  #end

  # Page tree
  collection_action :tree, :method => :get do
    @pages = Page.ordered.arrange
    @page_title = 'Список страниц'
  end

  # страница изменения порядка
  collection_action :manage, :method => :get do
    @pages = Page.ordered
    @page_title = I18n.t("active_admin.pages.sortable")
  end

  # меняем порядок
  collection_action :rebuild, :method => :post do
    id        = params[:id].to_i
    parent_id = params[:parent_id].to_i
    prev_id   = params[:prev_id].to_i
    next_id   = params[:next_id].to_i

    render :text => I18n.t("active_admin.pages.do_nothing") and return if parent_id.zero? && prev_id.zero? && next_id.zero?

    page = Page.find(id)
    if prev_id.zero? && next_id.zero?
      page.move_to_child_of Page.find(parent_id)
    elsif !prev_id.zero?
      page.move_to_right_of Page.find(prev_id)
    elsif !next_id.zero?
      page.move_to_left_of Page.find(next_id)
    end

    render(:nothing => true)
  end

  controller do
    def index
      redirect_to tree_admin_pages_url
    end

    def create
      @page = Page.new(params[:page])

      if @page.save
        first_sibling = @page.siblings.first
        @page.move_to_left_of(first_sibling) if first_sibling

        redirect_to(admin_pages_url)
      else
        render :action => 'new'
      end
    end
  end

end
