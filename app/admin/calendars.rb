ActiveAdmin.register Calendar do

  menu :parent => I18n.t('competitions.events')

  form :partial => 'form'

  index do
    selectable_column
    column :title
    column :start_at
    column :finish_at
    column :place
    column :organizations
    column :disciplines
    default_actions
  end

  show do |c|
    attributes_table do
      row :title
      row :start_at
      row :finish_at
      row :place
      row :organizations
      row :disciplines

      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  filter :title
  filter :start_at
  filter :finish_at
  filter :place
  filter :disciplines
end
