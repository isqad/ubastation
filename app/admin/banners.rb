ActiveAdmin.register Banner do

  form :partial => 'form'

  filter :name
  filter :views
  filter :clicks
  filter :place

  index do
    selectable_column
    column :banner do |banner|
      image_tag banner.banner.url(:thumb), :size => banner.banner.image_size(:thumb)
    end
    column :name do |banner|
      link_to banner.name, edit_admin_banner_path(banner)
    end
    column :place
    column :max_clicks
    column :clicks
    column :views
    column :ctr
    default_actions
  end

  show do |banner|
    attributes_table do
      row :name
      row :banner do |b|
        image_tag b.banner.url(:thumb), :size => b.banner.image_size(:thumb)
      end
      row :place
      row :clicks
      row :views
      row :ctr
      row :max_clicks
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

end
