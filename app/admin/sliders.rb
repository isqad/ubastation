# coding: utf-8
ActiveAdmin.register Slider do
  filter :name
  filter :created_at

  config.sort_order = 'position_asc'

  # Кнопка на изменение порядка страниц
  action_item :only => [ :index ] do
    link_to I18n.t("sliders.sort"), manage_admin_sliders_path
  end

  index do
    selectable_column
    column :slide do |s|
      image_tag s.slide.url(:thumb), :size => s.slide.image_size(:thumb)
    end
    column :name
    column :created_at
    default_actions
  end

  show do |banner|
    attributes_table do
      row :name
      row :slide do |s|
        image_tag s.slide.url(:thumb), :size => s.slide.image_size(:thumb)
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  form do |f|
    f.inputs I18n.t('formtastic.details') do
      f.input :name
      f.input :slide
    end
    f.buttons
  end

  # страница изменения порядка
  collection_action :manage, :method => :get do
    @sliders = Slider.ordered
  end

  # меняем порядок
  collection_action :rebuild, :method => :post do
    id        = params[:id].to_i
    prev_id   = params[:prev_id].to_i
    next_id   = params[:next_id].to_i

    if prev_id.zero? && next_id.zero?
      render :text => I18n.t("active_admin.sliders.do_nothing")
      return
    end

    slider = Slider.find(id)
    slider.rebuild_position(prev_id, next_id)

    render :nothing => true
  end

end
