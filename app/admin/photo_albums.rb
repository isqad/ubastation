# :nocov:
ActiveAdmin.register PhotoAlbum do
  menu :priority => 1
  form :partial => 'form'

  config.sort_order = 'created_at_desc'

  # Кнопка на изменение обложки
  action_item :only => [ :show, :edit ] do
    link_to I18n.t('active_admin.photo_album.change_cover'), changecover_admin_photo_album_path(params[:id])
  end

  show do |photo_album|
    attributes_table do
      row :cover do |album|
        if album.coverable.nil? && album.cover.exists?
          image_tag album.cover.url(:thumb), :alt => album.name, :size => album.cover.image_size(:thumb)
        elsif album.coverable && album.coverable_type == 'Video'
          image_tag album.coverable.thumbnail.url(:thumb), :alt => album.name, :size => album.coverable.thumbnail.image_size(:thumb)
        elsif album.coverable && album.coverable_type == 'Photo'
          image_tag album.coverable.photo.url(:thumb), :alt => album.name, :size => album.coverable.photo.image_size(:thumb)
        end
      end
      row :name
      row :created_at
      row :updated_at
      row :count_photos do |album|
        link_to album.count_photos, edit_admin_photo_album_path(album), :class => 'member_link'
      end
      row :count_videos do |album|
        link_to album.count_videos, edit_admin_photo_album_path(album), :class => 'member_link'
      end
      row :description do |album|
        album.description.html_safe
      end
      row :sort do |album|
        I18n.t("photo_albums.sort_types.#{album.sort}")
      end
      row :order do |album|
        I18n.t("photo_albums.order_types.#{album.order}")
      end
    end
    active_admin_comments
  end

  index do
    selectable_column
    column :name do |photo_album|
      link_to photo_album.name, edit_admin_photo_album_path(photo_album)
    end
    column :published do |photo_album|
      if photo_album.published?
        image_tag '/assets/online.png', :alt => 'yes', :title => 'published'
      else
        image_tag '/assets/offline.png', :alt => 'no', :title => 'not published'
      end
    end
    column :created_at
    column :count_photos
    column :count_videos
    column "" do |resource|
      links = ''.html_safe
      links += link_to I18n.t('active_admin.view'), resource_path(resource), :class => 'member_link view_link'
      links += link_to I18n.t('active_admin.edit'), edit_resource_path(resource), :class => 'member_link edit_link'
      links += link_to I18n.t('active_admin.delete'), resource_path(resource), :method => :delete,
                       :confirm => I18n.t('active_admin.delete_confirmation'), :class => 'member_link delete_link'
      links += link_to I18n.t('activerecord.models.photo.other'), "#{edit_resource_path(resource)}#edit-photos", :class => 'member_link'
      links += link_to I18n.t('activerecord.models.video.other'), "#{edit_resource_path(resource)}#add-video", :class => 'member_link'
      links
    end
  end

  filter :name
  filter :created_at

  member_action :changecover, :method => :get do
    @album = PhotoAlbum.find(params[:id])
    @photos = @album.photos
    @videos = @album.videos
  end

  member_action :dochangecover, :method => :post do
    @album = PhotoAlbum.find(params[:id])

    cover_model = params[:cover_type].to_s.camelize.constantize

    cover = cover_model.find(params[:cover_id])

    @album.cover = nil
    @album.save!

    @album.update_attributes!(:coverable => cover)

    render :nothing => true
  end

  # Сортировка фотографий
  # member_action :sort, :method => :get do

  # end

  # Прикрепление фотографии
  member_action :attach, :method => :post do

    if params.has_key?(:id)
      album = PhotoAlbum.find(params[:id])
      @photo = album.photos.build
    else
      @photo = Photo.new
    end

    # For XHR file uploads, request.params[:qqfile] will be the path to the temporary file
    # For regular form uploads (such as those made by Opera), request.params[:qqfile] will be an UploadedFile which can be returned unaltered.
    if not request.params[:qqfile].is_a?(String)
      @photo.photo = params[:qqfile]
    else
      ######################## dealing with local files #############
      # get file name
      file_name = params[:qqfile]
      # get file content type
      att_content_type = (request.content_type.to_s == "") ? "application/octet-stream" : request.content_type.to_s
      # create tempora##l file
      begin
        file = Tempfile.new(file_name, {:encoding =>  'BINARY'})
        file.print request.raw_post.force_encoding('BINARY')
      rescue RuntimeError => e
        raise e unless e.message.include?('cannot generate tempfile')
        file = Tempfile.new(file_name) # Ruby 1.8 compatibility
        file.binmode
        file.print request.raw_post
      end
      # put data into this file from raw post request

      # create several required methods for this temporal file
      Tempfile.send(:define_method, "content_type") {return att_content_type}
      Tempfile.send(:define_method, "original_filename") {return file_name}
      @photo.photo = file
    end

    if @photo.save
      respond_to do |format|
        format.plaintext
      end
    else
      render :json => @photo.errors.to_json
    end
  end

  # Переопределяем метод создания альбома
  controller do
    def create
      @photo_album = PhotoAlbum.new(params[:photo_album])

      if @photo_album.save
        redirect_to(edit_admin_photo_album_path(@photo_album), :notice => I18n.t("active_admin.photo_album.created"))
      else
        render :action => "new"
      end
    end
  end
end
# :nocov:
