# encoding: utf-8

class UrlValidator < ActiveModel::EachValidator
  URL_REGEXP_PATTERN = %r{
    \A
    (?:(?:https?|ftp)://)                                       # protocol identifier
    (?:\S+(?::\S*)?@)?                                          # user:pass authentication
    (?:                                                         # IP address exclusion private & local networks
      (?!10(?:\.\d{1,3}){3})
      (?!127(?:\.\d{1,3}){3})
      (?!169\.254(?:\.\d{1,3}){2})
      (?!192\.168(?:\.\d{1,3}){2})
      (?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})
      (?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])
      (?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}
      (?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))
      |
      (?:(?:[a-z\p{Ll}\p{Lu}0-9]-?)*[a-z\p{Ll}\p{Lu}0-9]+)     # host name
      (?:\.(?:[a-z\p{Ll}\p{Lu}0-9]-?)*[a-z\p{Ll}\p{Lu}0-9]+)*  # domain name
      (?:\.(?:[a-z\p{Ll}\p{Lu}]{2,}))                           # TLD identifier
      |
      (?:xn--[a-z0-9-]{1,59}|(?:(?:[a-z\u00a1-\uffff0-9]-?){0,62}[a-z\u00a1-\uffff0-9]{1,63}))
      (?:\.(?:xn--[a-z0-9-]{1,59}|(?:[a-z\u00a1-\uffff0-9]-?){0,62}[a-z\u00a1-\uffff0-9]{1,63}))
      (?:\.(?:xn--[a-z0-9-]{1,59}|(?:[a-z\u00a1-\uffff]{2,63})))
    )
    (?::\d{2,5})?                                               # port number
    (?:/[^\s]*)?                                                # resource path
    \z
  }iux

  def validate_each(record, attribute, value)
    unless value =~ URL_REGEXP_PATTERN
      message = options.fetch(:message, I18n.t('validators.url.message'))
      record.errors.add(attribute, message)
    end
  end
end
