# encoding: utf-8

ThinkingSphinx::Index.define :page, :with => :active_record do
  indexes title, :sortable => true
  indexes body
end

ThinkingSphinx::Index.define :page, :name => 'page_rt', :with => :real_time do
  indexes title, :sortable => true
  indexes body
end
