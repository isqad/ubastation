# encoding: utf-8

ThinkingSphinx::Index.define :photo_album, :with => :active_record do
  indexes name, :sortable => true
  indexes description
  where sanitize_sql(['published', true])
end

ThinkingSphinx::Index.define :photo_album, :name => 'photo_album_rt', :with => :real_time do
  indexes name, :sortable => true
  indexes description
  where sanitize_sql(['published', true])
end
