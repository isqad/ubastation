# encoding: utf-8

ThinkingSphinx::Index.define :post, :with => :active_record do
  indexes topic(:title), :as => :topic, :sortable => true
  indexes user(:name), :as => :author, :sortable => true
  indexes body
  has topic_id
  has user_id
  has created_at
end

ThinkingSphinx::Index.define :post, :name => 'post_rt', :with => :real_time do
  indexes topic(:title), :as => :topic, :sortable => true
  indexes user(:name), :as => :author, :sortable => true
  indexes body
  has topic_id, :type => :integer
  has user_id, :type => :integer
  has created_at, :type => :timestamp
end


