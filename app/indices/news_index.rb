# encoding: utf-8

ThinkingSphinx::Index.define :news, :with => :active_record do
  indexes title, :sortable => true
  indexes body
  where sanitize_sql(['published', true])
  has created_at
end

ThinkingSphinx::Index.define :news, :name => 'news_rt', :with => :real_time do
  indexes title, :sortable => true
  indexes body
  where sanitize_sql(['published', true])
  has created_at, :type => :timestamp
end
